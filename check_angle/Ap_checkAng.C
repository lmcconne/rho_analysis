#include "../useful_variables.C"
#include "../useful_variables.h"
using namespace std;

void Ap_checkAng()
{
	// set gStyle options
	gStyle->SetOptStat(10);
	gStyle->SetOptFit(1);

	// read input from ascii
	auto nt = new TNtuple("nt","nt","px1:py1:pz1:px2:py2:pz2:ang");
	nt->ReadFile("ang.dat");
	Float_t px1, py1, pz1, px2, py2, pz2, ang;
	nt->SetBranchAddress("px1",&px1);
	nt->SetBranchAddress("py1",&py1);
	nt->SetBranchAddress("pz1",&pz1);
	nt->SetBranchAddress("px2",&px2);
	nt->SetBranchAddress("py2",&py2);
	nt->SetBranchAddress("pz2",&pz2);
	nt->SetBranchAddress("ang",&ang);

	// create ascii output file
	auto output_file = fopen("Ap_output.txt","w");

	// cos(theta) hist
	auto h_luke = new TH1F("h_luke","from Luke",20,-1,1);
	auto h_ronan = new TH1F("h_ronan","from Ronan",20,-1,1);
	auto h_diff = new TH1F("h_diff","Difference",20,0,2);

	// Event Loop
    	auto nevts = nt->GetEntriesFast();
    	//auto nevts = 100;
	cout << "Running over " << nevts << " events..." << endl;
    
    	auto count = 1;
   
    	for(int i = 0; i < nevts; i++)
    	{
        	// print event count
        	if((count % 100) == 0) cout << count << endl;
        
        	// increment event counter
        	count += 1;
     
        	// retrieve event
        	nt->GetEntry(i);

		// create four vectors for the muons
    		TLorentzVector muon_vector[2];
		muon_vector[0].SetXYZM(px1,py1,pz1,muon_mass/1000.);
		muon_vector[1].SetXYZM(px2,py2,pz2,muon_mass/1000.);
			
    		// create a four vector for the mother
		TLorentzVector mother_vector;
		mother_vector = muon_vector[0] + muon_vector[1];

		// find the muons' pseudorapidities
    		auto muon_eta1 = muon_vector[0].PseudoRapidity();
    		auto muon_eta2 = muon_vector[1].PseudoRapidity();

    		// find the muons' pts
    		auto muon_pt1 = muon_vector[0].Pt();
    		auto muon_pt2 = muon_vector[1].Pt();

		// calc mother kinematic variables
		auto y = mother_vector.Rapidity();
		auto mass = mother_vector.M();
		auto pt2 = mother_vector.Pt() * mother_vector.Pt();
	 
		// get the theta variable
		auto theta = gammaP_theta(mother_vector,muon_vector[0],muon_vector[1],+1,-1);
		auto cosTheta = cos(theta);

		// fill hist
		h_luke->Fill(cosTheta);
		h_diff->Fill(fabs(cosTheta - ang));

		// write to output ascii file
		// order: px1, py1, pz1, px2, py2, pz2, theta
		fprintf(output_file,"%f %f %f %f %f %f %f \n",px1,py1,pz1,px2,py2,pz2,cosTheta);
	}

	// plot cos(theta)

	auto c_luke = new TCanvas("c_luke","from Luke");
	h_luke->Draw("E1");
	TF1 *f1 = new TF1("f1","[0] * (1 + [1]*x*x)",-1.,1.);
	f1->SetParNames("N","lambda");
	h_luke->Fit("f1");

	auto c_ronan = new TCanvas("c_ronan","from Ronan");
	nt->Project("h_ronan","ang");
	h_ronan->Draw("E1");
	TF1 *f2 = new TF1("f2","[0] * (1 + [1]*x*x)",-1.,1.);
	f2->SetParNames("N","lambda");
	h_ronan->Fit("f2");

	auto c_diff = new TCanvas("c_diff","difference");
	h_diff->Draw("E1");
}

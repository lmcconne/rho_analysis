#!bin/bash
rm -r build
mkdir build
cd build
cmake ..
make
cp ../config/slight.in .
cp ~/rho_analysis/flux/starlight/slight.in* .
cd ..

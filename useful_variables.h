#ifndef MY_GLOBALS_H
#define MY_GLOBALS_H

#include <TBox.h>
#include <TSpline.h>
#include <TGraph.h>
#include <TLorentzVector.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TCanvas.h>
#include <TMatrixD.h>
#include <TVectorD.h>
#include <TRandom3.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TH3F.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TFile.h>
#include <TNtuple.h>
#include <TChain.h>
#include <TF1.h>
#include <TStopwatch.h>
#include <TGenPhaseSpace.h>
#include <TString.h>
#include <iostream>
#include <math.h>
#include "bessel.h"
#include "bessel.cpp"

//////////////////////////////
// constants from STARlight //
//////////////////////////////

//extern double hbarc;
//extern double hbarcmev;
//extern double pi;
//extern double twoPi;
//extern double alpha;

//////////////////////
// Luke's constants //
//////////////////////

// set postions of trackers
extern const double ITOT_pos;
extern const double TT_pos;

// set magnet polarity
extern int mag_pol;

// all masses in MeV
//
// initialise proton mass
extern const double proton_mass;
// initialise rho mass
extern const double rho_mass;
// initialise rho width
extern const double rho_width;
// initialise omega mass
extern const double omega_mass;
// initialise omega width
extern const double omega_width;
// initialise the pion mass
extern const double pion_mass;
// initialise the kaon mass
extern const double kaon_mass;
// initialise the kaon width
extern const double kaon_width;
// initialise the muon mass
extern const double muon_mass;
// initialise the phi mass
extern const double phi_mass;
// initialise the phi width
extern const double phi_width;

// initialise min track pt in MeV
extern const double min_trck_pt;

// initialise beamline location estimate
extern const TVector3 pA_beam;
extern const TVector3 Ap_beam;

// initialise decay distance cut (mm)
extern const double decdist_cut;

// initialise kaon veto thresholds
extern const double kaon_cut;
extern const double kaon_sel;

// initialise chi chut
extern const double herschel_cut;

// initialise number of nbins in 3D histograms
extern const int xbins; // mass
extern const int ybins; // pt2
extern const int zbins; // theta

// initialise the histogram ranges
extern const double xlow; // mass in MeV
extern const double xhigh;
extern const double ylow; // pt2 in GeV^2
extern const double yhigh;
extern const double zlow; // theta
extern const double zhigh;

// set bin widths
extern const double ybin_width;
extern const double mbin_width;
extern const double pt2bin_width;

// herschel efficiency
extern const double pA_he_eff;
extern const double pA_he_stat;
extern const double pA_he_sys;
extern const double Ap_he_eff;
extern const double Ap_he_stat;
extern const double Ap_he_sys;

// crossing angles
extern const double theta_v;
extern const double theta_h;

// luminosity (micro b)
extern const double pA_lumi;
extern const double pA_lumi_err;
extern const double Ap_lumi;
extern const double Ap_lumi_err;

// Angular window corrections
extern const double Ath[6];

///////////////////////////
// function declarations //
///////////////////////////

// define function calculate the binomial error
double binomialError(double Nafter,double Nafter_err,double Nbefore,double Nbefore_err);

// function to calculate bkg. subtracted lambda
double calcLambda(double lambdaEl,double lambdaInel,double a,double b);

// function to calculate bkg. subtracted lambda error
double calcLambda_err(double lambdaEl_err,double lambdaInel_err,double a,double b);

// function to calculate the polarisation weight
double calcPolWeight(double& angle,double& l);

// function to check the efficiencies during an event loop
void checkEff(int& event,double& eff,const char* description);

// function to check the weights during an event loop
void checkWeight(int& event,double& weight,const char* description);

// class for converting TH1D into TF1 for convenience
class convert_1Dhisto;

// error on function of form a/b
double errDiv(double a,double a_err,double b,double b_err);

// error on function of form a*b
double errMult(double a,double a_err,double b,double b_err);

// Function used to obtain the photon flux using the photon energy.
// Returns k*(dn/dk).
// Uses bessel functions as defined in the STARlight source code.
double getFlux(double Egamma,double bmin,double beam_gamma,int Z);

// Get the flux table
double* getFluxTable();

// Get the flux interpolation
TSpline3* getFluxInterpol(TString input);

// function to get pull distribution
TGraph* getPull(TH1D* hist,TF1* fit_function,int colour);

// function to calculate cross section correction
void getWindow(TF1* fit_function, double lower_win_range, double upper_win_range, double lower_tot_range, double upper_tot_range, double& out, double& out_err);

// function to check if track is reonstructable as long
bool isLong(TLorentzVector& v,int& charge,int& bmag);
bool isLong_alt(TLorentzVector& v,int& charge,int& bmag);

// function to check if track is reonstructable as upstream
bool isUpstream(TLorentzVector& v,int& charge,int& bmag);
bool isUpstream_alt(TLorentzVector& v,int& charge,int& bmag);

// function to sample pt2 from the sum of two exponentials
double rejPt2(TRandom3& rnd,double& a,double& b);

// function to determine the cos(theta) variable
double gammaP_theta(TLorentzVector mother,TLorentzVector track1,TLorentzVector track2,int trck1_charge,int trck2_charge,double thetaV,double thetaH);
double pGamma_theta(TLorentzVector mother,TLorentzVector track1,TLorentzVector track2,int trck1_charge,int trck2_charge,double thetaV,double thetaH);

// function used to determine the location of the primary vertex
void vert(double tx1,double ty1,double tz1, double px1, double py1, double pz1,double tx2,double ty2,double tz2, double px2, double py2, double pz2,double &xv, double &yv,double &zv,double &vchi);

// function to calc W
double pGamma_W_old(TLorentzVector mother,double thetaV,double thetaH); // pA
double gammaP_W_old(TLorentzVector mother,double thetaV,double thetaH); // Ap
double pGamma_W(TLorentzVector mother); // pA
double gammaP_W(TLorentzVector mother); // Ap
double pGamma_W_alt(TLorentzVector mother); // pA
double gammaP_W_alt(TLorentzVector mother); // Ap

// function used to set a smart label for histograms
void setLabelY(TH1* h,const char* unit);
void setNormLabelY(TH1* h,const char* unit);

// function which inputs Wgp and gives Egamma in the p-Pb collision CoM frame
double wToEgamma(double Wgp,double beam_gamma);

// function to determine statistical significance for parameter differences between rapidity bins
void parDiffSignf(int N,double *pars,double *pars_err,double &pars_chi2,double &pars_chi2_prob);

// function used to determine ystar limits from ylab limits
void ystarFinder(double *ystar_low_arr,double *ystar_upp_ar);

#endif

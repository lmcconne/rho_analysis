#include "useful_variables.C"
#include "useful_variables.h"

void preAcc(TString outputID)
{
	// start timer
	auto stopwatch = new TStopwatch();
	stopwatch->Start();

	// create file
	TString output_name = "pre_acc_" + outputID + ".root"; 
	TFile pre_acc_file(output_name,"RECREATE");
    	cout << "A file " + output_name + "  has been created." << endl;

 	// set number of events to generate
	int nevts = 1000000000;
   	
       	// set up mass array pions in GeV
    	double pion_mass_array[2] = {pion_mass/1000.,pion_mass/1000.};

    	// set magnet polarity
    	int mag_pol = -1;

    	// create the random number generator object
    	auto rnd = new TRandom3();
	// according to the documentation this sets a unique seed
	// and should work with my submitting the script in batch
	rnd->SetSeed(0);

    	// create the decay event object
    	auto decay_event = new TGenPhaseSpace();

    	// set the values for A and B for the mass
    	// average values taken from my results
    	double A = 1;
	double B = -0.024;

    	// set the slope of the exponential sampled for the pt2
    	// average values taken from my work
    	double a = 9.0;
	double b = 6.3;

    	// set lambda (for polarisation)
    	double l = 1.0;
	
	// define histograms
    
    	auto hpt = new TH1D("hpt","Four-Vector Simulation",ybins,ylow,yhigh);
    	auto hmass = new TH1D("hmass","Four-Vector Simulation",xbins,xlow,xhigh);
    	auto ht = new TH1D("ht","Four-Vector Simulation",zbins,zlow,zhigh);
    	auto hy = new TH1D("hy","Four-Vector Simulation",6,2,5);
    	auto hy_in_acc = new TH1D("hy_in_acc","Four-Vector Simulation",6,2,5);

    	auto hmtp0_precut = new TH3D("hmtp0_precut","hmtp0_precut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
	auto hmtp2_precut = new TH3D("hmtp2_precut","hmtp2_precut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp1_precut = new TH3D("hmtp1_precut","hmtp1_precut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp3_precut = new TH3D("hmtp3_precut","hmtp3_precut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp4_precut = new TH3D("hmtp4_precut","hmtp4_precut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp5_precut = new TH3D("hmtp5_precut","hmtp5_precut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp_precut = new TH3D("hmtp_precut","hmtp_precut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);

	auto hmtp0_midcut = new TH3D("hmtp0_midcut","hmtp0_midcut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
	auto hmtp2_midcut = new TH3D("hmtp2_midcut","hmtp2_midcut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp1_midcut = new TH3D("hmtp1_midcut","hmtp1_midcut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp3_midcut = new TH3D("hmtp3_midcut","hmtp3_midcut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp4_midcut = new TH3D("hmtp4_midcut","hmtp4_midcut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp5_midcut = new TH3D("hmtp5_midcut","hmtp5_midcut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp_midcut = new TH3D("hmtp_midcut","hmtp_midcut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);

    	auto hmtp0_postcut = new TH3D("hmtp0_postcut","hmtp0_postcut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp1_postcut = new TH3D("hmtp1_postcut","hmtp1_postcut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp2_postcut = new TH3D("hmtp2_postcut","hmtp2_postcut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp3_postcut = new TH3D("hmtp3_postcut","hmtp3_postcut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp4_postcut = new TH3D("hmtp4_postcut","hmtp4_postcut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp5_postcut = new TH3D("hmtp5_postcut","hmtp5_postcut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp_postcut = new TH3D("hmtp_postcut","hmtp_postcut",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);

    	auto hmtp0_postcut_alt = new TH3D("hmtp0_postcut_alt","hmtp0_postcut_alt",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp1_postcut_alt = new TH3D("hmtp1_postcut_alt","hmtp1_postcut_alt",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp2_postcut_alt = new TH3D("hmtp2_postcut_alt","hmtp2_postcut_alt",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp3_postcut_alt = new TH3D("hmtp3_postcut_alt","hmtp3_postcut_alt",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp4_postcut_alt = new TH3D("hmtp4_postcut_alt","hmtp4_postcut_alt",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp5_postcut_alt = new TH3D("hmtp5_postcut_alt","hmtp5_postcut_alt",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);
    	auto hmtp_postcut_alt = new TH3D("hmtp_postcut_alt","hmtp_postcut_alt",xbins,xlow,xhigh,ybins,ylow,yhigh,zbins,zlow,zhigh);

    	hmass->Sumw2();
    	hpt->Sumw2();
    	ht->Sumw2();
    	hy->Sumw2();
    	hy_in_acc->Sumw2();
    	hmtp0_precut->Sumw2();
    	hmtp1_precut->Sumw2();
    	hmtp2_precut->Sumw2();
    	hmtp3_precut->Sumw2();
    	hmtp4_precut->Sumw2();
    	hmtp5_precut->Sumw2();
    	hmtp_precut->Sumw2();
    	hmtp0_midcut->Sumw2();
    	hmtp1_midcut->Sumw2();
    	hmtp2_midcut->Sumw2();
    	hmtp3_midcut->Sumw2();
    	hmtp4_midcut->Sumw2();
    	hmtp5_midcut->Sumw2();
    	hmtp_midcut->Sumw2();
    	hmtp0_postcut->Sumw2();
    	hmtp1_postcut->Sumw2();
    	hmtp2_postcut->Sumw2();
    	hmtp3_postcut->Sumw2();
    	hmtp4_postcut->Sumw2();
    	hmtp5_postcut->Sumw2();
    	hmtp_postcut->Sumw2();
    	hmtp0_postcut_alt->Sumw2();
    	hmtp1_postcut_alt->Sumw2();
    	hmtp2_postcut_alt->Sumw2();
    	hmtp3_postcut_alt->Sumw2();
    	hmtp4_postcut_alt->Sumw2();
    	hmtp5_postcut_alt->Sumw2();
    	hmtp_postcut_alt->Sumw2();

    	// Event Loop
    
    	cout << "Looping over TGP events..." << endl;
    
    	for(int i = 0;i < nevts;i++)
    	{
        	if((i % 100000) == 0) cout << i << endl;
        
        	// generate a decay for the y ##########################################
        	// need to use GeV here
		double mass = rejMass(rnd,A,B)/1000; 
		double pt2 = rejPt2(rnd,a,b); 

        	double pt = sqrt(pt2);
        	double y = rnd->Rndm() * 3 + 2;
        	double phi = 2 * TMath::Pi() * rnd->Rndm();
        	double pz = sqrt(pt*pt + mass*mass) * sinh(y);
        	double px = pt * cos(phi);
        	double py = pt * sin(phi);
        	auto mom = TVector3(px,py,pz);
        	double E = sqrt(px*px + py*py + pz*pz + mass*mass);
        
        	// create a four vector for the y
        	auto vector = TLorentzVector(mom,E);
        
        	auto W = TLorentzVector(TVector3(px,py,pz),E);
        
        	decay_event->SetDecay(W,2,pion_mass_array);
        
        	// generate event
        	decay_event->Generate();
        
        	// get the rho kinematics in MeV
        	vector *= 1000;
        	mom = vector.Vect();
        	mass = vector.M();
        	E = vector.E();
        	pt = vector.Pt();
        	pt2 = pow(pt/1000.,2); // this is in GeV^2
        
        	// work with the pion kinematics in MeV
        	auto pion1_vector = * (decay_event->GetDecay(0));
        	pion1_vector *= 1000;
        	auto pion2_vector = * (decay_event->GetDecay(1));
        	pion2_vector *= 1000;
        
        	// assuming the particles are pions, get their energy
        	double pion1_E = pion1_vector.E();
        	double pion2_E = pion2_vector.E();
        
        	// find the pions' pseudorapidities
        	double pion1_eta = pion1_vector.PseudoRapidity();
        	double pion2_eta = pion2_vector.PseudoRapidity();
        
        	// find the pions' pts
        	double pion1_pt = pion1_vector.Pt();
        	double pion2_pt = pion2_vector.Pt();
        
        	// track charges (arbitrarily set for this code)
        	int trck1_charge = +1;
        	int trck2_charge = -1;
        
        	// determine if tracks are reconstructible
        	bool trck1_isLong = isLong(pion1_vector,trck1_charge,mag_pol);
        	bool trck2_isLong = isLong(pion2_vector,trck2_charge,mag_pol);
        	bool trck1_isLong_alt = isLong_alt(pion1_vector,trck1_charge,mag_pol);
        	bool trck2_isLong_alt = isLong_alt(pion2_vector,trck2_charge,mag_pol);
        
        	// determine if tracks are reconstructible as upstream
        	bool trck1_isUpstream = isUpstream(pion1_vector,trck1_charge,mag_pol);
        	bool trck2_isUpstream = isUpstream(pion2_vector,trck2_charge,mag_pol);
        	bool trck1_isUpstream_alt = isUpstream_alt(pion1_vector,trck1_charge,mag_pol);
        	bool trck2_isUpstream_alt = isUpstream_alt(pion2_vector,trck2_charge,mag_pol);

        	// calc theta angle
        	double theta = pGamma_theta(vector,pion1_vector,pion2_vector,trck1_charge,trck2_charge);
		double ct = fabs(cos(theta));
 
        	// get polarisation weight
        	double weight = calcPolWeight(theta,l);

		// Fill rho kinematic hists
        	hpt->Fill(pt2);
        	hmass->Fill(mass);
        	hy->Fill(y);
        	ht->Fill(ct,weight);
        
        	// fill pre-cut 3D hists
        	hmtp_precut->Fill(mass,pt2,ct,weight);
        	if(y < 2.5) hmtp0_precut->Fill(mass,pt2,ct,weight);
        	else if((2.5 < y) && (y < 3)) hmtp1_precut->Fill(mass,pt2,ct,weight);
		else if((3 < y) && (y < 3.5)) hmtp2_precut->Fill(mass,pt2,ct,weight);
		else if((3.5 < y) && (y < 4)) hmtp3_precut->Fill(mass,pt2,ct,weight);
		else if((4 < y) && (y < 4.5)) hmtp4_precut->Fill(mass,pt2,ct,weight);
		else hmtp5_precut->Fill(mass,pt2,ct,weight);
    
        	// fiducial acceptance requirements
        	if(((2 < pion1_eta) && (pion1_eta < 5)) && ((2 < pion2_eta) && (pion2_eta < 5)) && (min_trck_pt < pion1_pt) && (min_trck_pt < pion2_pt))
        	{
			// fill mid-cut 3D hists
		    	hmtp_midcut->Fill(mass,pt2,ct,weight);
			if(y < 2.5) hmtp0_midcut->Fill(mass,pt2,ct,weight);
			else if((2.5 < y) && (y < 3)) hmtp1_midcut->Fill(mass,pt2,ct,weight);
			else if((3 < y) && (y < 3.5)) hmtp2_midcut->Fill(mass,pt2,ct,weight);
			else if((3.5 < y) && (y < 4)) hmtp3_midcut->Fill(mass,pt2,ct,weight);
			else if((4 < y) && (y < 4.5)) hmtp4_midcut->Fill(mass,pt2,ct,weight);
			else hmtp5_midcut->Fill(mass,pt2,ct,weight);

			// reconstruction acceptance requirements
			if(((trck1_isLong == true) || (trck1_isUpstream == true)) && ((trck2_isLong == true) || (trck2_isUpstream == true)))
			{
            			// fill hist for y within acc
            			hy_in_acc->Fill(y);

		    		// fill post-cut 3D hists
		    		hmtp_postcut->Fill(mass,pt2,ct,weight);
				if(y < 2.5) hmtp0_postcut->Fill(mass,pt2,ct,weight);
				else if((2.5 < y) && (y < 3)) hmtp1_postcut->Fill(mass,pt2,ct,weight);
				else if((3 < y) && (y < 3.5)) hmtp2_postcut->Fill(mass,pt2,ct,weight);
				else if((3.5 < y) && (y < 4)) hmtp3_postcut->Fill(mass,pt2,ct,weight);
				else if((4 < y) && (y < 4.5)) hmtp4_postcut->Fill(mass,pt2,ct,weight);
				else hmtp5_postcut->Fill(mass,pt2,ct,weight);
			}
			// alternative reconstruction acceptance requirements
			if(((trck1_isLong_alt == true) || (trck1_isUpstream_alt == true)) && ((trck2_isLong_alt == true) || (trck2_isUpstream_alt == true)))
			{
		    		// fill alt post-cut 3D hists
		    		hmtp_postcut_alt->Fill(mass,pt2,ct,weight);
				if(y < 2.5) hmtp0_postcut_alt->Fill(mass,pt2,ct,weight);
				else if((2.5 < y) && (y < 3)) hmtp1_postcut_alt->Fill(mass,pt2,ct,weight);
				else if((3 < y) && (y < 3.5)) hmtp2_postcut_alt->Fill(mass,pt2,ct,weight);
				else if((3.5 < y) && (y < 4)) hmtp3_postcut_alt->Fill(mass,pt2,ct,weight);
				else if((4 < y) && (y < 4.5)) hmtp4_postcut_alt->Fill(mass,pt2,ct,weight);
				else hmtp5_postcut_alt->Fill(mass,pt2,ct,weight);
			}

        	}
	}
    
 	cout << "Finished looping over the TGP events." << endl;

	// write hists to file
    
    	hmass->Write();
    	hpt->Write();
    	ht->Write();
    	hy->Write();
    	hy_in_acc->Write();
	
	hmtp0_precut->Write();
	hmtp1_precut->Write();
	hmtp2_precut->Write();
	hmtp3_precut->Write();
	hmtp4_precut->Write();
	hmtp5_precut->Write();
	hmtp_precut->Write();

	hmtp0_midcut->Write();
	hmtp1_midcut->Write();
	hmtp2_midcut->Write();
	hmtp3_midcut->Write();
	hmtp4_midcut->Write();
	hmtp5_midcut->Write();
	hmtp_midcut->Write();

	hmtp0_postcut->Write();
	hmtp1_postcut->Write();
	hmtp2_postcut->Write();
	hmtp3_postcut->Write();
	hmtp4_postcut->Write();
	hmtp5_postcut->Write();
	hmtp_postcut->Write();

	hmtp0_postcut_alt->Write();
	hmtp1_postcut_alt->Write();
	hmtp2_postcut_alt->Write();
	hmtp3_postcut_alt->Write();
	hmtp4_postcut_alt->Write();
	hmtp5_postcut_alt->Write();
	hmtp_postcut_alt->Write();

    	// end of script
    	pre_acc_file.Close();
    	cout << "File closed and we're done!" << endl;
    	stopwatch->Stop();
	stopwatch->Print();
}

#include "../../useful_variables.C"
#include "../../useful_variables.h"

// define sideband centres
const double K0_mass = 497.611;
const double left_centre = 447.611;
const double right_centre = 547.611;

// number of bins on either side of central bin (9 bins in total)
const int win_size = 4;

// set ideal efficiency
const double ideal_eff = 0.9;

// used to stored efficiencies averaged across the datasets
double eff_avg_array[6], eff_avg_array_err[6];

// print out efficiency summary table
double precut_array[14], precut_array_err[14], postcut_array[14], postcut_array_err[14], eff_array[14], eff_array_err[14];
void printTab()
{
	printf("Efficiency Summary Table:\n");
	printf("\\begin{tabular}{|c|c|} \\hline\n");
	printf("\t$y_{\\pi\\pi}$-interval & Efficiency \\\\ \\hline\n");
	printf("\t\\multicolumn{2}{c}{p-Pb Data} \\\\ \\hline\n");
	printf("\t(2,2.5) & \\num{%f(%f)} \\\\ \\hline\n",eff_array[0],eff_array_err[0]);
	printf("\t(2.5,3) & \\num{%f(%f)} \\\\ \\hline\n",eff_array[1],eff_array_err[1]);
	printf("\t(3,3.5) & \\num{%f(%f)} \\\\ \\hline\n",eff_array[2],eff_array_err[2]);
	printf("\t(3.5,4) & \\num{%f(%f)} \\\\ \\hline\n",eff_array[3],eff_array_err[3]);
	printf("\t(4,4.5) & \\num{%f(%f)} \\\\ \\hline\n",eff_array[4],eff_array_err[4]);
	printf("\t(4.5,5) & \\num{%f(%f)} \\\\ \\hline\n",eff_array[5],eff_array_err[5]);
	printf("\t(2,5) & \\num{%f(%f)} \\\\ \\hline\n",eff_array[6],eff_array_err[6]);
	printf("\t\\multicolumn{2}{c}{Pb-p Data} \\\\ \\hline\n");
	printf("\t(2,2.5) & \\num{%f(%f)} \\\\ \\hline\n",eff_array[7],eff_array_err[7]);
	printf("\t(2.5,3) & \\num{%f(%f)} \\\\ \\hline\n",eff_array[8],eff_array_err[8]);
	printf("\t(3,3.5) & \\num{%f(%f)} \\\\ \\hline\n",eff_array[9],eff_array_err[9]);
	printf("\t(3.5,4) & \\num{%f(%f)} \\\\ \\hline\n",eff_array[10],eff_array_err[10]);
	printf("\t(4,4.5) & \\num{%f(%f)} \\\\ \\hline\n",eff_array[11],eff_array_err[11]);
	printf("\t(4.5,5) & \\num{%f(%f)} \\\\ \\hline\n",eff_array[12],eff_array_err[12]);
	printf("\t(2,5) & \\num{%f(%f)} \\\\ \\hline\n",eff_array[13],eff_array_err[13]);
	printf("\\end{tabular}\n");
}

// define function to calculate the error for the efficiency obtained using the sidebands subtraction technique
double errSidebandSub(int nevts_precut_central,int nevts_precut_left,int nevts_precut_right,int nevts_postcut_central,int nevts_postcut_left,int nevts_postcut_right)
{
	// create the random number generator object
	auto rnd = new TRandom3();

	// create histogram to save efficiencies from pseudo-experiments
	auto heff = new TH1D("heff","heff",50,0.8,1.0);

	// loop for the number of pseudo-experiments
	for(int i = 0;i < 10000;i++)
	{
		// Poisson fluctuate the number of events in each region prior to the cut
        	int new_nevts_precut_central = rnd->Poisson(nevts_precut_central);
        	int new_nevts_precut_left = rnd->Poisson(nevts_precut_left);
        	int new_nevts_precut_right = rnd->Poisson(nevts_precut_right);
	
        	// subtract the background to get the number of signal events prior to the cut
        	auto new_nevts_precut_sig = new_nevts_precut_central - (new_nevts_precut_left + new_nevts_precut_right)/2.;

        	// roll the die
        	auto roll = rnd->Uniform();

        	// roll to see if the event passes
        	if(roll < ideal_eff)
		{
			// if the event passes Poisson fluctuate the number of events in each region prior to the cut
			auto new_nevts_postcut_central = rnd->Poisson(nevts_postcut_central);
			auto new_nevts_postcut_left = rnd->Poisson(nevts_postcut_left);
			auto new_nevts_postcut_right = rnd->Poisson(nevts_postcut_right);

			// subtract the background to get the number of signal events after the cut
			auto new_nevts_postcut_sig = new_nevts_postcut_central - (new_nevts_postcut_left + new_nevts_postcut_right)/2.;

			// calculate the effciency
			auto eff = new_nevts_postcut_sig/new_nevts_precut_sig;

	 		// fill the efficiency histogram
			heff->Fill(eff);
		}
	}

	// fit a gaussian to the histogram of efficiencies
	auto ferr = new TF1("ferr","gaus");
	heff->Fit("ferr","NQ");

	// get the error of the efficiency from the sprecutad of the Gaussian
	double err = ferr->GetParameter(2);

	delete heff;

	// return the sprecutad of the fitted gaussian
	return err;
}

// function used to calculate kaon-veto efficiency using side-band subtraction
void effSidebandSub(TH1D *h_precut,TH1D *h_postcut,double &nevts_postcut_sig,double &nevts_postcut_sig_err,double &nevts_precut_sig,double &nevts_precut_sig_err,double &eff,double &err)
{
	// define the centre of the regions
	int mass_mid_bin = h_precut->FindBin(K0_mass);
	int left_mid_bin = h_precut->FindBin(left_centre);
	int right_mid_bin = h_precut->FindBin(right_centre);
    
	// calculate the number of precut-cut events in the regions
	double nevts_precut_central_err;
	double nevts_precut_central = h_precut->IntegralAndError(mass_mid_bin - win_size,mass_mid_bin + win_size,nevts_precut_central_err);
	double nevts_precut_left_err;
	double nevts_precut_left = h_precut->IntegralAndError(left_mid_bin - win_size,left_mid_bin + win_size,nevts_precut_left_err);
	double nevts_precut_right_err;
	double nevts_precut_right = h_precut->IntegralAndError(right_mid_bin - win_size,right_mid_bin + win_size,nevts_precut_right_err);
    
	// calculate the number of postcut-cut events in the regions
	double nevts_postcut_central_err;
	double nevts_postcut_central = h_postcut->IntegralAndError(mass_mid_bin - win_size,mass_mid_bin + win_size,nevts_postcut_central_err);
	double nevts_postcut_left_err;
	double nevts_postcut_left = h_postcut->IntegralAndError(left_mid_bin - win_size,left_mid_bin + win_size,nevts_postcut_left_err);
	double nevts_postcut_right_err;
	double nevts_postcut_right = h_postcut->IntegralAndError(right_mid_bin - win_size,right_mid_bin + win_size,nevts_postcut_right_err);
    
	// calculate the number of precut-cut signal events
	nevts_precut_sig = nevts_precut_central - (nevts_precut_left + nevts_precut_right)/2.;
	nevts_precut_sig_err = sqrt(pow(nevts_precut_central_err,2.) + pow(nevts_precut_left_err/2.,2.) + pow(nevts_precut_left_err/2.,2.));
	// calculate the number of postcut-cut signal events
	nevts_postcut_sig = nevts_postcut_central - (nevts_postcut_left + nevts_postcut_right)/2.;
	nevts_postcut_sig_err = sqrt(pow(nevts_postcut_central_err,2.) + pow(nevts_postcut_left_err/2.,2.) + pow(nevts_postcut_left_err/2.,2.));

	// calculate the effciency
	eff = nevts_postcut_sig/nevts_precut_sig;
    
	// calculate the error on the efficiency
	err = errSidebandSub(nevts_precut_central,nevts_precut_left,nevts_precut_right,nevts_postcut_central,nevts_postcut_left,nevts_postcut_right);
}

// define function to calculate the effiency error histogram for the sidebands subtraction technique
TH1D* errHistSidebandSub(const char* name,TH1D *h_precut,TH1D *h_postcut)
{
	// define the centre of the regions
	int mass_mid_bin = h_precut->FindBin(K0_mass);
	int left_mid_bin = h_precut->FindBin(left_centre);
	int right_mid_bin = h_precut->FindBin(right_centre);
    
	// calculate the precut-cut number of events in the regions
	double nevts_precut_central = h_precut->Integral(mass_mid_bin - win_size,mass_mid_bin + win_size);
	double nevts_precut_left = h_precut->Integral(left_mid_bin - win_size,left_mid_bin + win_size);
	double nevts_precut_right = h_precut->Integral(right_mid_bin - win_size,right_mid_bin + win_size);
    
	// calculate the postcut-cut number of events in the regions
	double nevts_postcut_central = h_postcut->Integral(mass_mid_bin - win_size,mass_mid_bin + win_size);
	double nevts_postcut_left = h_postcut->Integral(left_mid_bin - win_size,left_mid_bin + win_size);
	double nevts_postcut_right = h_postcut->Integral(right_mid_bin - win_size,right_mid_bin + win_size);
    
	// calculate the number of signal events by subtracting the background
	double nevts_precut_sig = nevts_precut_central - (nevts_precut_left + nevts_precut_right)/2.;
	double nevts_postcut_sig = nevts_postcut_central - (nevts_postcut_left + nevts_postcut_right)/2.;
    
	// create the random number generator object
	auto rnd = new TRandom3();
    
	// create histogram to save efficiencies from pseudo-experiments
	auto heff = new TH1D(name,"Efficiencies",50,0.8,1.0);
	heff->GetXaxis()->SetTitle("Pseudo-Experiment Efficiencies");
	setLabelY(heff,"");
    
	// loop for the number of pseudo-experiments
	for(int i = 0;i < 10000;i++)
	{
        	// Poisson newuate the number of events in each region prior to the cut
        	int new_nevts_precut_central = rnd->Poisson(nevts_precut_central);
        	int new_nevts_precut_left = rnd->Poisson(nevts_precut_left);
        	int new_nevts_precut_right = rnd->Poisson(nevts_precut_right);

        	// subtract the background to get the number of signal events prior to the cut
        	double new_nevts_precut_sig = new_nevts_precut_central - (new_nevts_precut_left + new_nevts_precut_right)/2.;

        	// roll the die
        	double roll = rnd->Uniform();
        
        	// roll to see if the event passes
        	if(roll < ideal_eff)
		{
            		// if the event passes Poisson newuate the number of events in each region prior to the cut
            		int new_nevts_postcut_central = rnd->Poisson(nevts_postcut_central);
            		int new_nevts_postcut_left = rnd->Poisson(nevts_postcut_left);
            		int new_nevts_postcut_right = rnd->Poisson(nevts_postcut_right);

            		// subtract the background to get the number of signal events after the cut
            		double new_nevts_postcut_sig = new_nevts_postcut_central - (new_nevts_postcut_left + new_nevts_postcut_right)/2.;

            		// calculate the effciency
            		double eff = new_nevts_postcut_sig/new_nevts_precut_sig;
            
            		// fill the efficiency histogram
            		heff->Fill(eff);
		}
	}    

	// fit a gaussian to the histogram of efficiencies
	auto ferr = new TF1("ferr","gaus");
	ferr->SetTitle("Gaussian");
	heff->Fit("ferr","Q");   
    
	// return histogram of pseudo-experiment efficiencies
	return heff;
}

void kaovetEff()
{
	// start timer
	auto stopwatch = new TStopwatch();
	stopwatch->Start();

	// set LHCb style
	gROOT->ProcessLine(".L ../../lhcbStyle.C");

    	// get the ntuples
    	auto pA_data_file = new TFile("../../ntuple_maker/pA_mb_ntuple.root");
    	TNtuple* pA_nt = (TNtuple*)pA_data_file->Get("nt");
    	auto Ap_data_file = new TFile("../../ntuple_maker/Ap_mb_ntuple.root");
    	TNtuple* Ap_nt = (TNtuple*)Ap_data_file->Get("nt");

	// Selection Requirements
	TCut y_cut = "(2 < y) && (y < 5)"; // rapidity cut for the rho
	TCut phi_y_cut = "(2 < phi_y) && (phi_y < 5)"; // rapidity cut for the phi
	//TCut trck_type = "((trck1_type == 3) || (trck1_type == 4)) && ((trck2_type == 3) || (trck2_type == 4))"; // select long and upstream tracks
	TCut trck_type = "(trck1_type == 3) && (trck2_type == 3)"; // select long tracks
	TCut no_el = "((trck1_eop < 0.7) && (trck2_eop < 0.7))"; // cut electrons
	TCut no_pho = "(ngamma == 0)"; // photon-veto
	TCut os_cut = "((trck1_charge * trck2_charge) == -1)"; // opposite-sign
	TCut presel = y_cut&&trck_type&&os_cut&&no_pho&&no_el;

	// define decay distance cut
	TCut pA_dd = "(1 > sqrt(pow((xv - (0.862)),2.) + pow((yv - (-0.158)),2.)))";
	TCut Ap_dd = "(1 > sqrt(pow((xv - (0.881)),2.) + pow((yv - (-0.138)),2.)))";

	// define decay distance anti-cut
	TCut pA_antidd = "(1 < sqrt(pow((xv - (0.862)),2.) + pow((yv - (-0.158)),2.)))";
	TCut Ap_antidd = "(1 < sqrt(pow((xv - (0.881)),2.) + pow((yv - (-0.138)),2.)))";

	// define kaon veto
	TCut kv = "((trck1_richk <= 0) && (trck2_richk <= 0))";
	TCut antikv = "(!((trck1_richk <= 0) && (trck2_richk <= 0)))";

	// define herschel veto
	TCut her = "(her < 3.5)";

	// define cuts to narrow in on the K0
	TCut K0m_sel = "((400 < mass) && (mass < 600))";
	TCut pA_K0sel = y_cut&&trck_type&&os_cut&&no_pho&&no_el&&pA_antidd&&K0m_sel;
	TCut Ap_K0sel = y_cut&&trck_type&&os_cut&&no_pho&&no_el&&Ap_antidd&&K0m_sel;

	/////////////////////
	// make histograms //
	/////////////////////
	
	cout << "Making histograms..." << endl;

	auto hm_precut_list = new TList();
	auto hm_postcut_list = new TList();

	int hbins = 81;
	double hlow = 396.361;
	double hup = 598.861;

	auto pA_hm_precut = new TH1D("pA_hm_precut","p-Pb Data",hbins,hlow,hup);
	pA_nt->Project("pA_hm_precut","mass",presel&&pA_antidd);
	auto pA_hm_precut0 = new TH1D("pA_hm_precut0","p-Pb Data",hbins,hlow,hup); hm_precut_list->Add(pA_hm_precut0);
	pA_nt->Project("pA_hm_precut0","mass",presel&&pA_antidd&&"(y<2.5)");
	auto pA_hm_precut1 = new TH1D("pA_hm_precut1","p-Pb Data",hbins,hlow,hup); hm_precut_list->Add(pA_hm_precut1);
	pA_nt->Project("pA_hm_precut1","mass",presel&&pA_antidd&&"(2.5<y)&&(y<3)");
	auto pA_hm_precut2 = new TH1D("pA_hm_precut2","p-Pb Data",hbins,hlow,hup); hm_precut_list->Add(pA_hm_precut2);
	pA_nt->Project("pA_hm_precut2","mass",presel&&pA_antidd&&"(3<y)&&(y<3.5)");
	auto pA_hm_precut3 = new TH1D("pA_hm_precut3","p-Pb Data",hbins,hlow,hup); hm_precut_list->Add(pA_hm_precut3);
	pA_nt->Project("pA_hm_precut3","mass",presel&&pA_antidd&&"(3.5<y)&&(y<4)");
	auto pA_hm_precut4 = new TH1D("pA_hm_precut4","p-Pb Data",hbins,hlow,hup); hm_precut_list->Add(pA_hm_precut4);
	pA_nt->Project("pA_hm_precut4","mass",presel&&pA_antidd&&"(4<y)&&(y<4.5)");
	auto pA_hm_precut5 = new TH1D("pA_hm_precut5","p-Pb Data",hbins,hlow,hup); hm_precut_list->Add(pA_hm_precut5);
	pA_nt->Project("pA_hm_precut5","mass",presel&&pA_antidd&&"(4.5<y)");
	
	auto Ap_hm_precut = new TH1D("Ap_hm_precut","Pb-p Data",hbins,hlow,hup);
	Ap_nt->Project("Ap_hm_precut","mass",presel&&Ap_antidd);
	auto Ap_hm_precut0 = new TH1D("Ap_hm_precut0","Pb-p Data",hbins,hlow,hup); hm_precut_list->Add(Ap_hm_precut0);
	Ap_nt->Project("Ap_hm_precut0","mass",presel&&Ap_antidd&&"(y<2.5)");
	auto Ap_hm_precut1 = new TH1D("Ap_hm_precut1","Pb-p Data",hbins,hlow,hup); hm_precut_list->Add(Ap_hm_precut1);
	Ap_nt->Project("Ap_hm_precut1","mass",presel&&Ap_antidd&&"(2.5<y)&&(y<3)");
	auto Ap_hm_precut2 = new TH1D("Ap_hm_precut2","Pb-p Data",hbins,hlow,hup); hm_precut_list->Add(Ap_hm_precut2);
	Ap_nt->Project("Ap_hm_precut2","mass",presel&&Ap_antidd&&"(3<y)&&(y<3.5)");
	auto Ap_hm_precut3 = new TH1D("Ap_hm_precut3","Pb-p Data",hbins,hlow,hup); hm_precut_list->Add(Ap_hm_precut3);
	Ap_nt->Project("Ap_hm_precut3","mass",presel&&Ap_antidd&&"(3.5<y)&&(y<4)");
	auto Ap_hm_precut4 = new TH1D("Ap_hm_precut4","Pb-p Data",hbins,hlow,hup); hm_precut_list->Add(Ap_hm_precut4);
	Ap_nt->Project("Ap_hm_precut4","mass",presel&&Ap_antidd&&"(4<y)&&(y<4.5)");
	auto Ap_hm_precut5 = new TH1D("Ap_hm_precut5","Pb-p Data",hbins,hlow,hup); hm_precut_list->Add(Ap_hm_precut5);
	Ap_nt->Project("Ap_hm_precut5","mass",presel&&Ap_antidd&&"(4.5<y)");

	auto pA_hm_postcut = new TH1D("pA_hm_postcut","p-Pb Data",hbins,hlow,hup);
	pA_nt->Project("pA_hm_postcut","mass",presel&&pA_antidd&&kv);
	auto pA_hm_postcut0 = new TH1D("pA_hm_postcut0","p-Pb Data",hbins,hlow,hup); hm_postcut_list->Add(pA_hm_postcut0);
	pA_nt->Project("pA_hm_postcut0","mass",presel&&pA_antidd&&"(y<2.5)"&&kv);
	auto pA_hm_postcut1 = new TH1D("pA_hm_postcut1","p-Pb Data",hbins,hlow,hup); hm_postcut_list->Add(pA_hm_postcut1);
	pA_nt->Project("pA_hm_postcut1","mass",presel&&pA_antidd&&"(2.5<y)&&(y<3)"&&kv);
	auto pA_hm_postcut2 = new TH1D("pA_hm_postcut2","p-Pb Data",hbins,hlow,hup); hm_postcut_list->Add(pA_hm_postcut2);
	pA_nt->Project("pA_hm_postcut2","mass",presel&&pA_antidd&&"(3<y)&&(y<3.5)"&&kv);
	auto pA_hm_postcut3 = new TH1D("pA_hm_postcut3","p-Pb Data",hbins,hlow,hup); hm_postcut_list->Add(pA_hm_postcut3);
	pA_nt->Project("pA_hm_postcut3","mass",presel&&pA_antidd&&"(3.5<y)&&(y<4)"&&kv);
	auto pA_hm_postcut4 = new TH1D("pA_hm_postcut4","p-Pb Data",hbins,hlow,hup); hm_postcut_list->Add(pA_hm_postcut4);
	pA_nt->Project("pA_hm_postcut4","mass",presel&&pA_antidd&&"(4<y)&&(y<4.5)"&&kv);
	auto pA_hm_postcut5 = new TH1D("pA_hm_postcut5","p-Pb Data",hbins,hlow,hup); hm_postcut_list->Add(pA_hm_postcut5);
	pA_nt->Project("pA_hm_postcut5","mass",presel&&pA_antidd&&"(4.5<y)"&&kv);

	auto Ap_hm_postcut = new TH1D("Ap_hm_postcut","Pb-p Data",hbins,hlow,hup);
	Ap_nt->Project("Ap_hm_postcut","mass",presel&&Ap_antidd&&kv);
	auto Ap_hm_postcut0 = new TH1D("Ap_hm_postcut0","Pb-p Data",hbins,hlow,hup); hm_postcut_list->Add(Ap_hm_postcut0);
	Ap_nt->Project("Ap_hm_postcut0","mass",presel&&Ap_antidd&&"(y<2.5)"&&kv);
	auto Ap_hm_postcut1 = new TH1D("Ap_hm_postcut1","Pb-p Data",hbins,hlow,hup); hm_postcut_list->Add(Ap_hm_postcut1);
	Ap_nt->Project("Ap_hm_postcut1","mass",presel&&Ap_antidd&&"(2.5<y)&&(y<3)"&&kv);
	auto Ap_hm_postcut2 = new TH1D("Ap_hm_postcut2","Pb-p Data",hbins,hlow,hup); hm_postcut_list->Add(Ap_hm_postcut2);
	Ap_nt->Project("Ap_hm_postcut2","mass",presel&&Ap_antidd&&"(3<y)&&(y<3.5)"&&kv);
	auto Ap_hm_postcut3 = new TH1D("Ap_hm_postcut3","Pb-p Data",hbins,hlow,hup); hm_postcut_list->Add(Ap_hm_postcut3);
	Ap_nt->Project("Ap_hm_postcut3","mass",presel&&Ap_antidd&&"(3.5<y)&&(y<4)"&&kv);
	auto Ap_hm_postcut4 = new TH1D("Ap_hm_postcut4","Pb-p Data",hbins,hlow,hup); hm_postcut_list->Add(Ap_hm_postcut4);
	Ap_nt->Project("Ap_hm_postcut4","mass",presel&&Ap_antidd&&"(4<y)&&(y<4.5)"&&kv);
	auto Ap_hm_postcut5 = new TH1D("Ap_hm_postcut5","Pb-p Data",hbins,hlow,hup); hm_postcut_list->Add(Ap_hm_postcut5);
	Ap_nt->Project("Ap_hm_postcut5","mass",presel&&Ap_antidd&&"(4.5<y)"&&kv);

	//////////////////////////////
	// Plotting and Calculating //
	//////////////////////////////

	cout << "Plotting and calculating..." << endl;

	// pA distributions for all y

	auto hm_precut_ally_cvs = new TCanvas("hm_precut_ally_cvs","Pre-Veto Distributions");
	hm_precut_ally_cvs->Divide(1,2);
	hm_precut_ally_cvs->cd(1);
	pA_hm_precut->SetMarkerColor(kBlue);
	pA_hm_precut->SetLineColor(kBlue);
	pA_hm_precut->GetXaxis()->SetTitle("m_{#pi#pi} [MeV]");
	setLabelY(pA_hm_precut,"MeV");
	pA_hm_precut->Draw("E1");
	double pA_win_size_MeV = win_size*(pA_hm_precut->GetXaxis()->GetXmax() - pA_hm_precut->GetXaxis()->GetXmin())/pA_hm_precut->GetNbinsX()+1.25;
	auto pA_left_mass_line = new TLine(K0_mass - pA_win_size_MeV,0,K0_mass - pA_win_size_MeV,pA_hm_precut->GetMaximum());
	auto pA_right_mass_line = new TLine(K0_mass + pA_win_size_MeV,0,K0_mass + pA_win_size_MeV,pA_hm_precut->GetMaximum());
	auto pA_left_lower_line = new TLine(left_centre - pA_win_size_MeV,0,left_centre - pA_win_size_MeV,pA_hm_precut->GetMaximum());
	auto pA_right_lower_line = new TLine(left_centre + pA_win_size_MeV,0,left_centre + pA_win_size_MeV,pA_hm_precut->GetMaximum());
	auto pA_left_upper_line = new TLine(right_centre - pA_win_size_MeV,0,right_centre - pA_win_size_MeV,pA_hm_precut->GetMaximum());
	auto pA_right_upper_line = new TLine(right_centre + pA_win_size_MeV,0,right_centre + pA_win_size_MeV,pA_hm_precut->GetMaximum());
	pA_left_mass_line->Draw("SAME");
	pA_right_mass_line->Draw("SAME");
	pA_left_lower_line->Draw("SAME");
	pA_right_lower_line->Draw("SAME");
	pA_left_upper_line->Draw("SAME");
	pA_right_upper_line->Draw("SAME");

	// Ap distributions for all y

	hm_precut_ally_cvs->cd(2);
	Ap_hm_precut->SetMarkerColor(kRed);
	Ap_hm_precut->SetLineColor(kRed);
	Ap_hm_precut->GetXaxis()->SetTitle("m_{#pi#pi} [MeV]");
	setLabelY(Ap_hm_precut,"MeV");
	Ap_hm_precut->Draw("E1");
	double Ap_win_size_MeV = win_size*(Ap_hm_precut->GetXaxis()->GetXmax() - Ap_hm_precut->GetXaxis()->GetXmin())/Ap_hm_precut->GetNbinsX()+1.25;
	auto Ap_left_mass_line = new TLine(K0_mass - Ap_win_size_MeV,0,K0_mass - Ap_win_size_MeV,Ap_hm_precut->GetMaximum());
	auto Ap_right_mass_line = new TLine(K0_mass + Ap_win_size_MeV,0,K0_mass + Ap_win_size_MeV,Ap_hm_precut->GetMaximum());
	auto Ap_left_lower_line = new TLine(left_centre - Ap_win_size_MeV,0,left_centre - Ap_win_size_MeV,Ap_hm_precut->GetMaximum());
	auto Ap_right_lower_line = new TLine(left_centre + Ap_win_size_MeV,0,left_centre + Ap_win_size_MeV,Ap_hm_precut->GetMaximum());
	auto Ap_left_upper_line = new TLine(right_centre - Ap_win_size_MeV,0,right_centre - Ap_win_size_MeV,Ap_hm_precut->GetMaximum());
	auto Ap_right_upper_line = new TLine(right_centre + Ap_win_size_MeV,0,right_centre + Ap_win_size_MeV,Ap_hm_precut->GetMaximum());
	Ap_left_mass_line->Draw("SAME");
	Ap_right_mass_line->Draw("SAME");
	Ap_left_lower_line->Draw("SAME");
	Ap_right_lower_line->Draw("SAME");
	Ap_left_upper_line->Draw("SAME");
	Ap_right_upper_line->Draw("SAME");

	auto hm_postcut_ally_cvs = new TCanvas("hm_postcut_ally_cvs","Post-Veto Distributions");
	hm_postcut_ally_cvs->Divide(1,2);
	hm_postcut_ally_cvs->cd(1);
	pA_hm_postcut->SetMarkerColor(kBlue);
	pA_hm_postcut->SetLineColor(kBlue);
	pA_hm_postcut->GetXaxis()->SetTitle("m_{#pi#pi} [MeV]");
	setLabelY(pA_hm_postcut,"MeV");
	pA_hm_postcut->Draw("E1");
	hm_postcut_ally_cvs->cd(2);
	Ap_hm_postcut->SetMarkerColor(kRed);
	Ap_hm_postcut->SetLineColor(kRed);
	Ap_hm_postcut->GetXaxis()->SetTitle("m_{#pi#pi} [MeV]");
	setLabelY(Ap_hm_postcut,"MeV");
	Ap_hm_postcut->Draw("E1");
	
	// declare and initialise canvases for plotting in y bins
	auto hm_precut_cvs = new TCanvas("hm_precut_cvs","Pre-Veto Distributions in bins of rapidity");
	hm_precut_cvs->Divide(4,3);

	auto hm_postcut_cvs = new TCanvas("hm_postcut_cvs","Post-Veto Distributions in bins of rapidity");
	hm_postcut_cvs->Divide(4,3);

	// declare and initialise graph list and multigraph
	auto gr_list = new TList();
	auto ymgr_eff = new TMultiGraph();
	ymgr_eff->SetName("ymgr_eff");

	// set range for dataset specific graphs
	double y_range[6] = {2.25,2.75,3.25,3.75,4.25,4.75}, y_range_err[6] = {0,0,0,0,0,0}, eff_range[6] = {0,0,0,0,0,0}, eff_range_err[6] = {0,0,0,0,0,0};
		
	// loop over dataset
	for(int dat_itr = 0;dat_itr < 2;dat_itr++)
	{
		// set aesthetics for dataset
		auto colour = kBlue;
		TString dat_string = "pA_";
		TString leg_string = "p-Pb Data";
		if(dat_itr == 1)
		{
			colour = kRed;
			dat_string = "Ap_";
			leg_string = "Pb-p Data";
		}

		// loop over rapidity bins
		for(int y_itr = 0;y_itr < 6;y_itr++)
		{
			// calculate the efficiency
			double nevts_precut, nevts_precut_err, nevts_postcut, nevts_postcut_err,pevnt_eff, pevnt_eff_err;
			effSidebandSub((TH1D*)hm_precut_list->At(y_itr+6*dat_itr),(TH1D*)hm_postcut_list->At(y_itr+6*dat_itr),nevts_postcut,nevts_postcut_err,nevts_precut,nevts_precut_err,pevnt_eff,pevnt_eff_err);
			// save results
			eff_range[y_itr] = pevnt_eff; eff_range_err[y_itr] = pevnt_eff_err;
			precut_array[y_itr+7*dat_itr] = nevts_precut; precut_array_err[y_itr+7*dat_itr] = nevts_precut_err;
			postcut_array[y_itr+7*dat_itr] = nevts_postcut; postcut_array_err[y_itr+7*dat_itr] = nevts_postcut_err;
			eff_array[y_itr+7*dat_itr] = pevnt_eff; eff_array_err[y_itr+7*dat_itr] = pevnt_eff_err;

			// plot distributions
			hm_precut_cvs->cd(y_itr+6*dat_itr+1);
			((TH1D*)hm_precut_list->At(y_itr+6*dat_itr))->SetMarkerColor(colour);
			((TH1D*)hm_precut_list->At(y_itr+6*dat_itr))->SetLineColor(colour);
			((TH1D*)hm_precut_list->At(y_itr+6*dat_itr))->GetXaxis()->SetTitle("m_{#pi#pi} [MeV]");		
			setLabelY(((TH1D*)hm_precut_list->At(y_itr+6*dat_itr)),"MeV");		
			((TH1D*)hm_precut_list->At(y_itr+6*dat_itr))->Draw("E1");		
			hm_postcut_cvs->cd(y_itr+6*dat_itr+1);
			((TH1D*)hm_postcut_list->At(y_itr+6*dat_itr))->SetMarkerColor(colour);
			((TH1D*)hm_postcut_list->At(y_itr+6*dat_itr))->SetLineColor(colour);
			((TH1D*)hm_postcut_list->At(y_itr+6*dat_itr))->GetXaxis()->SetTitle("m_{#pi#pi} [MeV]");		
			setLabelY(((TH1D*)hm_postcut_list->At(y_itr+6*dat_itr)),"MeV");		
			((TH1D*)hm_postcut_list->At(y_itr+6*dat_itr))->Draw("E1");		
		}

		// plot efficiencies in a graph
		auto ygr_eff = new TGraphErrors(6,y_range,eff_range,y_range_err,eff_range_err);
		ygr_eff->SetName(dat_string + "ygr_eff");
		ygr_eff->SetTitle(leg_string);
		ygr_eff->SetMarkerColor(colour);
		ygr_eff->SetLineColor(colour);
		ymgr_eff->Add(ygr_eff);
		gr_list->Add(ygr_eff);
	}

	// calculate pA efficiency across all y bins
	double pA_nevts_precut, pA_nevts_precut_err, pA_nevts_postcut, pA_nevts_postcut_err,pA_eff, pA_eff_err;
	effSidebandSub(pA_hm_precut,pA_hm_postcut,pA_nevts_postcut,pA_nevts_postcut_err,pA_nevts_precut,pA_nevts_precut_err,pA_eff,pA_eff_err);
	printf("pA kaon-veto efficiency: %f +/- %f\n",pA_eff,pA_eff_err);
	precut_array[6] = pA_nevts_precut; precut_array_err[6] = pA_nevts_precut_err;
	postcut_array[6] = pA_nevts_postcut; postcut_array_err[6] = pA_nevts_postcut_err;
	eff_array[6] = pA_eff; eff_array_err[6] = pA_eff_err;
	// error sanity check
	auto eff_herr_cvs = new TCanvas("eff_herr_cvs","Data Error Sanity Check");
	eff_herr_cvs->Divide(2,1);
	eff_herr_cvs->cd(1);
	auto pA_name = "pA_eff_herr";
	auto pA_eff_herr = errHistSidebandSub(pA_name,pA_hm_precut,pA_hm_postcut);
	pA_eff_herr->SetMarkerColor(kBlue);
	pA_eff_herr->SetLineColor(kBlue);
	pA_eff_herr->Draw("E1");

	// calculate pA efficiency across all y bins
	double Ap_nevts_precut, Ap_nevts_precut_err, Ap_nevts_postcut, Ap_nevts_postcut_err,Ap_eff, Ap_eff_err;
	effSidebandSub(Ap_hm_precut,Ap_hm_postcut,Ap_nevts_postcut,Ap_nevts_postcut_err,Ap_nevts_precut,Ap_nevts_precut_err,Ap_eff,Ap_eff_err);
	printf("Ap kaon-veto efficiency: %f +/- %f\n",Ap_eff,Ap_eff_err);
	precut_array[13] = Ap_nevts_precut; precut_array_err[13] = Ap_nevts_precut_err;
	postcut_array[13] = Ap_nevts_postcut; postcut_array_err[13] = Ap_nevts_postcut_err;
	eff_array[13] = Ap_eff; eff_array_err[13] = Ap_eff_err;
	// error sanity check
	eff_herr_cvs->cd(2);
	auto Ap_name = "Ap_eff_herr";
	auto Ap_eff_herr = errHistSidebandSub(Ap_name,Ap_hm_precut,Ap_hm_postcut);
	Ap_eff_herr->SetMarkerColor(kRed);
	Ap_eff_herr->SetLineColor(kRed);
	Ap_eff_herr->GetXaxis()->SetTitle("Pseudo-Experiment Efficiencies");
	setLabelY(Ap_eff_herr,"");
	Ap_eff_herr->Draw("E1");

	// print the average between the two dataset efficiencies across all y bins
	double eff_avg = (pA_eff + Ap_eff)/2;
	double eff_avg_err = (sqrt(pow(pA_eff_err,2) + pow(Ap_eff_err,2)))/2;
	printf("Average kaon-veto efficiency: %f +/- %f\n",eff_avg,eff_avg_err);

	// plot all the efficiencies together in a multi-graph
	auto ymgr_eff_cvs = new TCanvas("ymgr_eff_cvs","Kaon-Veto Efficiency in bins of rapidity");
	ymgr_eff->GetXaxis()->SetTitle("y_{#pi#pi}");
	ymgr_eff->GetYaxis()->SetTitle("#varepsilon_{kaon-veto}");
	ymgr_eff->Draw("AP");

	// plot the average efficiency between the datasets for each bin
	auto ygr_eff_avg_cvs = new TCanvas("ygr_eff_avg_cvs","Average Kaon-Veto Efficiency in bins of rapidity");
	for(int i = 0; i < 6;i++)
	{
		eff_avg_array[i] = (eff_array[i] + eff_array[i+7])/2;
		eff_avg_array_err[i] = sqrt(pow(eff_array_err[i],2) + pow(eff_array_err[i+7],2))/2;
	}
	auto ygr_eff_avg = new TGraphErrors(6,y_range,eff_avg_array,y_range_err,eff_avg_array_err);
	ygr_eff_avg->SetName("ygr_eff_avg");
	ygr_eff_avg->GetXaxis()->SetTitle("y_{#pi#pi}");
	ygr_eff_avg->GetYaxis()->SetTitle("#varepsilon_{kaon-veto}");
	ygr_eff_avg->Draw("AP");

	///////////////////////////////////////
	// plot the accepted/rejected events //
	///////////////////////////////////////

	cout << "Making and plotting histograms for the accepted/rejected events..." << endl;
	auto haccrej_list = new TList();

	auto kv_cvs = new TCanvas("kv_cvs","Kaon-Veto Accepted/Rejected");
	kv_cvs->Divide(2,2);

	// p-Pb accepted events
	auto pA_hm_kv = new TH1D("pA_hm_kv","p-Pb Data",25,975,1100);
	pA_nt->Project("pA_hm_kv","phi_M",presel&&pA_dd&&kv);
	pA_hm_kv->SetMarkerColor(kBlue);
	pA_hm_kv->SetLineColor(kBlue);
	pA_hm_kv->GetXaxis()->SetTitle("m_{KK} [MeV]");
	setLabelY(pA_hm_kv,"MeV");
	haccrej_list->Add(pA_hm_kv);
	kv_cvs->cd(1);
	pA_hm_kv->Draw("E1");	

	// p-Pb rejected events
	auto pA_hm_akv = new TH1D("pA_hm_akv","p-Pb Data",25,975,1100);
	pA_nt->Project("pA_hm_akv","phi_M",presel&&pA_dd&&antikv);
	pA_hm_akv->SetMarkerColor(kBlue);
	pA_hm_akv->SetLineColor(kBlue);
	pA_hm_akv->GetXaxis()->SetTitle("m_{KK} [MeV]");
	setLabelY(pA_hm_akv,"MeV");
	haccrej_list->Add(pA_hm_akv);
	kv_cvs->cd(2);
	pA_hm_akv->Draw("E1");	

	// Pb-p accepted events
	auto Ap_hm_kv = new TH1D("Ap_hm_kv","p-Pb Data",25,975,1100);
	Ap_nt->Project("Ap_hm_kv","phi_M",presel&&Ap_dd&&kv);
	Ap_hm_kv->SetMarkerColor(kRed);
	Ap_hm_kv->SetLineColor(kRed);
	Ap_hm_kv->GetXaxis()->SetTitle("m_{KK} [MeV]");
	setLabelY(Ap_hm_kv,"MeV");
	haccrej_list->Add(Ap_hm_kv);
	kv_cvs->cd(3);
	Ap_hm_kv->Draw("E1");	

	// Pb-p rejected events
	auto Ap_hm_akv = new TH1D("Ap_hm_akv","p-Pb Data",25,975,1100);
	Ap_nt->Project("Ap_hm_akv","phi_M",presel&&Ap_dd&&antikv);
	Ap_hm_akv->SetMarkerColor(kRed);
	Ap_hm_akv->SetLineColor(kRed);
	Ap_hm_akv->GetXaxis()->SetTitle("m_{KK} [MeV]");
	setLabelY(Ap_hm_akv,"MeV");
	haccrej_list->Add(Ap_hm_akv);
	kv_cvs->cd(4);
	Ap_hm_akv->Draw("E1");

	// create an output file
	auto output_name = "output.root";
	auto output_file = new TFile(output_name,"RECREATE");
	cout << "An output file " << output_name  <<  " has been created..." << endl;
	cout << "Writing output to file..." << endl;
	pA_hm_precut->Write();
	pA_hm_postcut->Write();
	Ap_hm_precut->Write();
	Ap_hm_postcut->Write();
	pA_eff_herr->Write();
	Ap_eff_herr->Write();
	hm_precut_list->Write();
	hm_postcut_list->Write();
	gr_list->Write();
	ymgr_eff->Write();
	haccrej_list->Write();
	ygr_eff_avg->Write();
	cout << "We're done!" << endl;
}

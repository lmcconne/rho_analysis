//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Jul 23 20:41:11 2021 by ROOT version 6.22/08
// from TChain TupleTools/MyTuple/
//////////////////////////////////////////////////////////

#ifndef makeCalSam_pA_h
#define makeCalSam_pA_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class makeCalSam_pA {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          BCID;
   Int_t           BCType;
   UInt_t          OdinTCK;
   UInt_t          L0DUTCK;
   UInt_t          HLT1TCK;
   UInt_t          HLT2TCK;
   ULong64_t       GpsTime;
   Short_t         Polarity;
   Int_t           nPVs;
   Int_t           nTracks;
   Int_t           nLongTracks;
   Int_t           nDownstreamTracks;
   Int_t           nUpstreamTracks;
   Int_t           nVeloTracks;
   Int_t           nTTracks;
   Int_t           nBackTracks;
   Int_t           nRich1Hits;
   Int_t           nRich2Hits;
   Int_t           nVeloClusters;
   Int_t           nITClusters;
   Int_t           nTTClusters;
   Int_t           nOTClusters;
   Int_t           nSPDHits;
   Int_t           nMuonCoordsS0;
   Int_t           nMuonCoordsS1;
   Int_t           nMuonCoordsS2;
   Int_t           nMuonCoordsS3;
   Int_t           nMuonCoordsS4;
   Int_t           nMuonTracks;
   Int_t           B00;
   Int_t           B01;
   Int_t           B02;
   Int_t           B03;
   Int_t           B10;
   Int_t           B11;
   Int_t           B12;
   Int_t           B13;
   Int_t           B20;
   Int_t           B21;
   Int_t           B22;
   Int_t           B23;
   Int_t           F10;
   Int_t           F11;
   Int_t           F12;
   Int_t           F13;
   Int_t           F20;
   Int_t           F21;
   Int_t           F22;
   Int_t           F23;
   Double_t        log_hrc_fom_v4;
   Double_t        log_hrc_fom_B_v4;
   Double_t        log_hrc_fom_F_v4;
   Int_t           nchB;
   Float_t         adc_B[1000];   //[nchB]
   Int_t           nchF;
   Float_t         adc_F[1000];   //[nchF]
   Int_t           L0Global;
   UInt_t          Hlt1Global;
   UInt_t          Hlt2Global;
   Int_t           L0Muon_lowMultDecision;
   Int_t           L0DiMuon_lowMultDecision;
   Int_t           L0DiHadron_lowMultDecision;
   Int_t           L0Electron_lowMultDecision;
   Int_t           L0Photon_lowMultDecision;
   Int_t           L0DiEM_lowMultDecision;
   UInt_t          L0nSelections;
   Int_t           Hlt1NoPVPassThroughDecision;
   Int_t           Hlt1LowMultPassThroughDecision;
   Int_t           Hlt1NoBiasNonBeamBeamDecision;
   Int_t           Hlt1MBNoBiasRateLimitedDecision;
   Int_t           Hlt1MBNoBiasDecision;
   Int_t           Hlt1CEPVeloCutDecision;
   Int_t           Hlt1LowMultDecision;
   Int_t           Hlt1LowMultMuonDecision;
   Int_t           Hlt1LowMultHerschelDecision;
   Int_t           Hlt1LowMultVeloCut_LeptonsDecision;
   Int_t           Hlt1LowMultVeloAndHerschel_LeptonsDecision;
   UInt_t          Hlt1nSelections;
   Int_t           Hlt2NoBiasNonBeamBeamDecision;
   Int_t           Hlt2LowMultLMR2HHDecision;
   Int_t           Hlt2LowMultLMR2HHWSDecision;
   Int_t           Hlt2LowMultLMR2HHHHDecision;
   Int_t           Hlt2LowMultLMR2HHHHWSDecision;
   Int_t           Hlt2LowMultL2pPiDecision;
   Int_t           Hlt2LowMultD2KPiDecision;
   Int_t           Hlt2LowMultD2KPiPiDecision;
   Int_t           Hlt2LowMultD2KKPiDecision;
   Int_t           Hlt2LowMultD2K3PiDecision;
   Int_t           Hlt2LowMultChiC2HHDecision;
   Int_t           Hlt2LowMultChiC2HHHHDecision;
   Int_t           Hlt2LowMultChiC2PPDecision;
   Int_t           Hlt2LowMultHadron_noTrFiltDecision;
   Int_t           Hlt2LowMultL2pPiWSDecision;
   Int_t           Hlt2LowMultD2KPiWSDecision;
   Int_t           Hlt2LowMultD2KPiPiWSDecision;
   Int_t           Hlt2LowMultD2KKPiWSDecision;
   Int_t           Hlt2LowMultD2K3PiWSDecision;
   Int_t           Hlt2LowMultChiC2HHWSDecision;
   Int_t           Hlt2LowMultChiC2HHHHWSDecision;
   Int_t           Hlt2LowMultChiC2PPWSDecision;
   Int_t           Hlt2LowMultMuonDecision;
   Int_t           Hlt2LowMultDiMuonDecision;
   Int_t           Hlt2LowMultDiMuon_PSDecision;
   Int_t           Hlt2LowMultDiPhotonDecision;
   Int_t           Hlt2LowMultDiPhoton_HighMassDecision;
   Int_t           Hlt2LowMultpi0Decision;
   Int_t           Hlt2LowMultDiElectronDecision;
   Int_t           Hlt2LowMultDiElectron_noTrFiltDecision;
   Int_t           Hlt2MBMicroBiasVeloDecision;
   Int_t           Hlt2LowMultTechnical_MinBiasDecision;
   UInt_t          Hlt2nSelections;
   Int_t           MaxRoutingBits;
   Float_t         RoutingBits[64];   //[MaxRoutingBits]
   UInt_t          StrippingMBNoBiasDecision;
   UInt_t          StrippingHlt1NoBiasNonBeamBeamLineDecision;
   UInt_t          StrippingLowMultNonBeamBeamNoBiasLineDecision;
   UInt_t          StrippingLowMultMuonLineDecision;
   UInt_t          StrippingLowMultDiMuonLineDecision;
   UInt_t          StrippingHeavyIonTopologyLowActivityLineDecision;
   Float_t         ET_prev1;
   Float_t         ET_prev2;
   Int_t           nouttrck;
   Float_t         trck_key[10];   //[nouttrck]
   Float_t         trck_type[10];   //[nouttrck]
   Float_t         trck_posx[10];   //[nouttrck]
   Float_t         trck_posy[10];   //[nouttrck]
   Float_t         trck_posz[10];   //[nouttrck]
   Float_t         trck_px[10];   //[nouttrck]
   Float_t         trck_py[10];   //[nouttrck]
   Float_t         trck_pz[10];   //[nouttrck]
   Float_t         trck_charge[10];   //[nouttrck]
   Float_t         trck_muonid[10];   //[nouttrck]
   Float_t         trck_riche[10];   //[nouttrck]
   Float_t         trck_richk[10];   //[nouttrck]
   Float_t         trck_richp[10];   //[nouttrck]
   Float_t         trck_muonll[10];   //[nouttrck]
   Float_t         trck_muacc[10];   //[nouttrck]
   Float_t         trck_ecal[10];   //[nouttrck]
   Float_t         trck_hcal[10];   //[nouttrck]
   Float_t         trck_eop[10];   //[nouttrck]
   Float_t         trck_probe[10];   //[nouttrck]
   Float_t         trck_probmu[10];   //[nouttrck]
   Float_t         trck_probpi[10];   //[nouttrck]
   Float_t         trck_probk[10];   //[nouttrck]
   Float_t         trck_probp[10];   //[nouttrck]
   Float_t         trck_probghost[10];   //[nouttrck]
   Int_t           noutmuon;
   Float_t         muon_chi2ndf[10];   //[noutmuon]
   Float_t         muon_posx[10];   //[noutmuon]
   Float_t         muon_posy[10];   //[noutmuon]
   Float_t         muon_ux[10];   //[noutmuon]
   Float_t         muon_uy[10];   //[noutmuon]
   Float_t         muon_nshared[10];   //[noutmuon]
   Int_t           nphotons;
   Int_t           noutphot;
   Float_t         photon_px[10];   //[noutphot]
   Float_t         photon_py[10];   //[noutphot]
   Float_t         photon_pz[10];   //[noutphot]
   Float_t         photon_nspd[10];   //[noutphot]
   Float_t         photon_nprs[10];   //[noutphot]
   Int_t           noutspd;
   Float_t         spd_posx[30];   //[noutspd]
   Float_t         spd_posy[30];   //[noutspd]
   Int_t           noutprs;
   Float_t         prs_posx[30];   //[noutprs]
   Float_t         prs_posy[30];   //[noutprs]
   Int_t           noutl0mu;
   Float_t         l0mu_pt[10];   //[noutl0mu]
   Float_t         l0mu_phi[10];   //[noutl0mu]
   Float_t         l0mu_theta[10];   //[noutl0mu]
   Int_t           noutl0h;
   Float_t         l0h_et[10];   //[noutl0h]
   Int_t           noutl2mu;
   Float_t         hlt2mu_posx[10];   //[noutl2mu]
   Float_t         hlt2mu_posy[10];   //[noutl2mu]
   Float_t         hlt2mu_posz[10];   //[noutl2mu]
   Int_t           noutl1mu;
   Float_t         hlt1mu_posx[10];   //[noutl1mu]
   Float_t         hlt1mu_posy[10];   //[noutl1mu]
   Float_t         hlt1mu_posz[10];   //[noutl1mu]
   Float_t         hlt1mu_slopex[10];   //[noutl1mu]
   Float_t         hlt1mu_slopey[10];   //[noutl1mu]

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_BCType;   //!
   TBranch        *b_OdinTCK;   //!
   TBranch        *b_L0DUTCK;   //!
   TBranch        *b_HLT1TCK;   //!
   TBranch        *b_HLT2TCK;   //!
   TBranch        *b_GpsTime;   //!
   TBranch        *b_Polarity;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nLongTracks;   //!
   TBranch        *b_nDownstreamTracks;   //!
   TBranch        *b_nUpstreamTracks;   //!
   TBranch        *b_nVeloTracks;   //!
   TBranch        *b_nTTracks;   //!
   TBranch        *b_nBackTracks;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nVeloClusters;   //!
   TBranch        *b_nITClusters;   //!
   TBranch        *b_nTTClusters;   //!
   TBranch        *b_nOTClusters;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_nMuonCoordsS0;   //!
   TBranch        *b_nMuonCoordsS1;   //!
   TBranch        *b_nMuonCoordsS2;   //!
   TBranch        *b_nMuonCoordsS3;   //!
   TBranch        *b_nMuonCoordsS4;   //!
   TBranch        *b_nMuonTracks;   //!
   TBranch        *b_B00;   //!
   TBranch        *b_B01;   //!
   TBranch        *b_B02;   //!
   TBranch        *b_B03;   //!
   TBranch        *b_B10;   //!
   TBranch        *b_B11;   //!
   TBranch        *b_B12;   //!
   TBranch        *b_B13;   //!
   TBranch        *b_B20;   //!
   TBranch        *b_B21;   //!
   TBranch        *b_B22;   //!
   TBranch        *b_B23;   //!
   TBranch        *b_F10;   //!
   TBranch        *b_F11;   //!
   TBranch        *b_F12;   //!
   TBranch        *b_F13;   //!
   TBranch        *b_F20;   //!
   TBranch        *b_F21;   //!
   TBranch        *b_F22;   //!
   TBranch        *b_F23;   //!
   TBranch        *b_log_hrc_fom_v4;   //!
   TBranch        *b_log_hrc_fom_B_v4;   //!
   TBranch        *b_log_hrc_fom_F_v4;   //!
   TBranch        *b_nchB;   //!
   TBranch        *b_adc_B;   //!
   TBranch        *b_nchF;   //!
   TBranch        *b_adc_F;   //!
   TBranch        *b_L0Global;   //!
   TBranch        *b_Hlt1Global;   //!
   TBranch        *b_Hlt2Global;   //!
   TBranch        *b_L0Muon_lowMultDecision;   //!
   TBranch        *b_L0DiMuon_lowMultDecision;   //!
   TBranch        *b_L0DiHadron_lowMultDecision;   //!
   TBranch        *b_L0Electron_lowMultDecision;   //!
   TBranch        *b_L0Photon_lowMultDecision;   //!
   TBranch        *b_L0DiEM_lowMultDecision;   //!
   TBranch        *b_L0nSelections;   //!
   TBranch        *b_Hlt1NoPVPassThroughDecision;   //!
   TBranch        *b_Hlt1LowMultPassThroughDecision;   //!
   TBranch        *b_Hlt1NoBiasNonBeamBeamDecision;   //!
   TBranch        *b_Hlt1MBNoBiasRateLimitedDecision;   //!
   TBranch        *b_Hlt1MBNoBiasDecision;   //!
   TBranch        *b_Hlt1CEPVeloCutDecision;   //!
   TBranch        *b_Hlt1LowMultDecision;   //!
   TBranch        *b_Hlt1LowMultMuonDecision;   //!
   TBranch        *b_Hlt1LowMultHerschelDecision;   //!
   TBranch        *b_Hlt1LowMultVeloCut_LeptonsDecision;   //!
   TBranch        *b_Hlt1LowMultVeloAndHerschel_LeptonsDecision;   //!
   TBranch        *b_Hlt1nSelections;   //!
   TBranch        *b_Hlt2NoBiasNonBeamBeamDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHWSDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHHHDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHHHWSDecision;   //!
   TBranch        *b_Hlt2LowMultL2pPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2KKPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2K3PiDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHHHDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2PPDecision;   //!
   TBranch        *b_Hlt2LowMultHadron_noTrFiltDecision;   //!
   TBranch        *b_Hlt2LowMultL2pPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2KKPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2K3PiWSDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHWSDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHHHWSDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2PPWSDecision;   //!
   TBranch        *b_Hlt2LowMultMuonDecision;   //!
   TBranch        *b_Hlt2LowMultDiMuonDecision;   //!
   TBranch        *b_Hlt2LowMultDiMuon_PSDecision;   //!
   TBranch        *b_Hlt2LowMultDiPhotonDecision;   //!
   TBranch        *b_Hlt2LowMultDiPhoton_HighMassDecision;   //!
   TBranch        *b_Hlt2LowMultpi0Decision;   //!
   TBranch        *b_Hlt2LowMultDiElectronDecision;   //!
   TBranch        *b_Hlt2LowMultDiElectron_noTrFiltDecision;   //!
   TBranch        *b_Hlt2MBMicroBiasVeloDecision;   //!
   TBranch        *b_Hlt2LowMultTechnical_MinBiasDecision;   //!
   TBranch        *b_Hlt2nSelections;   //!
   TBranch        *b_MaxRoutingBits;   //!
   TBranch        *b_RoutingBits;   //!
   TBranch        *b_StrippingMBNoBiasDecision;   //!
   TBranch        *b_StrippingHlt1NoBiasNonBeamBeamLineDecision;   //!
   TBranch        *b_StrippingLowMultNonBeamBeamNoBiasLineDecision;   //!
   TBranch        *b_StrippingLowMultMuonLineDecision;   //!
   TBranch        *b_StrippingLowMultDiMuonLineDecision;   //!
   TBranch        *b_StrippingHeavyIonTopologyLowActivityLineDecision;   //!
   TBranch        *b_ET_prev1;   //!
   TBranch        *b_ET_prev2;   //!
   TBranch        *b_nouttrck;   //!
   TBranch        *b_trck_key;   //!
   TBranch        *b_trck_type;   //!
   TBranch        *b_trck_posx;   //!
   TBranch        *b_trck_posy;   //!
   TBranch        *b_trck_posz;   //!
   TBranch        *b_trck_px;   //!
   TBranch        *b_trck_py;   //!
   TBranch        *b_trck_pz;   //!
   TBranch        *b_trck_charge;   //!
   TBranch        *b_trck_muonid;   //!
   TBranch        *b_trck_riche;   //!
   TBranch        *b_trck_richk;   //!
   TBranch        *b_trck_richp;   //!
   TBranch        *b_trck_muonll;   //!
   TBranch        *b_trck_muacc;   //!
   TBranch        *b_trck_ecal;   //!
   TBranch        *b_trck_hcal;   //!
   TBranch        *b_trck_eop;   //!
   TBranch        *b_trck_probe;   //!
   TBranch        *b_trck_probmu;   //!
   TBranch        *b_trck_probpi;   //!
   TBranch        *b_trck_probk;   //!
   TBranch        *b_trck_probp;   //!
   TBranch        *b_trck_probghost;   //!
   TBranch        *b_noutmuon;   //!
   TBranch        *b_muon_chi2ndf;   //!
   TBranch        *b_muon_posx;   //!
   TBranch        *b_muon_posy;   //!
   TBranch        *b_muon_ux;   //!
   TBranch        *b_muon_uy;   //!
   TBranch        *b_muon_nshared;   //!
   TBranch        *b_nphotons;   //!
   TBranch        *b_noutphot;   //!
   TBranch        *b_photon_px;   //!
   TBranch        *b_photon_py;   //!
   TBranch        *b_photon_pz;   //!
   TBranch        *b_photon_nspd;   //!
   TBranch        *b_photon_nprs;   //!
   TBranch        *b_noutspd;   //!
   TBranch        *b_spd_posx;   //!
   TBranch        *b_spd_posy;   //!
   TBranch        *b_noutprs;   //!
   TBranch        *b_prs_posx;   //!
   TBranch        *b_prs_posy;   //!
   TBranch        *b_noutl0mu;   //!
   TBranch        *b_l0mu_pt;   //!
   TBranch        *b_l0mu_phi;   //!
   TBranch        *b_l0mu_theta;   //!
   TBranch        *b_noutl0h;   //!
   TBranch        *b_l0h_et;   //!
   TBranch        *b_noutl2mu;   //!
   TBranch        *b_hlt2mu_posx;   //!
   TBranch        *b_hlt2mu_posy;   //!
   TBranch        *b_hlt2mu_posz;   //!
   TBranch        *b_noutl1mu;   //!
   TBranch        *b_hlt1mu_posx;   //!
   TBranch        *b_hlt1mu_posy;   //!
   TBranch        *b_hlt1mu_posz;   //!
   TBranch        *b_hlt1mu_slopex;   //!
   TBranch        *b_hlt1mu_slopey;   //!

   makeCalSam_pA(TTree *tree=0);
   virtual ~makeCalSam_pA();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef makeCalSam_pA_cxx
makeCalSam_pA::makeCalSam_pA(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      f->GetObject("TupleTools/MyTuple",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
      TChain * chain = new TChain("TupleTools/MyTuple","");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058406.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058407.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058408.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058409.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058410.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058411.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058412.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058413.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058414.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058415.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058416.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058417.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058418.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058419.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058420.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058421.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058422.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058423.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058424.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058425.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058426.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058427.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058428.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058429.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058430.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058431.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058432.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058433.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058434.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058435.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058436.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058437.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058438.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058439.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058440.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058441.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058442.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058443.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058444.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058445.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058446.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058447.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058448.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058449.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058450.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058451.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058452.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058453.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058454.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058455.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058456.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058457.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058458.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058459.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058460.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058461.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058462.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058463.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058464.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058465.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058466.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058467.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058468.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058469.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058470.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058471.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058472.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058473.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058474.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058475.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058476.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058479.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058480.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058481.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058482.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058483.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058485.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058486.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058487.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058488.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058489.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058490.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058491.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058492.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058493.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058494.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058495.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058496.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058497.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058498.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058499.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058500.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058501.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058502.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058503.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058504.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058505.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058506.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058507.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058508.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058509.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058510.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058511.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058512.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058513.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058514.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058515.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058516.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058517.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058518.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058519.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058520.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058521.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058522.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058523.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058524.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058525.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058526.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058527.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058528.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058529.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058530.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058531.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058532.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058533.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058534.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058535.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058536.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058537.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058538.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058539.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016/mytuple432058540.root/TupleTools/MyTuple");
      tree = chain;
#endif // SINGLE_TREE

   }
   Init(tree);
}

makeCalSam_pA::~makeCalSam_pA()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t makeCalSam_pA::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t makeCalSam_pA::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void makeCalSam_pA::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
   fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
   fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
   fChain->SetBranchAddress("HLT1TCK", &HLT1TCK, &b_HLT1TCK);
   fChain->SetBranchAddress("HLT2TCK", &HLT2TCK, &b_HLT2TCK);
   fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
   fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nLongTracks", &nLongTracks, &b_nLongTracks);
   fChain->SetBranchAddress("nDownstreamTracks", &nDownstreamTracks, &b_nDownstreamTracks);
   fChain->SetBranchAddress("nUpstreamTracks", &nUpstreamTracks, &b_nUpstreamTracks);
   fChain->SetBranchAddress("nVeloTracks", &nVeloTracks, &b_nVeloTracks);
   fChain->SetBranchAddress("nTTracks", &nTTracks, &b_nTTracks);
   fChain->SetBranchAddress("nBackTracks", &nBackTracks, &b_nBackTracks);
   fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
   fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
   fChain->SetBranchAddress("nVeloClusters", &nVeloClusters, &b_nVeloClusters);
   fChain->SetBranchAddress("nITClusters", &nITClusters, &b_nITClusters);
   fChain->SetBranchAddress("nTTClusters", &nTTClusters, &b_nTTClusters);
   fChain->SetBranchAddress("nOTClusters", &nOTClusters, &b_nOTClusters);
   fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
   fChain->SetBranchAddress("nMuonCoordsS0", &nMuonCoordsS0, &b_nMuonCoordsS0);
   fChain->SetBranchAddress("nMuonCoordsS1", &nMuonCoordsS1, &b_nMuonCoordsS1);
   fChain->SetBranchAddress("nMuonCoordsS2", &nMuonCoordsS2, &b_nMuonCoordsS2);
   fChain->SetBranchAddress("nMuonCoordsS3", &nMuonCoordsS3, &b_nMuonCoordsS3);
   fChain->SetBranchAddress("nMuonCoordsS4", &nMuonCoordsS4, &b_nMuonCoordsS4);
   fChain->SetBranchAddress("nMuonTracks", &nMuonTracks, &b_nMuonTracks);
   fChain->SetBranchAddress("B00", &B00, &b_B00);
   fChain->SetBranchAddress("B01", &B01, &b_B01);
   fChain->SetBranchAddress("B02", &B02, &b_B02);
   fChain->SetBranchAddress("B03", &B03, &b_B03);
   fChain->SetBranchAddress("B10", &B10, &b_B10);
   fChain->SetBranchAddress("B11", &B11, &b_B11);
   fChain->SetBranchAddress("B12", &B12, &b_B12);
   fChain->SetBranchAddress("B13", &B13, &b_B13);
   fChain->SetBranchAddress("B20", &B20, &b_B20);
   fChain->SetBranchAddress("B21", &B21, &b_B21);
   fChain->SetBranchAddress("B22", &B22, &b_B22);
   fChain->SetBranchAddress("B23", &B23, &b_B23);
   fChain->SetBranchAddress("F10", &F10, &b_F10);
   fChain->SetBranchAddress("F11", &F11, &b_F11);
   fChain->SetBranchAddress("F12", &F12, &b_F12);
   fChain->SetBranchAddress("F13", &F13, &b_F13);
   fChain->SetBranchAddress("F20", &F20, &b_F20);
   fChain->SetBranchAddress("F21", &F21, &b_F21);
   fChain->SetBranchAddress("F22", &F22, &b_F22);
   fChain->SetBranchAddress("F23", &F23, &b_F23);
   fChain->SetBranchAddress("log_hrc_fom_v4", &log_hrc_fom_v4, &b_log_hrc_fom_v4);
   fChain->SetBranchAddress("log_hrc_fom_B_v4", &log_hrc_fom_B_v4, &b_log_hrc_fom_B_v4);
   fChain->SetBranchAddress("log_hrc_fom_F_v4", &log_hrc_fom_F_v4, &b_log_hrc_fom_F_v4);
   fChain->SetBranchAddress("nchB", &nchB, &b_nchB);
   fChain->SetBranchAddress("adc_B", adc_B, &b_adc_B);
   fChain->SetBranchAddress("nchF", &nchF, &b_nchF);
   fChain->SetBranchAddress("adc_F", adc_F, &b_adc_F);
   fChain->SetBranchAddress("L0Global", &L0Global, &b_L0Global);
   fChain->SetBranchAddress("Hlt1Global", &Hlt1Global, &b_Hlt1Global);
   fChain->SetBranchAddress("Hlt2Global", &Hlt2Global, &b_Hlt2Global);
   fChain->SetBranchAddress("L0Muon,lowMultDecision", &L0Muon_lowMultDecision, &b_L0Muon_lowMultDecision);
   fChain->SetBranchAddress("L0DiMuon,lowMultDecision", &L0DiMuon_lowMultDecision, &b_L0DiMuon_lowMultDecision);
   fChain->SetBranchAddress("L0DiHadron,lowMultDecision", &L0DiHadron_lowMultDecision, &b_L0DiHadron_lowMultDecision);
   fChain->SetBranchAddress("L0Electron,lowMultDecision", &L0Electron_lowMultDecision, &b_L0Electron_lowMultDecision);
   fChain->SetBranchAddress("L0Photon,lowMultDecision", &L0Photon_lowMultDecision, &b_L0Photon_lowMultDecision);
   fChain->SetBranchAddress("L0DiEM,lowMultDecision", &L0DiEM_lowMultDecision, &b_L0DiEM_lowMultDecision);
   fChain->SetBranchAddress("L0nSelections", &L0nSelections, &b_L0nSelections);
   fChain->SetBranchAddress("Hlt1NoPVPassThroughDecision", &Hlt1NoPVPassThroughDecision, &b_Hlt1NoPVPassThroughDecision);
   fChain->SetBranchAddress("Hlt1LowMultPassThroughDecision", &Hlt1LowMultPassThroughDecision, &b_Hlt1LowMultPassThroughDecision);
   fChain->SetBranchAddress("Hlt1NoBiasNonBeamBeamDecision", &Hlt1NoBiasNonBeamBeamDecision, &b_Hlt1NoBiasNonBeamBeamDecision);
   fChain->SetBranchAddress("Hlt1MBNoBiasRateLimitedDecision", &Hlt1MBNoBiasRateLimitedDecision, &b_Hlt1MBNoBiasRateLimitedDecision);
   fChain->SetBranchAddress("Hlt1MBNoBiasDecision", &Hlt1MBNoBiasDecision, &b_Hlt1MBNoBiasDecision);
   fChain->SetBranchAddress("Hlt1CEPVeloCutDecision", &Hlt1CEPVeloCutDecision, &b_Hlt1CEPVeloCutDecision);
   fChain->SetBranchAddress("Hlt1LowMultDecision", &Hlt1LowMultDecision, &b_Hlt1LowMultDecision);
   fChain->SetBranchAddress("Hlt1LowMultMuonDecision", &Hlt1LowMultMuonDecision, &b_Hlt1LowMultMuonDecision);
   fChain->SetBranchAddress("Hlt1LowMultHerschelDecision", &Hlt1LowMultHerschelDecision, &b_Hlt1LowMultHerschelDecision);
   fChain->SetBranchAddress("Hlt1LowMultVeloCut_LeptonsDecision", &Hlt1LowMultVeloCut_LeptonsDecision, &b_Hlt1LowMultVeloCut_LeptonsDecision);
   fChain->SetBranchAddress("Hlt1LowMultVeloAndHerschel_LeptonsDecision", &Hlt1LowMultVeloAndHerschel_LeptonsDecision, &b_Hlt1LowMultVeloAndHerschel_LeptonsDecision);
   fChain->SetBranchAddress("Hlt1nSelections", &Hlt1nSelections, &b_Hlt1nSelections);
   fChain->SetBranchAddress("Hlt2NoBiasNonBeamBeamDecision", &Hlt2NoBiasNonBeamBeamDecision, &b_Hlt2NoBiasNonBeamBeamDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHDecision", &Hlt2LowMultLMR2HHDecision, &b_Hlt2LowMultLMR2HHDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHWSDecision", &Hlt2LowMultLMR2HHWSDecision, &b_Hlt2LowMultLMR2HHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHHHDecision", &Hlt2LowMultLMR2HHHHDecision, &b_Hlt2LowMultLMR2HHHHDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHHHWSDecision", &Hlt2LowMultLMR2HHHHWSDecision, &b_Hlt2LowMultLMR2HHHHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultL2pPiDecision", &Hlt2LowMultL2pPiDecision, &b_Hlt2LowMultL2pPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiDecision", &Hlt2LowMultD2KPiDecision, &b_Hlt2LowMultD2KPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiPiDecision", &Hlt2LowMultD2KPiPiDecision, &b_Hlt2LowMultD2KPiPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KKPiDecision", &Hlt2LowMultD2KKPiDecision, &b_Hlt2LowMultD2KKPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2K3PiDecision", &Hlt2LowMultD2K3PiDecision, &b_Hlt2LowMultD2K3PiDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHDecision", &Hlt2LowMultChiC2HHDecision, &b_Hlt2LowMultChiC2HHDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHHHDecision", &Hlt2LowMultChiC2HHHHDecision, &b_Hlt2LowMultChiC2HHHHDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2PPDecision", &Hlt2LowMultChiC2PPDecision, &b_Hlt2LowMultChiC2PPDecision);
   fChain->SetBranchAddress("Hlt2LowMultHadron_noTrFiltDecision", &Hlt2LowMultHadron_noTrFiltDecision, &b_Hlt2LowMultHadron_noTrFiltDecision);
   fChain->SetBranchAddress("Hlt2LowMultL2pPiWSDecision", &Hlt2LowMultL2pPiWSDecision, &b_Hlt2LowMultL2pPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiWSDecision", &Hlt2LowMultD2KPiWSDecision, &b_Hlt2LowMultD2KPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiPiWSDecision", &Hlt2LowMultD2KPiPiWSDecision, &b_Hlt2LowMultD2KPiPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KKPiWSDecision", &Hlt2LowMultD2KKPiWSDecision, &b_Hlt2LowMultD2KKPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2K3PiWSDecision", &Hlt2LowMultD2K3PiWSDecision, &b_Hlt2LowMultD2K3PiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHWSDecision", &Hlt2LowMultChiC2HHWSDecision, &b_Hlt2LowMultChiC2HHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHHHWSDecision", &Hlt2LowMultChiC2HHHHWSDecision, &b_Hlt2LowMultChiC2HHHHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2PPWSDecision", &Hlt2LowMultChiC2PPWSDecision, &b_Hlt2LowMultChiC2PPWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultMuonDecision", &Hlt2LowMultMuonDecision, &b_Hlt2LowMultMuonDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiMuonDecision", &Hlt2LowMultDiMuonDecision, &b_Hlt2LowMultDiMuonDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiMuon_PSDecision", &Hlt2LowMultDiMuon_PSDecision, &b_Hlt2LowMultDiMuon_PSDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiPhotonDecision", &Hlt2LowMultDiPhotonDecision, &b_Hlt2LowMultDiPhotonDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiPhoton_HighMassDecision", &Hlt2LowMultDiPhoton_HighMassDecision, &b_Hlt2LowMultDiPhoton_HighMassDecision);
   fChain->SetBranchAddress("Hlt2LowMultpi0Decision", &Hlt2LowMultpi0Decision, &b_Hlt2LowMultpi0Decision);
   fChain->SetBranchAddress("Hlt2LowMultDiElectronDecision", &Hlt2LowMultDiElectronDecision, &b_Hlt2LowMultDiElectronDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiElectron_noTrFiltDecision", &Hlt2LowMultDiElectron_noTrFiltDecision, &b_Hlt2LowMultDiElectron_noTrFiltDecision);
   fChain->SetBranchAddress("Hlt2MBMicroBiasVeloDecision", &Hlt2MBMicroBiasVeloDecision, &b_Hlt2MBMicroBiasVeloDecision);
   fChain->SetBranchAddress("Hlt2LowMultTechnical_MinBiasDecision", &Hlt2LowMultTechnical_MinBiasDecision, &b_Hlt2LowMultTechnical_MinBiasDecision);
   fChain->SetBranchAddress("Hlt2nSelections", &Hlt2nSelections, &b_Hlt2nSelections);
   fChain->SetBranchAddress("MaxRoutingBits", &MaxRoutingBits, &b_MaxRoutingBits);
   fChain->SetBranchAddress("RoutingBits", RoutingBits, &b_RoutingBits);
   fChain->SetBranchAddress("StrippingMBNoBiasDecision", &StrippingMBNoBiasDecision, &b_StrippingMBNoBiasDecision);
   fChain->SetBranchAddress("StrippingHlt1NoBiasNonBeamBeamLineDecision", &StrippingHlt1NoBiasNonBeamBeamLineDecision, &b_StrippingHlt1NoBiasNonBeamBeamLineDecision);
   fChain->SetBranchAddress("StrippingLowMultNonBeamBeamNoBiasLineDecision", &StrippingLowMultNonBeamBeamNoBiasLineDecision, &b_StrippingLowMultNonBeamBeamNoBiasLineDecision);
   fChain->SetBranchAddress("StrippingLowMultMuonLineDecision", &StrippingLowMultMuonLineDecision, &b_StrippingLowMultMuonLineDecision);
   fChain->SetBranchAddress("StrippingLowMultDiMuonLineDecision", &StrippingLowMultDiMuonLineDecision, &b_StrippingLowMultDiMuonLineDecision);
   fChain->SetBranchAddress("StrippingHeavyIonTopologyLowActivityLineDecision", &StrippingHeavyIonTopologyLowActivityLineDecision, &b_StrippingHeavyIonTopologyLowActivityLineDecision);
   fChain->SetBranchAddress("ET_prev1", &ET_prev1, &b_ET_prev1);
   fChain->SetBranchAddress("ET_prev2", &ET_prev2, &b_ET_prev2);
   fChain->SetBranchAddress("nouttrck", &nouttrck, &b_nouttrck);
   fChain->SetBranchAddress("trck_key", trck_key, &b_trck_key);
   fChain->SetBranchAddress("trck_type", trck_type, &b_trck_type);
   fChain->SetBranchAddress("trck_posx", trck_posx, &b_trck_posx);
   fChain->SetBranchAddress("trck_posy", trck_posy, &b_trck_posy);
   fChain->SetBranchAddress("trck_posz", trck_posz, &b_trck_posz);
   fChain->SetBranchAddress("trck_px", trck_px, &b_trck_px);
   fChain->SetBranchAddress("trck_py", trck_py, &b_trck_py);
   fChain->SetBranchAddress("trck_pz", trck_pz, &b_trck_pz);
   fChain->SetBranchAddress("trck_charge", trck_charge, &b_trck_charge);
   fChain->SetBranchAddress("trck_muonid", trck_muonid, &b_trck_muonid);
   fChain->SetBranchAddress("trck_riche", trck_riche, &b_trck_riche);
   fChain->SetBranchAddress("trck_richk", trck_richk, &b_trck_richk);
   fChain->SetBranchAddress("trck_richp", trck_richp, &b_trck_richp);
   fChain->SetBranchAddress("trck_muonll", trck_muonll, &b_trck_muonll);
   fChain->SetBranchAddress("trck_muacc", trck_muacc, &b_trck_muacc);
   fChain->SetBranchAddress("trck_ecal", trck_ecal, &b_trck_ecal);
   fChain->SetBranchAddress("trck_hcal", trck_hcal, &b_trck_hcal);
   fChain->SetBranchAddress("trck_eop", trck_eop, &b_trck_eop);
   fChain->SetBranchAddress("trck_probe", trck_probe, &b_trck_probe);
   fChain->SetBranchAddress("trck_probmu", trck_probmu, &b_trck_probmu);
   fChain->SetBranchAddress("trck_probpi", trck_probpi, &b_trck_probpi);
   fChain->SetBranchAddress("trck_probk", trck_probk, &b_trck_probk);
   fChain->SetBranchAddress("trck_probp", trck_probp, &b_trck_probp);
   fChain->SetBranchAddress("trck_probghost", trck_probghost, &b_trck_probghost);
   fChain->SetBranchAddress("noutmuon", &noutmuon, &b_noutmuon);
   fChain->SetBranchAddress("muon_chi2ndf", muon_chi2ndf, &b_muon_chi2ndf);
   fChain->SetBranchAddress("muon_posx", muon_posx, &b_muon_posx);
   fChain->SetBranchAddress("muon_posy", muon_posy, &b_muon_posy);
   fChain->SetBranchAddress("muon_ux", muon_ux, &b_muon_ux);
   fChain->SetBranchAddress("muon_uy", muon_uy, &b_muon_uy);
   fChain->SetBranchAddress("muon_nshared", muon_nshared, &b_muon_nshared);
   fChain->SetBranchAddress("nphotons", &nphotons, &b_nphotons);
   fChain->SetBranchAddress("noutphot", &noutphot, &b_noutphot);
   fChain->SetBranchAddress("photon_px", photon_px, &b_photon_px);
   fChain->SetBranchAddress("photon_py", photon_py, &b_photon_py);
   fChain->SetBranchAddress("photon_pz", photon_pz, &b_photon_pz);
   fChain->SetBranchAddress("photon_nspd", photon_nspd, &b_photon_nspd);
   fChain->SetBranchAddress("photon_nprs", photon_nprs, &b_photon_nprs);
   fChain->SetBranchAddress("noutspd", &noutspd, &b_noutspd);
   fChain->SetBranchAddress("spd_posx", spd_posx, &b_spd_posx);
   fChain->SetBranchAddress("spd_posy", spd_posy, &b_spd_posy);
   fChain->SetBranchAddress("noutprs", &noutprs, &b_noutprs);
   fChain->SetBranchAddress("prs_posx", prs_posx, &b_prs_posx);
   fChain->SetBranchAddress("prs_posy", prs_posy, &b_prs_posy);
   fChain->SetBranchAddress("noutl0mu", &noutl0mu, &b_noutl0mu);
   fChain->SetBranchAddress("l0mu_pt", l0mu_pt, &b_l0mu_pt);
   fChain->SetBranchAddress("l0mu_phi", l0mu_phi, &b_l0mu_phi);
   fChain->SetBranchAddress("l0mu_theta", l0mu_theta, &b_l0mu_theta);
   fChain->SetBranchAddress("noutl0h", &noutl0h, &b_noutl0h);
   fChain->SetBranchAddress("l0h_et", l0h_et, &b_l0h_et);
   fChain->SetBranchAddress("noutl2mu", &noutl2mu, &b_noutl2mu);
   fChain->SetBranchAddress("hlt2mu_posx", hlt2mu_posx, &b_hlt2mu_posx);
   fChain->SetBranchAddress("hlt2mu_posy", hlt2mu_posy, &b_hlt2mu_posy);
   fChain->SetBranchAddress("hlt2mu_posz", hlt2mu_posz, &b_hlt2mu_posz);
   fChain->SetBranchAddress("noutl1mu", &noutl1mu, &b_noutl1mu);
   fChain->SetBranchAddress("hlt1mu_posx", hlt1mu_posx, &b_hlt1mu_posx);
   fChain->SetBranchAddress("hlt1mu_posy", hlt1mu_posy, &b_hlt1mu_posy);
   fChain->SetBranchAddress("hlt1mu_posz", hlt1mu_posz, &b_hlt1mu_posz);
   fChain->SetBranchAddress("hlt1mu_slopex", hlt1mu_slopex, &b_hlt1mu_slopex);
   fChain->SetBranchAddress("hlt1mu_slopey", hlt1mu_slopey, &b_hlt1mu_slopey);
   Notify();
}

Bool_t makeCalSam_pA::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void makeCalSam_pA::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t makeCalSam_pA::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef makeCalSam_pA_cxx

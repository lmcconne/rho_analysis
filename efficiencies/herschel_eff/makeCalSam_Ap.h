//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Jul 23 20:42:03 2021 by ROOT version 6.22/08
// from TChain TupleTools/MyTuple/
//////////////////////////////////////////////////////////

#ifndef makeCalSam_Ap_h
#define makeCalSam_Ap_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class makeCalSam_Ap {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          BCID;
   Int_t           BCType;
   UInt_t          OdinTCK;
   UInt_t          L0DUTCK;
   UInt_t          HLT1TCK;
   UInt_t          HLT2TCK;
   ULong64_t       GpsTime;
   Short_t         Polarity;
   Int_t           nPVs;
   Int_t           nTracks;
   Int_t           nLongTracks;
   Int_t           nDownstreamTracks;
   Int_t           nUpstreamTracks;
   Int_t           nVeloTracks;
   Int_t           nTTracks;
   Int_t           nBackTracks;
   Int_t           nRich1Hits;
   Int_t           nRich2Hits;
   Int_t           nVeloClusters;
   Int_t           nITClusters;
   Int_t           nTTClusters;
   Int_t           nOTClusters;
   Int_t           nSPDHits;
   Int_t           nMuonCoordsS0;
   Int_t           nMuonCoordsS1;
   Int_t           nMuonCoordsS2;
   Int_t           nMuonCoordsS3;
   Int_t           nMuonCoordsS4;
   Int_t           nMuonTracks;
   Int_t           B00;
   Int_t           B01;
   Int_t           B02;
   Int_t           B03;
   Int_t           B10;
   Int_t           B11;
   Int_t           B12;
   Int_t           B13;
   Int_t           B20;
   Int_t           B21;
   Int_t           B22;
   Int_t           B23;
   Int_t           F10;
   Int_t           F11;
   Int_t           F12;
   Int_t           F13;
   Int_t           F20;
   Int_t           F21;
   Int_t           F22;
   Int_t           F23;
   Double_t        log_hrc_fom_v4;
   Double_t        log_hrc_fom_B_v4;
   Double_t        log_hrc_fom_F_v4;
   Int_t           nchB;
   Float_t         adc_B[1000];   //[nchB]
   Int_t           nchF;
   Float_t         adc_F[1000];   //[nchF]
   Int_t           L0Global;
   UInt_t          Hlt1Global;
   UInt_t          Hlt2Global;
   Int_t           L0Muon_lowMultDecision;
   Int_t           L0DiMuon_lowMultDecision;
   Int_t           L0DiHadron_lowMultDecision;
   Int_t           L0Electron_lowMultDecision;
   Int_t           L0Photon_lowMultDecision;
   Int_t           L0DiEM_lowMultDecision;
   UInt_t          L0nSelections;
   Int_t           Hlt1NoPVPassThroughDecision;
   Int_t           Hlt1LowMultPassThroughDecision;
   Int_t           Hlt1NoBiasNonBeamBeamDecision;
   Int_t           Hlt1MBNoBiasRateLimitedDecision;
   Int_t           Hlt1MBNoBiasDecision;
   Int_t           Hlt1CEPVeloCutDecision;
   Int_t           Hlt1LowMultDecision;
   Int_t           Hlt1LowMultMuonDecision;
   Int_t           Hlt1LowMultHerschelDecision;
   Int_t           Hlt1LowMultVeloCut_LeptonsDecision;
   Int_t           Hlt1LowMultVeloAndHerschel_LeptonsDecision;
   UInt_t          Hlt1nSelections;
   Int_t           Hlt2NoBiasNonBeamBeamDecision;
   Int_t           Hlt2LowMultLMR2HHDecision;
   Int_t           Hlt2LowMultLMR2HHWSDecision;
   Int_t           Hlt2LowMultLMR2HHHHDecision;
   Int_t           Hlt2LowMultLMR2HHHHWSDecision;
   Int_t           Hlt2LowMultL2pPiDecision;
   Int_t           Hlt2LowMultD2KPiDecision;
   Int_t           Hlt2LowMultD2KPiPiDecision;
   Int_t           Hlt2LowMultD2KKPiDecision;
   Int_t           Hlt2LowMultD2K3PiDecision;
   Int_t           Hlt2LowMultChiC2HHDecision;
   Int_t           Hlt2LowMultChiC2HHHHDecision;
   Int_t           Hlt2LowMultChiC2PPDecision;
   Int_t           Hlt2LowMultHadron_noTrFiltDecision;
   Int_t           Hlt2LowMultL2pPiWSDecision;
   Int_t           Hlt2LowMultD2KPiWSDecision;
   Int_t           Hlt2LowMultD2KPiPiWSDecision;
   Int_t           Hlt2LowMultD2KKPiWSDecision;
   Int_t           Hlt2LowMultD2K3PiWSDecision;
   Int_t           Hlt2LowMultChiC2HHWSDecision;
   Int_t           Hlt2LowMultChiC2HHHHWSDecision;
   Int_t           Hlt2LowMultChiC2PPWSDecision;
   Int_t           Hlt2LowMultMuonDecision;
   Int_t           Hlt2LowMultDiMuonDecision;
   Int_t           Hlt2LowMultDiMuon_PSDecision;
   Int_t           Hlt2LowMultDiPhotonDecision;
   Int_t           Hlt2LowMultDiPhoton_HighMassDecision;
   Int_t           Hlt2LowMultpi0Decision;
   Int_t           Hlt2LowMultDiElectronDecision;
   Int_t           Hlt2LowMultDiElectron_noTrFiltDecision;
   Int_t           Hlt2MBMicroBiasVeloDecision;
   Int_t           Hlt2LowMultTechnical_MinBiasDecision;
   UInt_t          Hlt2nSelections;
   Int_t           MaxRoutingBits;
   Float_t         RoutingBits[64];   //[MaxRoutingBits]
   UInt_t          StrippingMBNoBiasDecision;
   UInt_t          StrippingHlt1NoBiasNonBeamBeamLineDecision;
   UInt_t          StrippingLowMultNonBeamBeamNoBiasLineDecision;
   UInt_t          StrippingLowMultMuonLineDecision;
   UInt_t          StrippingLowMultDiMuonLineDecision;
   UInt_t          StrippingHeavyIonTopologyLowActivityLineDecision;
   Float_t         ET_prev1;
   Float_t         ET_prev2;
   Int_t           nouttrck;
   Float_t         trck_key[10];   //[nouttrck]
   Float_t         trck_type[10];   //[nouttrck]
   Float_t         trck_posx[10];   //[nouttrck]
   Float_t         trck_posy[10];   //[nouttrck]
   Float_t         trck_posz[10];   //[nouttrck]
   Float_t         trck_px[10];   //[nouttrck]
   Float_t         trck_py[10];   //[nouttrck]
   Float_t         trck_pz[10];   //[nouttrck]
   Float_t         trck_charge[10];   //[nouttrck]
   Float_t         trck_muonid[10];   //[nouttrck]
   Float_t         trck_riche[10];   //[nouttrck]
   Float_t         trck_richk[10];   //[nouttrck]
   Float_t         trck_richp[10];   //[nouttrck]
   Float_t         trck_muonll[10];   //[nouttrck]
   Float_t         trck_muacc[10];   //[nouttrck]
   Float_t         trck_ecal[10];   //[nouttrck]
   Float_t         trck_hcal[10];   //[nouttrck]
   Float_t         trck_eop[10];   //[nouttrck]
   Float_t         trck_probe[10];   //[nouttrck]
   Float_t         trck_probmu[10];   //[nouttrck]
   Float_t         trck_probpi[10];   //[nouttrck]
   Float_t         trck_probk[10];   //[nouttrck]
   Float_t         trck_probp[10];   //[nouttrck]
   Float_t         trck_probghost[10];   //[nouttrck]
   Int_t           noutmuon;
   Float_t         muon_chi2ndf[10];   //[noutmuon]
   Float_t         muon_posx[10];   //[noutmuon]
   Float_t         muon_posy[10];   //[noutmuon]
   Float_t         muon_ux[10];   //[noutmuon]
   Float_t         muon_uy[10];   //[noutmuon]
   Float_t         muon_nshared[10];   //[noutmuon]
   Int_t           nphotons;
   Int_t           noutphot;
   Float_t         photon_px[10];   //[noutphot]
   Float_t         photon_py[10];   //[noutphot]
   Float_t         photon_pz[10];   //[noutphot]
   Float_t         photon_nspd[10];   //[noutphot]
   Float_t         photon_nprs[10];   //[noutphot]
   Int_t           noutspd;
   Float_t         spd_posx[30];   //[noutspd]
   Float_t         spd_posy[30];   //[noutspd]
   Int_t           noutprs;
   Float_t         prs_posx[30];   //[noutprs]
   Float_t         prs_posy[30];   //[noutprs]
   Int_t           noutl0mu;
   Float_t         l0mu_pt[10];   //[noutl0mu]
   Float_t         l0mu_phi[10];   //[noutl0mu]
   Float_t         l0mu_theta[10];   //[noutl0mu]
   Int_t           noutl0h;
   Float_t         l0h_et[10];   //[noutl0h]
   Int_t           noutl2mu;
   Float_t         hlt2mu_posx[10];   //[noutl2mu]
   Float_t         hlt2mu_posy[10];   //[noutl2mu]
   Float_t         hlt2mu_posz[10];   //[noutl2mu]
   Int_t           noutl1mu;
   Float_t         hlt1mu_posx[10];   //[noutl1mu]
   Float_t         hlt1mu_posy[10];   //[noutl1mu]
   Float_t         hlt1mu_posz[10];   //[noutl1mu]
   Float_t         hlt1mu_slopex[10];   //[noutl1mu]
   Float_t         hlt1mu_slopey[10];   //[noutl1mu]

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_BCType;   //!
   TBranch        *b_OdinTCK;   //!
   TBranch        *b_L0DUTCK;   //!
   TBranch        *b_HLT1TCK;   //!
   TBranch        *b_HLT2TCK;   //!
   TBranch        *b_GpsTime;   //!
   TBranch        *b_Polarity;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nLongTracks;   //!
   TBranch        *b_nDownstreamTracks;   //!
   TBranch        *b_nUpstreamTracks;   //!
   TBranch        *b_nVeloTracks;   //!
   TBranch        *b_nTTracks;   //!
   TBranch        *b_nBackTracks;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nVeloClusters;   //!
   TBranch        *b_nITClusters;   //!
   TBranch        *b_nTTClusters;   //!
   TBranch        *b_nOTClusters;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_nMuonCoordsS0;   //!
   TBranch        *b_nMuonCoordsS1;   //!
   TBranch        *b_nMuonCoordsS2;   //!
   TBranch        *b_nMuonCoordsS3;   //!
   TBranch        *b_nMuonCoordsS4;   //!
   TBranch        *b_nMuonTracks;   //!
   TBranch        *b_B00;   //!
   TBranch        *b_B01;   //!
   TBranch        *b_B02;   //!
   TBranch        *b_B03;   //!
   TBranch        *b_B10;   //!
   TBranch        *b_B11;   //!
   TBranch        *b_B12;   //!
   TBranch        *b_B13;   //!
   TBranch        *b_B20;   //!
   TBranch        *b_B21;   //!
   TBranch        *b_B22;   //!
   TBranch        *b_B23;   //!
   TBranch        *b_F10;   //!
   TBranch        *b_F11;   //!
   TBranch        *b_F12;   //!
   TBranch        *b_F13;   //!
   TBranch        *b_F20;   //!
   TBranch        *b_F21;   //!
   TBranch        *b_F22;   //!
   TBranch        *b_F23;   //!
   TBranch        *b_log_hrc_fom_v4;   //!
   TBranch        *b_log_hrc_fom_B_v4;   //!
   TBranch        *b_log_hrc_fom_F_v4;   //!
   TBranch        *b_nchB;   //!
   TBranch        *b_adc_B;   //!
   TBranch        *b_nchF;   //!
   TBranch        *b_adc_F;   //!
   TBranch        *b_L0Global;   //!
   TBranch        *b_Hlt1Global;   //!
   TBranch        *b_Hlt2Global;   //!
   TBranch        *b_L0Muon_lowMultDecision;   //!
   TBranch        *b_L0DiMuon_lowMultDecision;   //!
   TBranch        *b_L0DiHadron_lowMultDecision;   //!
   TBranch        *b_L0Electron_lowMultDecision;   //!
   TBranch        *b_L0Photon_lowMultDecision;   //!
   TBranch        *b_L0DiEM_lowMultDecision;   //!
   TBranch        *b_L0nSelections;   //!
   TBranch        *b_Hlt1NoPVPassThroughDecision;   //!
   TBranch        *b_Hlt1LowMultPassThroughDecision;   //!
   TBranch        *b_Hlt1NoBiasNonBeamBeamDecision;   //!
   TBranch        *b_Hlt1MBNoBiasRateLimitedDecision;   //!
   TBranch        *b_Hlt1MBNoBiasDecision;   //!
   TBranch        *b_Hlt1CEPVeloCutDecision;   //!
   TBranch        *b_Hlt1LowMultDecision;   //!
   TBranch        *b_Hlt1LowMultMuonDecision;   //!
   TBranch        *b_Hlt1LowMultHerschelDecision;   //!
   TBranch        *b_Hlt1LowMultVeloCut_LeptonsDecision;   //!
   TBranch        *b_Hlt1LowMultVeloAndHerschel_LeptonsDecision;   //!
   TBranch        *b_Hlt1nSelections;   //!
   TBranch        *b_Hlt2NoBiasNonBeamBeamDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHWSDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHHHDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHHHWSDecision;   //!
   TBranch        *b_Hlt2LowMultL2pPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2KKPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2K3PiDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHHHDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2PPDecision;   //!
   TBranch        *b_Hlt2LowMultHadron_noTrFiltDecision;   //!
   TBranch        *b_Hlt2LowMultL2pPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2KKPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2K3PiWSDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHWSDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHHHWSDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2PPWSDecision;   //!
   TBranch        *b_Hlt2LowMultMuonDecision;   //!
   TBranch        *b_Hlt2LowMultDiMuonDecision;   //!
   TBranch        *b_Hlt2LowMultDiMuon_PSDecision;   //!
   TBranch        *b_Hlt2LowMultDiPhotonDecision;   //!
   TBranch        *b_Hlt2LowMultDiPhoton_HighMassDecision;   //!
   TBranch        *b_Hlt2LowMultpi0Decision;   //!
   TBranch        *b_Hlt2LowMultDiElectronDecision;   //!
   TBranch        *b_Hlt2LowMultDiElectron_noTrFiltDecision;   //!
   TBranch        *b_Hlt2MBMicroBiasVeloDecision;   //!
   TBranch        *b_Hlt2LowMultTechnical_MinBiasDecision;   //!
   TBranch        *b_Hlt2nSelections;   //!
   TBranch        *b_MaxRoutingBits;   //!
   TBranch        *b_RoutingBits;   //!
   TBranch        *b_StrippingMBNoBiasDecision;   //!
   TBranch        *b_StrippingHlt1NoBiasNonBeamBeamLineDecision;   //!
   TBranch        *b_StrippingLowMultNonBeamBeamNoBiasLineDecision;   //!
   TBranch        *b_StrippingLowMultMuonLineDecision;   //!
   TBranch        *b_StrippingLowMultDiMuonLineDecision;   //!
   TBranch        *b_StrippingHeavyIonTopologyLowActivityLineDecision;   //!
   TBranch        *b_ET_prev1;   //!
   TBranch        *b_ET_prev2;   //!
   TBranch        *b_nouttrck;   //!
   TBranch        *b_trck_key;   //!
   TBranch        *b_trck_type;   //!
   TBranch        *b_trck_posx;   //!
   TBranch        *b_trck_posy;   //!
   TBranch        *b_trck_posz;   //!
   TBranch        *b_trck_px;   //!
   TBranch        *b_trck_py;   //!
   TBranch        *b_trck_pz;   //!
   TBranch        *b_trck_charge;   //!
   TBranch        *b_trck_muonid;   //!
   TBranch        *b_trck_riche;   //!
   TBranch        *b_trck_richk;   //!
   TBranch        *b_trck_richp;   //!
   TBranch        *b_trck_muonll;   //!
   TBranch        *b_trck_muacc;   //!
   TBranch        *b_trck_ecal;   //!
   TBranch        *b_trck_hcal;   //!
   TBranch        *b_trck_eop;   //!
   TBranch        *b_trck_probe;   //!
   TBranch        *b_trck_probmu;   //!
   TBranch        *b_trck_probpi;   //!
   TBranch        *b_trck_probk;   //!
   TBranch        *b_trck_probp;   //!
   TBranch        *b_trck_probghost;   //!
   TBranch        *b_noutmuon;   //!
   TBranch        *b_muon_chi2ndf;   //!
   TBranch        *b_muon_posx;   //!
   TBranch        *b_muon_posy;   //!
   TBranch        *b_muon_ux;   //!
   TBranch        *b_muon_uy;   //!
   TBranch        *b_muon_nshared;   //!
   TBranch        *b_nphotons;   //!
   TBranch        *b_noutphot;   //!
   TBranch        *b_photon_px;   //!
   TBranch        *b_photon_py;   //!
   TBranch        *b_photon_pz;   //!
   TBranch        *b_photon_nspd;   //!
   TBranch        *b_photon_nprs;   //!
   TBranch        *b_noutspd;   //!
   TBranch        *b_spd_posx;   //!
   TBranch        *b_spd_posy;   //!
   TBranch        *b_noutprs;   //!
   TBranch        *b_prs_posx;   //!
   TBranch        *b_prs_posy;   //!
   TBranch        *b_noutl0mu;   //!
   TBranch        *b_l0mu_pt;   //!
   TBranch        *b_l0mu_phi;   //!
   TBranch        *b_l0mu_theta;   //!
   TBranch        *b_noutl0h;   //!
   TBranch        *b_l0h_et;   //!
   TBranch        *b_noutl2mu;   //!
   TBranch        *b_hlt2mu_posx;   //!
   TBranch        *b_hlt2mu_posy;   //!
   TBranch        *b_hlt2mu_posz;   //!
   TBranch        *b_noutl1mu;   //!
   TBranch        *b_hlt1mu_posx;   //!
   TBranch        *b_hlt1mu_posy;   //!
   TBranch        *b_hlt1mu_posz;   //!
   TBranch        *b_hlt1mu_slopex;   //!
   TBranch        *b_hlt1mu_slopey;   //!

   makeCalSam_Ap(TTree *tree=0);
   virtual ~makeCalSam_Ap();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef makeCalSam_Ap_cxx
makeCalSam_Ap::makeCalSam_Ap(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      f->GetObject("TupleTools/MyTuple",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
      TChain * chain = new TChain("TupleTools/MyTuple","");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484709.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484711.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484714.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484717.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484719.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484722.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484725.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484728.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484729.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484732.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484736.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484738.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484742.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484745.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484748.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484753.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484756.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484759.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484763.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484764.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484767.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484771.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484773.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484776.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484779.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484782.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484785.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484788.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484791.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484794.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484796.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484799.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484802.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484806.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484808.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484813.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484816.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484819.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484821.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484824.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484826.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484829.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484831.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484834.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484836.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484839.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484842.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484844.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484847.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484849.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484851.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484855.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484857.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484859.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484862.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484865.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484868.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484870.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484873.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484876.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484878.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484880.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484883.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484886.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484889.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484892.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484893.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484896.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484899.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484901.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484904.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484906.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484909.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484911.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484914.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484916.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484919.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484921.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484924.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484925.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484926.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484929.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484931.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484934.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484937.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484939.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484942.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484945.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484948.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484950.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484953.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484956.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484958.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484960.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484962.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484965.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484967.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484970.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484973.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484975.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484989.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484991.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484993.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484996.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432484998.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485001.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485004.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485006.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485009.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485011.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485014.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485016.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485018.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485021.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485025.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485028.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485030.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485033.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485035.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485038.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485040.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485043.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485045.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485048.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485049.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485052.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485054.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485057.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485059.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485062.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485065.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485067.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485070.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485072.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485076.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485078.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485082.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485084.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485087.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485089.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485092.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485095.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485097.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485098.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485100.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485102.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485104.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485106.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485109.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485111.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485113.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485115.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485117.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485119.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485121.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485123.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485125.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485127.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485129.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485131.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485133.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485135.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485138.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485139.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485142.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485146.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485149.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485152.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485153.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485156.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485160.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485162.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485166.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485168.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485169.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485173.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485182.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485192.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485199.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485212.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485221.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485236.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485247.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485256.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485265.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485274.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485299.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485315.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485327.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485334.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485345.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485358.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485360.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485361.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485363.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485365.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485366.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485368.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485370.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485371.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485388.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485390.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485391.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485393.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485395.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485396.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485398.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485400.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485402.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485403.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485404.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485410.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485422.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485430.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485436.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485443.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485451.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485464.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485471.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485478.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485481.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485483.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485487.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485490.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485493.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485496.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485499.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485502.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485507.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485512.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485517.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485522.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485524.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485526.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485528.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485530.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485533.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485535.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485537.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485538.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485539.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485540.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485541.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485542.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485543.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485544.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485545.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485546.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485547.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485548.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485549.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485550.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485551.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485552.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485553.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485554.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485555.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485556.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485557.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485558.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485559.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485560.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485561.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485562.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485563.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485564.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485565.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485566.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485567.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485568.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485569.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485592.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485642.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485650.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485695.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485708.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485738.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485769.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485808.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485842.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485859.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485879.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485905.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485923.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485938.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432485962.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486008.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486066.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486107.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486149.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486170.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486198.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486224.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486241.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486254.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486269.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486281.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486294.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486301.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486309.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486349.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486350.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486351.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486352.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486353.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486354.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486355.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486356.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486357.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486358.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486359.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486360.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486361.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486362.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486363.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486364.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486365.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486366.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486367.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486368.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486369.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486370.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486371.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486372.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486373.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486374.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486375.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486376.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486378.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486379.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486380.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486381.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486382.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486383.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486384.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486385.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486386.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486387.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486388.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486389.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486390.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486391.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486392.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486393.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486394.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486395.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486396.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486397.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486398.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486399.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486400.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486401.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486402.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486403.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486404.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486405.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486406.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486407.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486408.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486409.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486410.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486411.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486412.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486413.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486414.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486415.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486416.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486417.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486418.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486419.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486420.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486421.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486422.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486423.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486424.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486426.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486427.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486431.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486432.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486433.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486434.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486435.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486436.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486437.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486438.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486439.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486440.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486442.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486443.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486444.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486445.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486446.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486447.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486448.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486449.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486450.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486451.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486452.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486453.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486454.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486455.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486456.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486457.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486458.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486459.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486460.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486461.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486462.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486463.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486464.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486465.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486466.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486467.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486468.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486469.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486470.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486471.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486472.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486473.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486474.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486475.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486476.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486477.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486478.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486479.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486480.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486481.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486482.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486483.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486484.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486485.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486486.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486487.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486488.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486489.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486490.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486491.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486492.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486493.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486494.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486495.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486496.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486497.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486498.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486499.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486500.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486501.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486502.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486503.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486504.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486505.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486506.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486507.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486508.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486509.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486510.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486512.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486513.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486515.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486516.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486517.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486518.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486519.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486520.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486521.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486522.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486524.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486527.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486528.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486530.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486531.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486532.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486534.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486535.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486536.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486537.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486538.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486539.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486540.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486542.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486543.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486544.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486545.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486546.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486547.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486548.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486549.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486550.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486551.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486552.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486555.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486556.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486557.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486558.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486559.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486560.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486561.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486562.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486563.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486564.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486568.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486569.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486570.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486571.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486572.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486573.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486574.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486575.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486576.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486577.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486579.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486582.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486583.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486584.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486585.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486586.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486587.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486588.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486589.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486590.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486591.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486592.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486593.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486594.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486595.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486596.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486597.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486598.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486599.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486600.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486601.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486603.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486607.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486608.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486609.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486611.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486613.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486614.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486616.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486617.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486618.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486619.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486620.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486621.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486622.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486623.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486624.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486625.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486626.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486627.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486630.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486632.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486634.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486636.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486638.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486640.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486641.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486642.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486643.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486644.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486645.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486646.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486647.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486648.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486649.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486650.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486651.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486652.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486653.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486654.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486658.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486675.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486689.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486701.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486710.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486711.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486712.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486713.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486714.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486715.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486716.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486717.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486718.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486719.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486720.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486721.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486722.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486723.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486724.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486725.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486726.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486727.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486728.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486729.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486730.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486731.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486732.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486733.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486734.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486735.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486737.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486738.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486739.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486740.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486741.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486742.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486743.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486744.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486745.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486746.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486747.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486749.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486751.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486752.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486753.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486754.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486755.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486756.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486757.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486758.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486759.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486760.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486761.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486762.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486763.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486764.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486766.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486767.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486769.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486770.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486771.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486774.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486775.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486776.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486777.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486778.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486779.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486780.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486781.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486782.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486783.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486784.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486785.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486786.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486787.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486788.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486790.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486791.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486792.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486794.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486796.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486798.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486799.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486800.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486801.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486802.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486803.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486805.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486807.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486810.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486811.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486812.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486813.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486814.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486815.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486816.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486817.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486818.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486819.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486820.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486821.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486822.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486823.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486824.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486825.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486826.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486827.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486828.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486829.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486830.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486831.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486832.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486833.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486834.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486835.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486836.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486837.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486838.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486839.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486840.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486841.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486842.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486843.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486844.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486845.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486846.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486847.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486848.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486849.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486850.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486851.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486852.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486853.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486854.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486855.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486856.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486857.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486858.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486859.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486860.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486861.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486862.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486863.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486864.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486865.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486866.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486867.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486868.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486869.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486870.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486871.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486872.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486873.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486874.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486875.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486876.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486877.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486878.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486879.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486880.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486882.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486883.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486884.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486885.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486886.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486887.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486888.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486889.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486900.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486904.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486907.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486917.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486936.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432486955.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487017.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487031.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487044.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487058.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487068.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487071.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487072.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487082.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487086.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487089.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487091.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487097.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487110.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487124.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487125.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487130.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487133.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487151.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487166.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487179.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487247.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487267.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487279.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487350.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487409.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487435.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487473.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487509.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487531.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487574.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487586.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016/mytuple432487615.root/TupleTools/MyTuple");
      tree = chain;
#endif // SINGLE_TREE

   }
   Init(tree);
}

makeCalSam_Ap::~makeCalSam_Ap()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t makeCalSam_Ap::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t makeCalSam_Ap::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void makeCalSam_Ap::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
   fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
   fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
   fChain->SetBranchAddress("HLT1TCK", &HLT1TCK, &b_HLT1TCK);
   fChain->SetBranchAddress("HLT2TCK", &HLT2TCK, &b_HLT2TCK);
   fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
   fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nLongTracks", &nLongTracks, &b_nLongTracks);
   fChain->SetBranchAddress("nDownstreamTracks", &nDownstreamTracks, &b_nDownstreamTracks);
   fChain->SetBranchAddress("nUpstreamTracks", &nUpstreamTracks, &b_nUpstreamTracks);
   fChain->SetBranchAddress("nVeloTracks", &nVeloTracks, &b_nVeloTracks);
   fChain->SetBranchAddress("nTTracks", &nTTracks, &b_nTTracks);
   fChain->SetBranchAddress("nBackTracks", &nBackTracks, &b_nBackTracks);
   fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
   fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
   fChain->SetBranchAddress("nVeloClusters", &nVeloClusters, &b_nVeloClusters);
   fChain->SetBranchAddress("nITClusters", &nITClusters, &b_nITClusters);
   fChain->SetBranchAddress("nTTClusters", &nTTClusters, &b_nTTClusters);
   fChain->SetBranchAddress("nOTClusters", &nOTClusters, &b_nOTClusters);
   fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
   fChain->SetBranchAddress("nMuonCoordsS0", &nMuonCoordsS0, &b_nMuonCoordsS0);
   fChain->SetBranchAddress("nMuonCoordsS1", &nMuonCoordsS1, &b_nMuonCoordsS1);
   fChain->SetBranchAddress("nMuonCoordsS2", &nMuonCoordsS2, &b_nMuonCoordsS2);
   fChain->SetBranchAddress("nMuonCoordsS3", &nMuonCoordsS3, &b_nMuonCoordsS3);
   fChain->SetBranchAddress("nMuonCoordsS4", &nMuonCoordsS4, &b_nMuonCoordsS4);
   fChain->SetBranchAddress("nMuonTracks", &nMuonTracks, &b_nMuonTracks);
   fChain->SetBranchAddress("B00", &B00, &b_B00);
   fChain->SetBranchAddress("B01", &B01, &b_B01);
   fChain->SetBranchAddress("B02", &B02, &b_B02);
   fChain->SetBranchAddress("B03", &B03, &b_B03);
   fChain->SetBranchAddress("B10", &B10, &b_B10);
   fChain->SetBranchAddress("B11", &B11, &b_B11);
   fChain->SetBranchAddress("B12", &B12, &b_B12);
   fChain->SetBranchAddress("B13", &B13, &b_B13);
   fChain->SetBranchAddress("B20", &B20, &b_B20);
   fChain->SetBranchAddress("B21", &B21, &b_B21);
   fChain->SetBranchAddress("B22", &B22, &b_B22);
   fChain->SetBranchAddress("B23", &B23, &b_B23);
   fChain->SetBranchAddress("F10", &F10, &b_F10);
   fChain->SetBranchAddress("F11", &F11, &b_F11);
   fChain->SetBranchAddress("F12", &F12, &b_F12);
   fChain->SetBranchAddress("F13", &F13, &b_F13);
   fChain->SetBranchAddress("F20", &F20, &b_F20);
   fChain->SetBranchAddress("F21", &F21, &b_F21);
   fChain->SetBranchAddress("F22", &F22, &b_F22);
   fChain->SetBranchAddress("F23", &F23, &b_F23);
   fChain->SetBranchAddress("log_hrc_fom_v4", &log_hrc_fom_v4, &b_log_hrc_fom_v4);
   fChain->SetBranchAddress("log_hrc_fom_B_v4", &log_hrc_fom_B_v4, &b_log_hrc_fom_B_v4);
   fChain->SetBranchAddress("log_hrc_fom_F_v4", &log_hrc_fom_F_v4, &b_log_hrc_fom_F_v4);
   fChain->SetBranchAddress("nchB", &nchB, &b_nchB);
   fChain->SetBranchAddress("adc_B", adc_B, &b_adc_B);
   fChain->SetBranchAddress("nchF", &nchF, &b_nchF);
   fChain->SetBranchAddress("adc_F", adc_F, &b_adc_F);
   fChain->SetBranchAddress("L0Global", &L0Global, &b_L0Global);
   fChain->SetBranchAddress("Hlt1Global", &Hlt1Global, &b_Hlt1Global);
   fChain->SetBranchAddress("Hlt2Global", &Hlt2Global, &b_Hlt2Global);
   fChain->SetBranchAddress("L0Muon,lowMultDecision", &L0Muon_lowMultDecision, &b_L0Muon_lowMultDecision);
   fChain->SetBranchAddress("L0DiMuon,lowMultDecision", &L0DiMuon_lowMultDecision, &b_L0DiMuon_lowMultDecision);
   fChain->SetBranchAddress("L0DiHadron,lowMultDecision", &L0DiHadron_lowMultDecision, &b_L0DiHadron_lowMultDecision);
   fChain->SetBranchAddress("L0Electron,lowMultDecision", &L0Electron_lowMultDecision, &b_L0Electron_lowMultDecision);
   fChain->SetBranchAddress("L0Photon,lowMultDecision", &L0Photon_lowMultDecision, &b_L0Photon_lowMultDecision);
   fChain->SetBranchAddress("L0DiEM,lowMultDecision", &L0DiEM_lowMultDecision, &b_L0DiEM_lowMultDecision);
   fChain->SetBranchAddress("L0nSelections", &L0nSelections, &b_L0nSelections);
   fChain->SetBranchAddress("Hlt1NoPVPassThroughDecision", &Hlt1NoPVPassThroughDecision, &b_Hlt1NoPVPassThroughDecision);
   fChain->SetBranchAddress("Hlt1LowMultPassThroughDecision", &Hlt1LowMultPassThroughDecision, &b_Hlt1LowMultPassThroughDecision);
   fChain->SetBranchAddress("Hlt1NoBiasNonBeamBeamDecision", &Hlt1NoBiasNonBeamBeamDecision, &b_Hlt1NoBiasNonBeamBeamDecision);
   fChain->SetBranchAddress("Hlt1MBNoBiasRateLimitedDecision", &Hlt1MBNoBiasRateLimitedDecision, &b_Hlt1MBNoBiasRateLimitedDecision);
   fChain->SetBranchAddress("Hlt1MBNoBiasDecision", &Hlt1MBNoBiasDecision, &b_Hlt1MBNoBiasDecision);
   fChain->SetBranchAddress("Hlt1CEPVeloCutDecision", &Hlt1CEPVeloCutDecision, &b_Hlt1CEPVeloCutDecision);
   fChain->SetBranchAddress("Hlt1LowMultDecision", &Hlt1LowMultDecision, &b_Hlt1LowMultDecision);
   fChain->SetBranchAddress("Hlt1LowMultMuonDecision", &Hlt1LowMultMuonDecision, &b_Hlt1LowMultMuonDecision);
   fChain->SetBranchAddress("Hlt1LowMultHerschelDecision", &Hlt1LowMultHerschelDecision, &b_Hlt1LowMultHerschelDecision);
   fChain->SetBranchAddress("Hlt1LowMultVeloCut_LeptonsDecision", &Hlt1LowMultVeloCut_LeptonsDecision, &b_Hlt1LowMultVeloCut_LeptonsDecision);
   fChain->SetBranchAddress("Hlt1LowMultVeloAndHerschel_LeptonsDecision", &Hlt1LowMultVeloAndHerschel_LeptonsDecision, &b_Hlt1LowMultVeloAndHerschel_LeptonsDecision);
   fChain->SetBranchAddress("Hlt1nSelections", &Hlt1nSelections, &b_Hlt1nSelections);
   fChain->SetBranchAddress("Hlt2NoBiasNonBeamBeamDecision", &Hlt2NoBiasNonBeamBeamDecision, &b_Hlt2NoBiasNonBeamBeamDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHDecision", &Hlt2LowMultLMR2HHDecision, &b_Hlt2LowMultLMR2HHDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHWSDecision", &Hlt2LowMultLMR2HHWSDecision, &b_Hlt2LowMultLMR2HHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHHHDecision", &Hlt2LowMultLMR2HHHHDecision, &b_Hlt2LowMultLMR2HHHHDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHHHWSDecision", &Hlt2LowMultLMR2HHHHWSDecision, &b_Hlt2LowMultLMR2HHHHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultL2pPiDecision", &Hlt2LowMultL2pPiDecision, &b_Hlt2LowMultL2pPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiDecision", &Hlt2LowMultD2KPiDecision, &b_Hlt2LowMultD2KPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiPiDecision", &Hlt2LowMultD2KPiPiDecision, &b_Hlt2LowMultD2KPiPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KKPiDecision", &Hlt2LowMultD2KKPiDecision, &b_Hlt2LowMultD2KKPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2K3PiDecision", &Hlt2LowMultD2K3PiDecision, &b_Hlt2LowMultD2K3PiDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHDecision", &Hlt2LowMultChiC2HHDecision, &b_Hlt2LowMultChiC2HHDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHHHDecision", &Hlt2LowMultChiC2HHHHDecision, &b_Hlt2LowMultChiC2HHHHDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2PPDecision", &Hlt2LowMultChiC2PPDecision, &b_Hlt2LowMultChiC2PPDecision);
   fChain->SetBranchAddress("Hlt2LowMultHadron_noTrFiltDecision", &Hlt2LowMultHadron_noTrFiltDecision, &b_Hlt2LowMultHadron_noTrFiltDecision);
   fChain->SetBranchAddress("Hlt2LowMultL2pPiWSDecision", &Hlt2LowMultL2pPiWSDecision, &b_Hlt2LowMultL2pPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiWSDecision", &Hlt2LowMultD2KPiWSDecision, &b_Hlt2LowMultD2KPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiPiWSDecision", &Hlt2LowMultD2KPiPiWSDecision, &b_Hlt2LowMultD2KPiPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KKPiWSDecision", &Hlt2LowMultD2KKPiWSDecision, &b_Hlt2LowMultD2KKPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2K3PiWSDecision", &Hlt2LowMultD2K3PiWSDecision, &b_Hlt2LowMultD2K3PiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHWSDecision", &Hlt2LowMultChiC2HHWSDecision, &b_Hlt2LowMultChiC2HHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHHHWSDecision", &Hlt2LowMultChiC2HHHHWSDecision, &b_Hlt2LowMultChiC2HHHHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2PPWSDecision", &Hlt2LowMultChiC2PPWSDecision, &b_Hlt2LowMultChiC2PPWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultMuonDecision", &Hlt2LowMultMuonDecision, &b_Hlt2LowMultMuonDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiMuonDecision", &Hlt2LowMultDiMuonDecision, &b_Hlt2LowMultDiMuonDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiMuon_PSDecision", &Hlt2LowMultDiMuon_PSDecision, &b_Hlt2LowMultDiMuon_PSDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiPhotonDecision", &Hlt2LowMultDiPhotonDecision, &b_Hlt2LowMultDiPhotonDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiPhoton_HighMassDecision", &Hlt2LowMultDiPhoton_HighMassDecision, &b_Hlt2LowMultDiPhoton_HighMassDecision);
   fChain->SetBranchAddress("Hlt2LowMultpi0Decision", &Hlt2LowMultpi0Decision, &b_Hlt2LowMultpi0Decision);
   fChain->SetBranchAddress("Hlt2LowMultDiElectronDecision", &Hlt2LowMultDiElectronDecision, &b_Hlt2LowMultDiElectronDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiElectron_noTrFiltDecision", &Hlt2LowMultDiElectron_noTrFiltDecision, &b_Hlt2LowMultDiElectron_noTrFiltDecision);
   fChain->SetBranchAddress("Hlt2MBMicroBiasVeloDecision", &Hlt2MBMicroBiasVeloDecision, &b_Hlt2MBMicroBiasVeloDecision);
   fChain->SetBranchAddress("Hlt2LowMultTechnical_MinBiasDecision", &Hlt2LowMultTechnical_MinBiasDecision, &b_Hlt2LowMultTechnical_MinBiasDecision);
   fChain->SetBranchAddress("Hlt2nSelections", &Hlt2nSelections, &b_Hlt2nSelections);
   fChain->SetBranchAddress("MaxRoutingBits", &MaxRoutingBits, &b_MaxRoutingBits);
   fChain->SetBranchAddress("RoutingBits", RoutingBits, &b_RoutingBits);
   fChain->SetBranchAddress("StrippingMBNoBiasDecision", &StrippingMBNoBiasDecision, &b_StrippingMBNoBiasDecision);
   fChain->SetBranchAddress("StrippingHlt1NoBiasNonBeamBeamLineDecision", &StrippingHlt1NoBiasNonBeamBeamLineDecision, &b_StrippingHlt1NoBiasNonBeamBeamLineDecision);
   fChain->SetBranchAddress("StrippingLowMultNonBeamBeamNoBiasLineDecision", &StrippingLowMultNonBeamBeamNoBiasLineDecision, &b_StrippingLowMultNonBeamBeamNoBiasLineDecision);
   fChain->SetBranchAddress("StrippingLowMultMuonLineDecision", &StrippingLowMultMuonLineDecision, &b_StrippingLowMultMuonLineDecision);
   fChain->SetBranchAddress("StrippingLowMultDiMuonLineDecision", &StrippingLowMultDiMuonLineDecision, &b_StrippingLowMultDiMuonLineDecision);
   fChain->SetBranchAddress("StrippingHeavyIonTopologyLowActivityLineDecision", &StrippingHeavyIonTopologyLowActivityLineDecision, &b_StrippingHeavyIonTopologyLowActivityLineDecision);
   fChain->SetBranchAddress("ET_prev1", &ET_prev1, &b_ET_prev1);
   fChain->SetBranchAddress("ET_prev2", &ET_prev2, &b_ET_prev2);
   fChain->SetBranchAddress("nouttrck", &nouttrck, &b_nouttrck);
   fChain->SetBranchAddress("trck_key", trck_key, &b_trck_key);
   fChain->SetBranchAddress("trck_type", trck_type, &b_trck_type);
   fChain->SetBranchAddress("trck_posx", trck_posx, &b_trck_posx);
   fChain->SetBranchAddress("trck_posy", trck_posy, &b_trck_posy);
   fChain->SetBranchAddress("trck_posz", trck_posz, &b_trck_posz);
   fChain->SetBranchAddress("trck_px", trck_px, &b_trck_px);
   fChain->SetBranchAddress("trck_py", trck_py, &b_trck_py);
   fChain->SetBranchAddress("trck_pz", trck_pz, &b_trck_pz);
   fChain->SetBranchAddress("trck_charge", trck_charge, &b_trck_charge);
   fChain->SetBranchAddress("trck_muonid", trck_muonid, &b_trck_muonid);
   fChain->SetBranchAddress("trck_riche", trck_riche, &b_trck_riche);
   fChain->SetBranchAddress("trck_richk", trck_richk, &b_trck_richk);
   fChain->SetBranchAddress("trck_richp", trck_richp, &b_trck_richp);
   fChain->SetBranchAddress("trck_muonll", trck_muonll, &b_trck_muonll);
   fChain->SetBranchAddress("trck_muacc", trck_muacc, &b_trck_muacc);
   fChain->SetBranchAddress("trck_ecal", trck_ecal, &b_trck_ecal);
   fChain->SetBranchAddress("trck_hcal", trck_hcal, &b_trck_hcal);
   fChain->SetBranchAddress("trck_eop", trck_eop, &b_trck_eop);
   fChain->SetBranchAddress("trck_probe", trck_probe, &b_trck_probe);
   fChain->SetBranchAddress("trck_probmu", trck_probmu, &b_trck_probmu);
   fChain->SetBranchAddress("trck_probpi", trck_probpi, &b_trck_probpi);
   fChain->SetBranchAddress("trck_probk", trck_probk, &b_trck_probk);
   fChain->SetBranchAddress("trck_probp", trck_probp, &b_trck_probp);
   fChain->SetBranchAddress("trck_probghost", trck_probghost, &b_trck_probghost);
   fChain->SetBranchAddress("noutmuon", &noutmuon, &b_noutmuon);
   fChain->SetBranchAddress("muon_chi2ndf", muon_chi2ndf, &b_muon_chi2ndf);
   fChain->SetBranchAddress("muon_posx", muon_posx, &b_muon_posx);
   fChain->SetBranchAddress("muon_posy", muon_posy, &b_muon_posy);
   fChain->SetBranchAddress("muon_ux", muon_ux, &b_muon_ux);
   fChain->SetBranchAddress("muon_uy", muon_uy, &b_muon_uy);
   fChain->SetBranchAddress("muon_nshared", muon_nshared, &b_muon_nshared);
   fChain->SetBranchAddress("nphotons", &nphotons, &b_nphotons);
   fChain->SetBranchAddress("noutphot", &noutphot, &b_noutphot);
   fChain->SetBranchAddress("photon_px", photon_px, &b_photon_px);
   fChain->SetBranchAddress("photon_py", photon_py, &b_photon_py);
   fChain->SetBranchAddress("photon_pz", photon_pz, &b_photon_pz);
   fChain->SetBranchAddress("photon_nspd", photon_nspd, &b_photon_nspd);
   fChain->SetBranchAddress("photon_nprs", photon_nprs, &b_photon_nprs);
   fChain->SetBranchAddress("noutspd", &noutspd, &b_noutspd);
   fChain->SetBranchAddress("spd_posx", spd_posx, &b_spd_posx);
   fChain->SetBranchAddress("spd_posy", spd_posy, &b_spd_posy);
   fChain->SetBranchAddress("noutprs", &noutprs, &b_noutprs);
   fChain->SetBranchAddress("prs_posx", prs_posx, &b_prs_posx);
   fChain->SetBranchAddress("prs_posy", prs_posy, &b_prs_posy);
   fChain->SetBranchAddress("noutl0mu", &noutl0mu, &b_noutl0mu);
   fChain->SetBranchAddress("l0mu_pt", l0mu_pt, &b_l0mu_pt);
   fChain->SetBranchAddress("l0mu_phi", l0mu_phi, &b_l0mu_phi);
   fChain->SetBranchAddress("l0mu_theta", l0mu_theta, &b_l0mu_theta);
   fChain->SetBranchAddress("noutl0h", &noutl0h, &b_noutl0h);
   fChain->SetBranchAddress("l0h_et", l0h_et, &b_l0h_et);
   fChain->SetBranchAddress("noutl2mu", &noutl2mu, &b_noutl2mu);
   fChain->SetBranchAddress("hlt2mu_posx", hlt2mu_posx, &b_hlt2mu_posx);
   fChain->SetBranchAddress("hlt2mu_posy", hlt2mu_posy, &b_hlt2mu_posy);
   fChain->SetBranchAddress("hlt2mu_posz", hlt2mu_posz, &b_hlt2mu_posz);
   fChain->SetBranchAddress("noutl1mu", &noutl1mu, &b_noutl1mu);
   fChain->SetBranchAddress("hlt1mu_posx", hlt1mu_posx, &b_hlt1mu_posx);
   fChain->SetBranchAddress("hlt1mu_posy", hlt1mu_posy, &b_hlt1mu_posy);
   fChain->SetBranchAddress("hlt1mu_posz", hlt1mu_posz, &b_hlt1mu_posz);
   fChain->SetBranchAddress("hlt1mu_slopex", hlt1mu_slopex, &b_hlt1mu_slopex);
   fChain->SetBranchAddress("hlt1mu_slopey", hlt1mu_slopey, &b_hlt1mu_slopey);
   Notify();
}

Bool_t makeCalSam_Ap::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void makeCalSam_Ap::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t makeCalSam_Ap::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef makeCalSam_Ap_cxx

#define makeCalSam_Ap_cxx
#include "makeCalSam_Ap.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

TNtuple *nt = new TNtuple("nt","nt","m:pt2:nph:nspd:etprev:mb:her:run:trck1_type:trck2_type");
double ped[40][6];
TMatrixD Sev(20,20);
TMatrixD Sodd(20,20);

void init_herschel(const char * indir, const char * pA)
{
	char file[60];
	FILE* fp;
	char tmp1[40],tmp2[40],tmp3[40],tmp4[40],tmp5[40],tmp6[40];
	
	// Getting pedestals
	sprintf(file,"%s/herschel_calibration/pedestals/pedestals_%s_commonskew.txt",indir,pA);
	fp = fopen(file,"r");
	if(!fp)
	{
		printf("error opening file %s \n",file);
		exit(1);
	}
	for(int i = 0;i < 40;i++)
	{
		fscanf(fp,"%s %s %s %s %s %s ",tmp1,tmp2,tmp3,tmp4,tmp5,tmp6);
		ped[i][0] = atof(tmp1);
		ped[i][1] = atof(tmp2);
		ped[i][2] = atof(tmp3);
		ped[i][3] = atof(tmp4);
		ped[i][4] = atof(tmp5);
		ped[i][5] = atof(tmp6);
	}
	fclose(fp);
	
	// Get correlation coefficients 
	// even bunch id
	sprintf(file,"%s/herschel_calibration/correlations/cal_corrcoef_%s_ev_common.txt",indir,pA);
	fp = fopen(file,"r");
	if(!fp)
	{
		printf("error opening file %s \n",file);
		exit(1);
	}
	for(int i = 0;i < 20;i++)
	{
		for(int j = i;j < 20;j++)
		{
			fscanf(fp,"%s ",tmp1);
			if(i == j)
			{
				Sev(i,j)=1.0;
			}
			else
			{
				Sev(i,j) = atof(tmp1);
			}
		}
	}
	fclose(fp);
	
	// Get correlation coefficients 
	// odd bunch id
	sprintf(file,"%s/herschel_calibration/correlations/cal_corrcoef_%s_odd_common.txt",indir,pA);
	fp = fopen(file,"r");
	if(!fp)
	{
		printf("error opening file %s \n",file);
		exit(1);
	}
	for(int i = 0;i < 20;i++)
	{
		for(int j = i;j < 20;j++)
		{
			fscanf(fp,"%s ",tmp1);
			if(i == j)
			{
				Sodd(i,j) = 1.0;
			}
			else
			{
				Sodd(i,j) = atof(tmp1);
			}
		}
	}
	fclose(fp);
	Sev.Invert();
	Sodd.Invert();
}

float get_herschel_fom(TVectorD X, int bunchid)
{
	TVectorD v = X;
	if(bunchid%2 == 0) 
	{
		v *= Sev;
	}
	else
	{
		v *= Sodd;
	}
	double d = v*X;
	d = log(d);
	return d;
}

double get_chi(int i, double adc, int bunchid)
{
	double chi=-999;
	int a = (int)i/4;
	int mod = i%4;
	int ind = a*8 + mod; 
	
	if(bunchid%2 == 1) ind += 4;
	
	if(adc < ped[ind][0])
	{
		chi = (adc - ped[ind][0])/(ped[ind][1]*(1 + ped[ind][2]*adc));
	}
	else
	{
		chi = (adc - ped[ind][0])/(ped[ind][1]);
	}
	return chi;
}

void makeCalSam_Ap::Loop()
{
	double mmu = 105.66;
	float ntuple[10];
	TLorentzVector vm[10],vx;

	init_herschel(".","Pbp");

	Long64_t nentries = fChain->GetEntriesFast();
	// nentries = 100000;
	Long64_t nbytes = 0, nb = 0;
	for (Long64_t jentry = 0;jentry < nentries;jentry++)
	{
		if(jentry/100000 == (jentry + 99999)/100000) cout << jentry << endl;
		Long64_t ientry = LoadTree(jentry);
		if (ientry < 0) break;
		nb = fChain->GetEntry(jentry); nbytes += nb;
		// if (Cut(ientry) < 0) continue;
		if((Hlt2LowMultMuonDecision > 0 || Hlt2LowMultDiMuonDecision > 0) && nVeloTracks == 2)
		{
			int nuse = 0;
			int nmuon = 0;
			for(int i = 0;i < nouttrck;i++)
			{
				if((trck_type[i] == 3) || (trck_type[i] == 4))
				{
					if(nuse < 2)
					{
						vm[nuse].SetXYZM(trck_px[i],trck_py[i],trck_pz[i],mmu);
						if(trck_muonid[i] > 0) nmuon += 1;
						nuse += 1;
					}
				}
			}
			// neutrals  ??? 730 for extrap to ecal
			int ngamma = 0;
			for(int i = 0;i < noutphot;i++)
			{
				int iflag = 1;
				for(int j = 0;j < nouttrck;j++)
				{
					float dely = photon_py[i]/photon_pz[i] - trck_py[j]/trck_pz[j];
					float delx = photon_px[i]/photon_pz[i] - trck_px[j]/trck_pz[j] - 780*trck_charge[j]/trck_pz[j]; //down
					if(fabs(delx) < .025 && fabs(dely) < .025) iflag = 0;
				}
				if(photon_nprs[i] > 0 && iflag > 0)
				{
					ngamma += 1;
				}
			}
			if(nuse == 2 && nmuon == 2)
			{
				vx = vm[0] + vm[1];
				ntuple[0] = vx.M();
				ntuple[1] = pow(vx.Pt()/1000.,2);
				ntuple[2] = ngamma;
				ntuple[3] = nSPDHits;
				ntuple[4] = ET_prev1;
				ntuple[5] = Hlt2MBMicroBiasVeloDecision;
				TVectorD herchi(20);
				int X[20];
				int bunchid = BCID;
				X[0] = B00;
				X[1] = B01;
				X[2] = B02;
				X[3] = B03;
				X[4] = B10;
				X[5] = B11;
				X[6] = B12;
				X[7] = B13;
				X[8] = B20;
				X[9] = B21;
				X[10] = B22;
				X[11] = B23;
				X[12] = F10;
				X[13] = F11;
				X[14] = F12;
				X[15] = F13;
				X[16] = F20;
				X[17] = F21;
				X[18] = F22;
				X[19] = F23;
				for(int i = 0;i < 20;i++)
				{
					herchi(i) = get_chi(i, X[i], bunchid);
				}
				float fom = get_herschel_fom(herchi, bunchid);
				ntuple[6] = fom;
				ntuple[7] = runNumber;
				ntuple[8] = trck_type[0];
				ntuple[9] = trck_type[1];
				// cout << runNumber << " " << eventNumber << " " << fom << " bunchid " << bunchid << endl;
				nt->Fill(ntuple);
			}
		}
	}
	
	TFile *f = new TFile("Ap_tmp.root","recreate");
	nt->Write();
	f->Close();
}

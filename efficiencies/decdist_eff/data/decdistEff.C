#include "../../../useful_variables.C"
#include "../../../useful_variables.h"

// variables used to store efficiencies
double pA_eff_array[6], pA_eff_err_array[6];
double Ap_eff_array[6], Ap_eff_err_array[6];
double avg_eff_array[6], avg_eff_err_array[6];

// array variables for plotting table
double F_array[4], mu1_array[4], sigma1_array[4], mu2_array[4], sigma2_array[4];
double F_err_array[4], mu1_err_array[4], sigma1_err_array[4], mu2_err_array[4], sigma2_err_array[4];
double chi2_array[4], ndf_array[4], prob_array[4];
double fxv[2] = {0,0}, fxv_err[2] = {0,0}, fyv[2] = {0,0}, fyv_err[2] = {0,0};
void printTab()
{	
	// make sure the dominant values are shown first
	for(int i  = 0;i < 4;i++)
	{
		printf("%d: F = %f\n",i,F_array[i]);
		if(F_array[i] < 0.5)
		{
			F_array[i] = 1 - F_array[i];

			double mutemp = mu1_array[i];
			double mutemp_err = mu1_err_array[i];
			mu1_array[i] = mu2_array[i];
			mu1_err_array[i] = mu2_err_array[i];
			mu2_array[i] = mutemp;
			mu2_err_array[i] = mutemp_err;

			double sigtemp = sigma1_array[i];
			double sigtemp_err = sigma1_err_array[i];
			sigma1_array[i] = sigma2_array[i];
			sigma1_err_array[i] = sigma2_err_array[i];
			sigma2_array[i] = sigtemp;
			sigma2_err_array[i] = sigtemp_err;
		}
	}

	printf("\nFit Summary Table:\n");
	printf("\\begin{tabular}{|c|c|c|c|c|c|c|c|} \\hline\n");
	printf("\tCoordinate & $\\mathcal{F}$ & $\\mu_{1}$ [mm] & $\\sigma_{1}$ [mm] & $\\mu_{2}$ [mm] & $\\sigma_{2}$ [mm] & $\\chi^{2}$/NDFs & $P\\left(\\chi^{2}\\right)$ \\\\ \\hline\n");
	printf("\t\\multicolumn{8}{c}{p-Pb Data} \\\\ \\hline\n");
	printf("\t$x_{v}$ & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & %g/%g & %g \\\\ \\hline\n",F_array[0],mu1_array[0],sigma1_array[0],mu2_array[0],sigma2_array[0],chi2_array[0],ndf_array[0],prob_array[0]);
	printf("\t$y_{v}$ & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & %g/%g & %g \\\\ \\hline\n",F_array[1],mu1_array[1],sigma1_array[1],mu2_array[1],sigma2_array[1],chi2_array[1],ndf_array[1],prob_array[1]);
	printf("\t\\multicolumn{8}{c}{Pb-p Data} \\\\ \\hline\n");
	printf("\t$x_{v}$ & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & %g/%g & %g \\\\ \\hline\n",F_array[2],mu1_array[2],sigma1_array[2],mu2_array[2],sigma2_array[2],chi2_array[2],ndf_array[2],prob_array[2]);
	printf("\t$y_{v}$ & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & \\num[round-mode=figures,round-precision=3]{%f} & %g/%g & %g \\\\ \\hline\n",F_array[3],mu1_array[3],sigma1_array[3],mu2_array[3],sigma2_array[3],chi2_array[3],ndf_array[3],prob_array[3]);
	printf("\\end{tabular}\n");

	printf("\nBeam-Line Location Summary Table:\n");
	printf("\\begin{tabular}{|c|c|c|} \\hline\n");
	printf("\tDataset & $x_{BL}$ & $y_{BL}$  \\\\ \\hline\n");
	printf("\tp-Pb & \\SI{%f(%f)}{\\mm} & \\SI{%f(%f)}{\\mm} \\\\ \\hline\n",fxv[0],fxv_err[0],fyv[0],fyv_err[0]);
	printf("\tPb-p & \\SI{%f(%f)}{\\mm} & \\SI{%f(%f)}{\\mm} \\\\ \\hline\n",fxv[1],fxv_err[1],fyv[1],fyv_err[1]);
	printf("\\end{tabular}\n\n");

	printf("Efficiency Summary Table\n");
	printf("\\begin{tabular}{|c|c|} \\hline\n");
	printf("\t$y$-interval & $\\overline{\\varepsilon}_{\\text{decay-distance}}$ \\\\ \\hline\n");
	printf("\t(2,2.5) & \\num{%f(%f)} \\\\ \\hline\n",avg_eff_array[0],avg_eff_err_array[0]);
	printf("\t(2.5,3) & \\num{%f(%f)} \\\\ \\hline\n",avg_eff_array[1],avg_eff_err_array[1]);
	printf("\t(3,3.5) & \\num{%f(%f)} \\\\ \\hline\n",avg_eff_array[2],avg_eff_err_array[2]);
	printf("\t(3.5,4) & \\num{%f(%f)} \\\\ \\hline\n",avg_eff_array[3],avg_eff_err_array[3]);
	printf("\t(4,4.5) & \\num{%f(%f)} \\\\ \\hline\n",avg_eff_array[4],avg_eff_err_array[4]);
	printf("\t(4.5,5) & \\num{%f(%f)} \\\\ \\hline\n",avg_eff_array[5],avg_eff_err_array[5]);
	printf("\\end{tabular}\n");
}

void getEff(TH1D *hDsi,double &eff,double &eff_err,double cut)
{
	int cut_bin = hDsi->FindFixBin(-cut+0.05);
	int mid_bin = hDsi->FindFixBin(0-0.05);

	double nevts_pre_err;
	double nevts_pre = hDsi->IntegralAndError(1,mid_bin,nevts_pre_err);

	double nevts_post_err;
	double nevts_post = hDsi->IntegralAndError(cut_bin,mid_bin,nevts_post_err);

	eff = nevts_post/nevts_pre;
	eff_err = binomialError(nevts_post,nevts_post_err,nevts_pre,nevts_pre_err);
}

void getPurity(TH1D *hDsi,double &pur,double &pur_err,double cut)
{
	int left_cut_bin = hDsi->FindFixBin(-cut+0.05);
	int left_zero_bin = hDsi->FindFixBin(0-0.05);
	int right_cut_bin = hDsi->FindFixBin(cut-0.05);
	int right_zero_bin = hDsi->FindFixBin(0+0.05);

	double nevts_left_err;
	double nevts_left = hDsi->IntegralAndError(left_cut_bin,left_zero_bin,nevts_left_err);

	double nevts_right_err;
	double nevts_right = hDsi->IntegralAndError(right_zero_bin,right_cut_bin,nevts_right_err);

	double nevts_con = nevts_right - nevts_left;
	double nevts_con_err = sqrt(pow(nevts_right_err,2) + pow(nevts_left_err,2));

	double nevts_tot_err;
	double nevts_tot =  hDsi->IntegralAndError(left_cut_bin,right_cut_bin,nevts_tot_err);

	double nevts_sig = nevts_tot - nevts_con;
	double nevts_sig_err = sqrt(pow(nevts_tot_err,2) + pow(nevts_con_err,2));

	pur = nevts_sig/nevts_tot;
	pur_err = errDiv(nevts_sig,nevts_sig_err,nevts_tot,nevts_tot_err);
}

void decdistEff()
{
	///////////////////
	// Preliminaries //
	///////////////////

	// start timer
	auto stopwatch = new TStopwatch();
	stopwatch->Start();

	// set LHCb style
	gROOT->ProcessLine(".L ../../../lhcbStyle.C");

    	// get the ntuples
    	auto pA_data_file = new TFile("../../../ntuple_maker/pA_mb_ntuple.root");
    	TNtuple* pA_nt = (TNtuple*)pA_data_file->Get("nt");
    	auto Ap_data_file = new TFile("../../../ntuple_maker/Ap_mb_ntuple.root");
    	TNtuple* Ap_nt = (TNtuple*)Ap_data_file->Get("nt");

	// set cut value to use in script
	double cut = 1;

	////////////////////////////
	// Selection Requirements //
	////////////////////////////

	TCut y_cut = "(2 < y) && (y < 5)"; // tracking
	//TCut trck_type = "(trck1_type == 3) && (trck2_type == 3)"; // select long tracks
	TCut trck_type = "((trck1_type == 3) || (trck1_type == 4) && (trck2_type == 3) || (trck2_type == 4))"; // use long or upstream tracks
	TCut no_el = "((trck1_eop < 0.7) && (trck2_eop < 0.7))"; // cut electrons
	TCut no_pho = "(ngamma == 0)"; // photon-veto
	TCut os_cut = "((trck1_charge * trck2_charge) == -1)"; // opposite-sign

	// define kaon veto
	TCut kv = "((trck1_richk <= 0) && (trck2_richk <= 0))";

	// define herschel veto
	TCut her = "(her < 3.5)";

	TCut presel = y_cut&&trck_type&&os_cut&&no_pho;

	TCut pA_dd = "(1 > sqrt(pow((xv - (0.863)),2) + pow((yv - (-0.158)),2)))";
	TCut Ap_dd = "(1 > sqrt(pow((xv - (0.881)),2) + pow((yv - (-0.138)),2)))";

	TCut pA_antidd = "(1 < sqrt(pow((xv - (0.863)),2) + pow((yv - (-0.158)),2)))";
	TCut Ap_antidd = "(1 < sqrt(pow((xv - (0.881)),2) + pow((yv - (-0.138)),2)))";

	// plot beam histograms
	cout << "Making and plotting beam histograms..." << endl;

	// declare and initialise histogram list
	auto h_list = new TList();

	///////////////////////
	// Get Beam Location //
	///////////////////////

	// project the PVs into histograms
	auto pA_hxv = new TH1D("pA_hxv","p-Pb Data",60,-0.637,2.363); h_list->Add(pA_hxv);
	pA_nt->Project("pA_hxv","xv",presel);
	auto pA_hyv = new TH1D("pA_hyv","p-Pb Data",60,-1.658,1.342); h_list->Add(pA_hyv);
	pA_nt->Project("pA_hyv","yv",presel);
	auto Ap_hxv = new TH1D("Ap_hxv","Pb-p Data",60,-0.619,2.381); h_list->Add(Ap_hxv);
	Ap_nt->Project("Ap_hxv","xv",presel);
	auto Ap_hyv = new TH1D("Ap_hyv","Pb-p Data",60,-1.636,1.362); h_list->Add(Ap_hyv);
	Ap_nt->Project("Ap_hyv","yv",presel);

	// fit Gaussians to the PV coordinate distributions
	auto beam_cvs = new TCanvas("beam_cvs","PV Coordinate Distributions");	
	beam_cvs->Divide(2,2);
	beam_cvs->cd(1);
	gPad->SetLogy();
	pA_hxv->GetXaxis()->SetTitle("x_{pv} [mm]");
	setLabelY(pA_hxv,"mm");
	pA_hxv->SetMarkerColor(kBlue);
	pA_hxv->SetLineColor(kBlue);
	auto pA_fxv = new TF1("pA_fxv","[0]*([1]/sqrt(2*pi)/[3]*exp(-0.5*pow((x-[2])/[3],2))+(1-[1])/sqrt(2*pi)/[5]*exp(-0.5*pow((x-[4])/[5],2)))",-2,2);
	pA_fxv->SetParameters(30000,0,0.8,0.1,0.8,0.1);
	pA_fxv->SetParNames("N","F","mu_1","sigma_1","mu_2","sigma_2");
	pA_fxv->SetParLimits(1,0,1);
	pA_fxv->SetParLimits(2,0.8,0.9);
	pA_fxv->SetParLimits(4,0.8,0.9);

	pA_fxv->SetTitle("Fit Result");
	pA_fxv->SetLineColor(kBlue-1);
	pA_fxv->SetLineWidth(3);
	pA_hxv->Draw("E1");
	pA_hxv->Fit("pA_fxv");
	pA_hxv->SetMaximum(pA_fxv->GetMaximum());
	F_array[0] = pA_fxv->GetParameter(1); F_err_array[0] = pA_fxv->GetParError(1);
	mu1_array[0] = pA_fxv->GetParameter(2); mu1_err_array[0] = pA_fxv->GetParError(2);
	sigma1_array[0] = pA_fxv->GetParameter(3); sigma1_err_array[0] = pA_fxv->GetParError(3);
	mu2_array[0] = pA_fxv->GetParameter(4); mu2_err_array[0] = pA_fxv->GetParError(4);
	sigma2_array[0] = pA_fxv->GetParameter(5); sigma2_err_array[0] = pA_fxv->GetParError(5);
	chi2_array[0] = pA_fxv->GetChisquare();
	ndf_array[0] = pA_fxv->GetNDF();
	prob_array[0] = pA_fxv->GetProb();
	fxv[0] = F_array[0]*mu1_array[0] + (1-F_array[0])*mu2_array[0];
	fxv_err[0] = sqrt(pow(pA_fxv->GetParError(2),2)/2 + pow(pA_fxv->GetParError(4),2)/2);
	double pA_hxv_nevts_peak;
	if(sigma1_array[0] > sigma2_array[0]) pA_hxv_nevts_peak = pA_hxv->Integral(pA_hxv->FindBin(mu1_array[0] - 2*sigma1_array[0]),pA_hxv->FindBin(mu1_array[0] + 2*sigma1_array[0]));
	else pA_hxv_nevts_peak = pA_hxv->Integral(pA_hxv->FindBin(mu2_array[0] - 2*sigma2_array[0]),pA_hxv->FindBin(mu2_array[0] + 2*sigma2_array[0]));
	double pA_hxv_nevts_tot = pA_hxv->Integral(1,pA_hxv->GetNbinsX());
	printf("%% events in peak for %s: %f\n",pA_hxv->GetName(),pA_hxv_nevts_peak/pA_hxv_nevts_tot);

	beam_cvs->cd(2);
	gPad->SetLogy();
	pA_hyv->GetXaxis()->SetTitle("y_{pv} [mm]");
	setLabelY(pA_hyv,"mm");
	pA_hyv->SetMarkerColor(kBlue);
	pA_hyv->SetLineColor(kBlue);
	auto pA_fyv = new TF1("pA_fyv","[0]*([1]/sqrt(2*pi)/[3]*exp(-0.5*pow((x-[2])/[3],2))+(1-[1])/sqrt(2*pi)/[5]*exp(-0.5*pow((x-[4])/[5],2)))",-5,5);
	pA_fyv->SetParameters(30000,0,-0.1,0.1,-0.1,0.1);
	pA_fyv->SetParNames("N","F","mu_1","sigma_1","mu_2","sigma_2");
	pA_fyv->SetParLimits(1,0,1);
	pA_fyv->SetParLimits(2,-0.2,-0.1);
	pA_fyv->SetParLimits(4,-0.2,-0.1);
	pA_fyv->SetTitle("Fit Result");
	pA_fyv->SetLineColor(kBlue-1);
	pA_fyv->SetLineWidth(3);
	pA_hyv->Draw("E1");
	pA_hyv->Fit("pA_fyv","E");
	pA_hyv->SetMaximum(pA_fyv->GetMaximum());
	F_array[1] = pA_fyv->GetParameter(1); F_err_array[1] = pA_fyv->GetParError(1);
	mu1_array[1] = pA_fyv->GetParameter(2); mu1_err_array[1] = pA_fyv->GetParError(2);
	sigma1_array[1] = pA_fyv->GetParameter(3); sigma1_err_array[1] = pA_fyv->GetParError(3);
	mu2_array[1] = pA_fyv->GetParameter(4); mu2_err_array[1] = pA_fyv->GetParError(4);
	sigma2_array[1] = pA_fyv->GetParameter(5); sigma2_err_array[1] = pA_fyv->GetParError(5);
	chi2_array[1] = pA_fyv->GetChisquare();
	ndf_array[1] = pA_fyv->GetNDF();
	prob_array[1] = pA_fyv->GetProb();
	fyv[0] = F_array[1]*mu1_array[1] + (1-F_array[1])*mu2_array[1];
	fyv_err[0] = sqrt(pow(pA_fyv->GetParError(2),2)/2 + pow(pA_fyv->GetParError(4),2)/2);
	double pA_hyv_nevts_peak;
	if(sigma1_array[1] > sigma2_array[1]) pA_hyv_nevts_peak = pA_hyv->Integral(pA_hyv->FindBin(mu1_array[1] - 2*sigma1_array[1]),pA_hyv->FindBin(mu1_array[1] + 2*sigma1_array[1]));
	else pA_hyv_nevts_peak = pA_hyv->Integral(pA_hyv->FindBin(mu2_array[1] - 2*sigma2_array[1]),pA_hyv->FindBin(mu2_array[1] + 2*sigma2_array[1]));
	double pA_hyv_nevts_tot = pA_hyv->Integral(1,pA_hyv->GetNbinsX());
	printf("%% events in peak for %s: %f\n",pA_hyv->GetName(),pA_hyv_nevts_peak/pA_hyv_nevts_tot);

	beam_cvs->cd(3);
	gPad->SetLogy();
	Ap_hxv->GetXaxis()->SetTitle("x_{pv} [mm]");
	setLabelY(Ap_hxv,"mm");
	Ap_hxv->SetMarkerColor(kRed);
	Ap_hxv->SetLineColor(kRed);
	auto Ap_fxv = new TF1("Ap_fxv","[0]*([1]/sqrt(2*pi)/[3]*exp(-0.5*pow((x-[2])/[3],2))+(1-[1])/sqrt(2*pi)/[5]*exp(-0.5*pow((x-[4])/[5],2)))",-5,5);
	Ap_fxv->SetParameters(30000,0,0.8,0.1,0.8,0.1);
	Ap_fxv->SetParNames("N","F","mu_1","sigma_1","mu_2","sigma_2");
	Ap_fxv->SetParLimits(1,0,1);
	Ap_fxv->SetParLimits(2,0.8,0.9);
	Ap_fxv->SetParLimits(4,0.8,0.9);
	Ap_fxv->SetTitle("Fit Result");
	Ap_fxv->SetLineColor(kRed-1);
	Ap_fxv->SetLineWidth(3);
	Ap_hxv->Draw("E1");
	Ap_hxv->Fit("Ap_fxv","E");
	Ap_hxv->SetMaximum(Ap_fxv->GetMaximum());
	F_array[2] = Ap_fxv->GetParameter(1); F_err_array[2] = Ap_fxv->GetParError(1);
	mu1_array[2] = Ap_fxv->GetParameter(2); mu1_err_array[2] = Ap_fxv->GetParError(2);
	sigma1_array[2] = Ap_fxv->GetParameter(3); sigma1_err_array[2] = Ap_fxv->GetParError(3);
	mu2_array[2] = Ap_fxv->GetParameter(4); mu2_err_array[2] = Ap_fxv->GetParError(4);
	sigma2_array[2] = Ap_fxv->GetParameter(5); sigma2_err_array[2] = Ap_fxv->GetParError(5);
	chi2_array[2] = Ap_fxv->GetChisquare();
	ndf_array[2] = Ap_fxv->GetNDF();
	prob_array[2] = Ap_fxv->GetProb();
	fxv[1] = F_array[2]*mu1_array[2] + (1-F_array[2])*mu2_array[2];
	fxv_err[1] = sqrt(pow(Ap_fxv->GetParError(2),2)/2 + pow(Ap_fxv->GetParError(4),2)/2);
	double Ap_hxv_nevts_peak;
	if(sigma1_array[2] > sigma2_array[2]) Ap_hxv_nevts_peak = Ap_hxv->Integral(Ap_hxv->FindBin(mu1_array[2] - 2*sigma1_array[2]),Ap_hxv->FindBin(mu1_array[2] + 2*sigma1_array[2]));
	else Ap_hxv_nevts_peak = Ap_hxv->Integral(Ap_hxv->FindBin(mu2_array[2] - 2*sigma2_array[2]),Ap_hxv->FindBin(mu2_array[2] + 2*sigma2_array[2]));
	double Ap_hxv_nevts_tot = Ap_hxv->Integral(1,Ap_hxv->GetNbinsX());
	printf("%% events in peak for %s: %f\n",Ap_hxv->GetName(),Ap_hxv_nevts_peak/Ap_hxv_nevts_tot);

	beam_cvs->cd(4);
	gPad->SetLogy();
	Ap_hyv->GetXaxis()->SetTitle("y_{pv} [mm]");
	setLabelY(Ap_hyv,"mm");
	Ap_hyv->SetMarkerColor(kRed);
	Ap_hyv->SetLineColor(kRed);
	auto Ap_fyv = new TF1("Ap_fyv","[0]*([1]/sqrt(2*pi)/[3]*exp(-0.5*pow((x-[2])/[3],2))+(1-[1])/sqrt(2*pi)/[5]*exp(-0.5*pow((x-[4])/[5],2)))",-5,5);
	Ap_fyv->SetParameters(30000,0,-0.1,0.1,-0.1,0.1);
	Ap_fyv->SetParNames("N","F","mu_1","sigma_1","mu_2","sigma_2");
	Ap_fyv->SetParLimits(1,0,1);
	Ap_fyv->SetParLimits(2,-0.15,-0.1);
	Ap_fyv->SetParLimits(4,-0.15,-0.1);
	Ap_fyv->SetTitle("Fit Result");
	Ap_fyv->SetLineColor(kRed-1);
	Ap_fyv->SetLineWidth(3);
	Ap_hyv->Draw("E1");
	Ap_hyv->Fit("Ap_fyv","E");
	Ap_hyv->SetMaximum(Ap_fyv->GetMaximum());
	F_array[3] = Ap_fyv->GetParameter(1); F_err_array[3] = Ap_fyv->GetParError(1);
	mu1_array[3] = Ap_fyv->GetParameter(2); mu1_err_array[3] = Ap_fyv->GetParError(2);
	sigma1_array[3] = Ap_fyv->GetParameter(3); sigma1_err_array[3] = Ap_fyv->GetParError(3);
	mu2_array[3] = Ap_fyv->GetParameter(4); mu2_err_array[3] = Ap_fyv->GetParError(4);
	sigma2_array[3] = Ap_fyv->GetParameter(5); sigma2_err_array[3] = Ap_fyv->GetParError(5);
	chi2_array[3] = Ap_fyv->GetChisquare();
	ndf_array[3] = Ap_fyv->GetNDF();
	prob_array[3] = Ap_fyv->GetProb();
	fyv[1] = F_array[3]*mu1_array[3] + (1-F_array[3])*mu2_array[3];
	fyv_err[1] = sqrt(pow(Ap_fyv->GetParError(2),2)/2 + pow(Ap_fyv->GetParError(4),2)/2);
	double Ap_hyv_nevts_peak;
	if(sigma1_array[3] > sigma2_array[3]) Ap_hyv_nevts_peak = Ap_hyv->Integral(Ap_hyv->FindBin(mu1_array[3] - 2*sigma1_array[3]),Ap_hyv->FindBin(mu1_array[3] + 2*sigma1_array[3]));
	else Ap_hyv_nevts_peak = Ap_hyv->Integral(Ap_hyv->FindBin(mu2_array[3] - 2*sigma2_array[3]),Ap_hyv->FindBin(mu2_array[3] + 2*sigma2_array[3]));
	double Ap_hyv_nevts_tot = Ap_hyv->Integral(1,Ap_hyv->GetNbinsX());
	printf("%% events in peak for %s: %f\n",Ap_hyv->GetName(),Ap_hyv_nevts_peak/Ap_hyv_nevts_tot);

	/////////////////////////
	// Plot Decay Distance //
	/////////////////////////

	// make and plot histograms for the transverse decay-distance
	cout << "Making and plotting histograms for the transverse decay-distance..." << endl;
	auto pA_hD = new TH1D("pA_hD","p-Pb Data",50,0,5); h_list->Add(pA_hD);
	pA_nt->Project("pA_hD","sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2))",presel);
	auto Ap_hD = new TH1D("Ap_hD","Pb-p Data",50,0,5); h_list->Add(Ap_hD);
	Ap_nt->Project("Ap_hD","sqrt(pow(xv-0.881,2)+pow(yv-(-0.138),2))",presel);
	auto hD_cvs = new TCanvas("hD_cvs","Transverse Decay-Distance");	
	hD_cvs->Divide(2,1);
	hD_cvs->cd(1);
	pA_hD->GetXaxis()->SetTitle("D_{T} [mm]");
	setLabelY(pA_hD,"mm");
	pA_hD->SetMarkerColor(kBlue);
	pA_hD->SetLineColor(kBlue);
	pA_hD->Draw("E1");
	hD_cvs->cd(2);
	Ap_hD->GetXaxis()->SetTitle("D_{T} [mm]");
	setLabelY(Ap_hD,"mm");
	Ap_hD->SetMarkerColor(kRed);
	Ap_hD->SetLineColor(kRed);
	Ap_hD->Draw("E1");

	// make and plot histograms for the signed transverse decay-distance

	cout << "Making and plotting histograms for the signed transverse decay-distance..." << endl;
	auto hDsi_cvs = new TCanvas("hDsi_cvs","Signed Transverse Decay-Distance");	
	hDsi_cvs->Divide(2,1);
	hDsi_cvs->cd(1);
	auto pA_hDsi = new TH1D("pA_hDsi","p-Pb Data",200,-10,10); h_list->Add(pA_hDsi);
	pA_nt->Project("pA_hDsi","TMath::Sign(1,((trck1_px+trck2_px)*(xv-0.863)+(trck1_py+trck2_py)*(yv-(-0.158))))*sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2))",presel);
	pA_hDsi->GetXaxis()->SetTitle("Signed D_{T} [mm]");
	setLabelY(pA_hDsi,"mm");
	pA_hDsi->SetMarkerColor(kBlue);
	pA_hDsi->SetLineColor(kBlue);
	pA_hDsi->Draw("E1");
	hDsi_cvs->cd(2);
	auto Ap_hDsi = new TH1D("Ap_hDsi","Pb-p Data",200,-10,10); h_list->Add(Ap_hDsi);
	Ap_nt->Project("Ap_hDsi","TMath::Sign(1,((trck1_px+trck2_px)*(xv-0.881)+(trck1_py+trck2_py)*(yv-(-0.138))))*sqrt(pow(xv-0.881,2)+pow(yv-(-0.138),2))",presel);
	Ap_hDsi->GetXaxis()->SetTitle("Signed D_{T} [mm]");
	setLabelY(Ap_hDsi,"mm");
	Ap_hDsi->SetMarkerColor(kRed);
	Ap_hDsi->SetLineColor(kRed);
	Ap_hDsi->Draw("E1");

	// make histograms for signed D_T in bins of rapidity

	auto pA_hDsi0 = new TH1D("pA_hDsi0","p-Pb Data",200,-10,10); h_list->Add(pA_hDsi0);
	pA_nt->Project("pA_hDsi0","TMath::Sign(1,((trck1_px+trck2_px)*(xv-0.863)+(trck1_py+trck2_py)*(yv-(-0.158))))*sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2))",presel&&"(y<2.5)");
	auto pA_hDsi1 = new TH1D("pA_hDsi1","p-Pb Data",200,-10,10); h_list->Add(pA_hDsi1);
	pA_nt->Project("pA_hDsi1","TMath::Sign(1,((trck1_px+trck2_px)*(xv-0.863)+(trck1_py+trck2_py)*(yv-(-0.158))))*sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2))",presel&&"(2.5<y)&&(y<3)");
	auto pA_hDsi2 = new TH1D("pA_hDsi2","p-Pb Data",200,-10,10); h_list->Add(pA_hDsi2);
	pA_nt->Project("pA_hDsi2","TMath::Sign(1,((trck1_px+trck2_px)*(xv-0.863)+(trck1_py+trck2_py)*(yv-(-0.158))))*sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2))",presel&&"(3<y)&&(y<3.5)");
	auto pA_hDsi3 = new TH1D("pA_hDsi3","p-Pb Data",200,-10,10); h_list->Add(pA_hDsi3);
	pA_nt->Project("pA_hDsi3","TMath::Sign(1,((trck1_px+trck2_px)*(xv-0.863)+(trck1_py+trck2_py)*(yv-(-0.158))))*sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2))",presel&&"(3.5<y)&&(y<4)");
	auto pA_hDsi4 = new TH1D("pA_hDsi4","p-Pb Data",200,-10,10); h_list->Add(pA_hDsi4);
	pA_nt->Project("pA_hDsi4","TMath::Sign(1,((trck1_px+trck2_px)*(xv-0.863)+(trck1_py+trck2_py)*(yv-(-0.158))))*sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2))",presel&&"(4<y)&&(y<4.5)");
	auto pA_hDsi5 = new TH1D("pA_hDsi5","p-Pb Data",200,-10,10); h_list->Add(pA_hDsi5);
	pA_nt->Project("pA_hDsi5","TMath::Sign(1,((trck1_px+trck2_px)*(xv-0.863)+(trck1_py+trck2_py)*(yv-(-0.158))))*sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2))",presel&&"(4.5<y)&&(y<5)");

	auto Ap_hDsi0 = new TH1D("Ap_hDsi0","p-Pb Data",200,-10,10); h_list->Add(Ap_hDsi0);
	Ap_nt->Project("Ap_hDsi0","TMath::Sign(1,((trck1_px+trck2_px)*(xv-0.863)+(trck1_py+trck2_py)*(yv-(-0.158))))*sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2))",presel&&"(y<2.5)");
	auto Ap_hDsi1 = new TH1D("Ap_hDsi1","p-Pb Data",200,-10,10); h_list->Add(Ap_hDsi1);
	Ap_nt->Project("Ap_hDsi1","TMath::Sign(1,((trck1_px+trck2_px)*(xv-0.863)+(trck1_py+trck2_py)*(yv-(-0.158))))*sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2))",presel&&"(2.5<y)&&(y<3)");
	auto Ap_hDsi2 = new TH1D("Ap_hDsi2","p-Pb Data",200,-10,10); h_list->Add(Ap_hDsi2);
	Ap_nt->Project("Ap_hDsi2","TMath::Sign(1,((trck1_px+trck2_px)*(xv-0.863)+(trck1_py+trck2_py)*(yv-(-0.158))))*sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2))",presel&&"(3<y)&&(y<3.5)");
	auto Ap_hDsi3 = new TH1D("Ap_hDsi3","p-Pb Data",200,-10,10); h_list->Add(Ap_hDsi3);
	Ap_nt->Project("Ap_hDsi3","TMath::Sign(1,((trck1_px+trck2_px)*(xv-0.863)+(trck1_py+trck2_py)*(yv-(-0.158))))*sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2))",presel&&"(3.5<y)&&(y<4)");
	auto Ap_hDsi4 = new TH1D("Ap_hDsi4","p-Pb Data",200,-10,10); h_list->Add(Ap_hDsi4);
	Ap_nt->Project("Ap_hDsi4","TMath::Sign(1,((trck1_px+trck2_px)*(xv-0.863)+(trck1_py+trck2_py)*(yv-(-0.158))))*sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2))",presel&&"(4<y)&&(y<4.5)");
	auto Ap_hDsi5 = new TH1D("Ap_hDsi5","p-Pb Data",200,-10,10); h_list->Add(Ap_hDsi5);
	Ap_nt->Project("Ap_hDsi5","TMath::Sign(1,((trck1_px+trck2_px)*(xv-0.863)+(trck1_py+trck2_py)*(yv-(-0.158))))*sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2))",presel&&"(4.5<y)&&(y<5)");

	// calculate efficiency using signed decay distance
	cout << "Calculating efficiencies..." << endl;
	double pA_eff, pA_eff_err;
	getEff(pA_hDsi,pA_eff,pA_eff_err,cut);
	printf("pA Efficiency: %f +/- %f\n",pA_eff,pA_eff_err);
	double Ap_eff, Ap_eff_err;
	getEff(Ap_hDsi,Ap_eff,Ap_eff_err,cut);
	printf("Ap Efficiency: %f +/- %f\n",Ap_eff,Ap_eff_err);

	// calculate the efficiencies in bins of rapidity

	getEff(pA_hDsi0,pA_eff_array[0],pA_eff_err_array[0],cut);
	getEff(pA_hDsi1,pA_eff_array[1],pA_eff_err_array[1],cut);
	getEff(pA_hDsi2,pA_eff_array[2],pA_eff_err_array[2],cut);
	getEff(pA_hDsi3,pA_eff_array[3],pA_eff_err_array[3],cut);
	getEff(pA_hDsi4,pA_eff_array[4],pA_eff_err_array[4],cut);
	getEff(pA_hDsi5,pA_eff_array[5],pA_eff_err_array[5],cut);

	getEff(Ap_hDsi0,Ap_eff_array[0],Ap_eff_err_array[0],cut);
	getEff(Ap_hDsi1,Ap_eff_array[1],Ap_eff_err_array[1],cut);
	getEff(Ap_hDsi2,Ap_eff_array[2],Ap_eff_err_array[2],cut);
	getEff(Ap_hDsi3,Ap_eff_array[3],Ap_eff_err_array[3],cut);
	getEff(Ap_hDsi4,Ap_eff_array[4],Ap_eff_err_array[4],cut);
	getEff(Ap_hDsi5,Ap_eff_array[5],Ap_eff_err_array[5],cut);

	// store the average efficiencies across the datasets
	avg_eff_array[0] = (pA_eff_array[0] + Ap_eff_array[0])/2;
	avg_eff_err_array[0] = sqrt(pow(pA_eff_err_array[0],2) + pow(Ap_eff_err_array[0],2))/2;
	avg_eff_array[1] = (pA_eff_array[1] + Ap_eff_array[1])/2;
	avg_eff_err_array[1] = sqrt(pow(pA_eff_err_array[1],2) + pow(Ap_eff_err_array[1],2))/2;
	avg_eff_array[2] = (pA_eff_array[2] + Ap_eff_array[2])/2;
	avg_eff_err_array[2] = sqrt(pow(pA_eff_err_array[2],2) + pow(Ap_eff_err_array[2],2))/2;
	avg_eff_array[3] = (pA_eff_array[3] + Ap_eff_array[3])/2;
	avg_eff_err_array[3] = sqrt(pow(pA_eff_err_array[3],2) + pow(Ap_eff_err_array[3],2))/2;
	avg_eff_array[4] = (pA_eff_array[4] + Ap_eff_array[4])/2;
	avg_eff_err_array[4] = sqrt(pow(pA_eff_err_array[4],2) + pow(Ap_eff_err_array[4],2))/2;
	avg_eff_array[5] = (pA_eff_array[5] + Ap_eff_array[5])/2;
	avg_eff_err_array[5] = sqrt(pow(pA_eff_err_array[5],2) + pow(Ap_eff_err_array[5],2))/2;

	/////////////////////////
	// Plot the Efficiency //
	/////////////////////////

	double yarr[6] = {2.25,2.75,3.25,3.75,4.25,4.75};	
	double yarr_err[6] = {0,0,0,0,0,0};
	auto eff_cvs = new TCanvas("eff_cvs","Efficiency");
	eff_cvs->SetLeftMargin(0.165);
	auto ygr_eff = new TGraphErrors(6,yarr,avg_eff_array,yarr_err,avg_eff_err_array);
	ygr_eff->SetName("ygr_eff");
	ygr_eff->SetTitle("Efficiency");
	ygr_eff->GetXaxis()->SetTitle("y_{#pi#pi}");
	ygr_eff->GetYaxis()->SetTitle("#varepsilon_{D_{T}}");
	ygr_eff->GetYaxis()->SetTitleOffset(1.125);
	ygr_eff->Draw("AP");

	//////////////////////////////////////////////////
	// Plot Histograms for Accepted/Rejected Events //
	//////////////////////////////////////////////////

	cout << "Making and plotting histograms for the accepted/rejected events..." << endl;
	auto pA_hm_dd = new TH1D("pA_hm_dd","p-Pb Data",40,400,600);
	pA_nt->Project("pA_hm_dd","mass",presel&&pA_dd);
	h_list->Add(pA_hm_dd);
	auto pA_hm_add = new TH1D("pA_hm_add","p-Pb Data",40,400,600);
	pA_nt->Project("pA_hm_add","mass",presel&&pA_antidd);
	h_list->Add(pA_hm_add);
	auto Ap_hm_dd = new TH1D("Ap_hm_dd","p-Pb Data",40,400,600);
	Ap_nt->Project("Ap_hm_dd","mass",presel&&Ap_dd);
	h_list->Add(Ap_hm_dd);
	auto Ap_hm_add = new TH1D("Ap_hm_add","p-Pb Data",40,400,600);
	Ap_nt->Project("Ap_hm_add","mass",presel&&Ap_antidd);
	h_list->Add(Ap_hm_add);
	auto dd_cvs = new TCanvas("dd_cvs","Decay-Distance Accepted/Rejected");
	dd_cvs->Divide(2,2);
	dd_cvs->cd(1);
	pA_hm_dd->SetMarkerColor(kBlue);
	pA_hm_dd->SetLineColor(kBlue);
	pA_hm_dd->GetXaxis()->SetTitle("m_{#pi#pi} [MeV]");
	setLabelY(pA_hm_dd,"MeV");
	pA_hm_dd->Draw("E1");	
	dd_cvs->cd(2);
	pA_hm_add->SetMarkerColor(kBlue);
	pA_hm_add->SetLineColor(kBlue);
	pA_hm_add->GetXaxis()->SetTitle("m_{#pi#pi} [MeV]");
	setLabelY(pA_hm_add,"MeV");
	pA_hm_add->Draw("E1");	
	dd_cvs->cd(3);
	Ap_hm_dd->SetMarkerColor(kRed);
	Ap_hm_dd->SetLineColor(kRed);
	Ap_hm_dd->GetXaxis()->SetTitle("m_{#pi#pi} [MeV]");
	setLabelY(Ap_hm_dd,"MeV");
	Ap_hm_dd->Draw("E1");	
	dd_cvs->cd(4);
	Ap_hm_add->SetMarkerColor(kRed);
	Ap_hm_add->SetLineColor(kRed);
	Ap_hm_add->GetXaxis()->SetTitle("m_{#pi#pi} [MeV]");
	setLabelY(Ap_hm_add,"MeV");
	Ap_hm_add->Draw("E1");

	////////////////////////////////////////////////////////
	// Plot Profile of Transverse Decay Distance vs. Mass //
	////////////////////////////////////////////////////////

	cout << "Plotting profile of transverse decay distance vs. mass..." << endl;
	auto pDm_cvs = new TCanvas("pDm_cvs","Profile of Transverse Decay Distance vs Mass");
	pDm_cvs->Divide(2,1);
	pDm_cvs->cd(1);
	auto pA_hDm = new TH2D("pA_hDm","E",100,0,2000,10,0,1);
	pA_nt->Project("pA_hDm","sqrt(pow(xv-0.863,2)+pow(yv-(-0.158),2)):mass",presel);
	auto pA_pDm = (TProfile*)pA_hDm->ProfileX();
	pA_pDm->SetMarkerColor(kBlue);
	pA_pDm->SetLineColor(kBlue);
	pA_pDm->GetXaxis()->SetTitle("m_{#pi#pi} [MeV]");
	pA_pDm->GetYaxis()->SetTitle("Average D_{T} [mm]");
	pA_pDm->Draw();
	pDm_cvs->cd(2);
	auto Ap_hDm = new TH2D("Ap_hDm","E",100,0,2000,10,0,1);
	Ap_nt->Project("Ap_hDm","sqrt(pow(xv-0.881,2)+pow(yv-(-0.138),2)):mass",presel);
	auto Ap_pDm = (TProfile*)Ap_hDm->ProfileX();
	Ap_pDm->SetMarkerColor(kRed);
	Ap_pDm->SetLineColor(kRed);
	Ap_pDm->GetXaxis()->SetTitle("m_{#pi#pi} [MeV]");
	Ap_pDm->GetYaxis()->SetTitle("Average D_{T} [mm]");
	Ap_pDm->Draw();

	// save plots for presentation
	auto cvs_save_dd = new TCanvas("cvs_save_dd","Accepted: For saving");
	cvs_save_dd->SetLeftMargin(0.175);
	pA_hm_dd->GetYaxis()->SetTitleOffset(1.25);
	pA_hm_dd->Draw("E1");
	auto cvs_save_add = new TCanvas("cvs_save_add","Rejected: For saving");
	pA_hm_add->Draw("E1");

	/////////
	// End //
	/////////

	// create an output file
	auto output_name = "output.hists";
	auto output_file = new TFile(output_name,"RECREATE");
	cout << "An output file " << output_name  <<  " has been created..." << endl;
	cout << "Writing output to file..." << endl;
	h_list->Write();
	ygr_eff->Write();
	cout << "We're done!" << endl;
}

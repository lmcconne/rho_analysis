//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//to run
//g++ hist_maker.cpp $(root-config --cflags --libs) -o hist_maker
// ./hist_maker
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// This code performs a set of cuts and selections to plot a histogram of the transverse momentum of two tracks. 
// It plots the pre and postcut histograms and writes them to a ROOT file called pidk_histograms.root

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//include statements

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <math.h>
#include <cmath>  
#include <TH1F.h>
#include <TF1.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <cmath>
#include <utility>
#include <chrono>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using namespace std;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Define the cuts that will be used in the analysis.

float  cutPIDk = 0;

string MassSel = "((600 < tru_mass) && (tru_mass < 850))";

string MassRecoSel = "((600 < reco_mass) && (reco_mass < 850))";

//string pt2 = "(pt2<.01)";

string PhiPhot = "(abs(cos_az) > 0.9995)";

string preSelection = MassSel; // + " && " + PhiPhot;

string prerecoSelection = MassRecoSel + " && " + PhiPhot;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Get the histograms for before and after the cuts (track 1)

pair<TH1F*, TH1F*> getHistograms(string name, TTree *tree, string selection, float cutValue) {

//  string yRange = "(" + to_string(yRangeLower) + " < true_y" + ") && (true_y < " + to_string(yRangeUpper) + ")";

  string preCut_histName = name + "_preCut";
  TH1F *preCutHist = new TH1F(preCut_histName.c_str(), "", 6,2,5);
  tree->Project(preCut_histName.c_str(), "true_y", (preSelection ).c_str());

  string no_photons = selection + " && (ngamma == " + to_string(cutValue) + ")";

  string postCut_histName = name + "_postCut";
  TH1F* postCutHist = new TH1F(postCut_histName.c_str(), "", 6,2,5);
  tree->Project(postCut_histName.c_str(),"true_y",(no_photons).c_str());

  pair<TH1F*, TH1F*> histograms = make_pair(preCutHist, postCutHist);
  return histograms;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Get the histograms for before and after the cuts (track 1)

pair<TH1F*, TH1F*> getrecoHistograms(string name, TTree *tree, string selection, float cutValue) {

//  string yRange = "(" + to_string(yRangeLower) + " < true_y" + ") && (true_y < " + to_string(yRangeUpper) + ")";

  string preCut_histName = name + "_preCut";
  TH1F *preCutHist = new TH1F(preCut_histName.c_str(), "", 6,2,5);
  tree->Project(preCut_histName.c_str(), "reco_y", (prerecoSelection ).c_str());

  string no_photons = selection + " && (ngamma == " + to_string(cutValue) + ")";

  string postCut_histName = name + "_postCut";
  TH1F* postCutHist = new TH1F(postCut_histName.c_str(), "", 6,2,5);
  tree->Project(postCut_histName.c_str(),"reco_y",(no_photons).c_str());

  pair<TH1F*, TH1F*> histograms = make_pair(preCutHist, postCutHist);
  return histograms;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// get root files, then save the postCut hist and  preCut hist 

int main() {

// Start timekeeping
auto start = std::chrono::high_resolution_clock::now();

   TFile *file;
   TTree *tree;

   TH1F *postCutHist; //[6];
   TH1F *preCutHist; //[6];

   TH1F *postrecoCutHist; //[6];
   TH1F *prerecoCutHist;

   // Define
   const int NUM_FILES = 1;
  // const int NUM_Y_RANGES = 6;
   double ymin, ymax;

   // Open the output ROOT file
   TFile* output_file = new TFile("./photon_histograms.root", "RECREATE");

   // y Stepping
  // for (int yRange = 0; yRange < NUM_Y_RANGES; yRange++) {
  //     ymin = 2.0 + yRange * 0.5;
  //     ymax = 2.5 + yRange * 0.5;
  //     if (ymax > 5.0) {
  //         continue;
  //     }

       // Create histograms for summing
    //   preCutHist[yRange] = new TH1F(Form("preCutHist%d", yRange), Form("preCutHist%d", yRange), 30,600,900);
    //   postCutHist[yRange] = new TH1F(Form("postCutHist%d", yRange), Form("postCutHist%d", yRange), 30,600,900);
   preCutHist = new TH1F(Form("preCutHist"), Form("preCutHist"), 6,2,5);
   postCutHist = new TH1F(Form("postCutHist"), Form("postCutHist"), 6,2,5);

   prerecoCutHist = new TH1F(Form("prerecoCutHist"), Form("prerecoCutHist"), 6,2,5);
   postrecoCutHist = new TH1F(Form("postrecoCutHist"), Form("postrecoCutHist"), 6,2,5);
//   }

   // Loop over files
   for (int j = 1; j <= NUM_FILES; j++) {
       file = new TFile(Form("/eos/lhcb/wg/CEP/amanda/analysis_histograms/photons/mc/photonmc.root", j), "READ");
//       file = new TFile(Form("./photeff%d.root", j), "READ");
       tree = (TTree*)file->Get("nt");


           std::pair<TH1F*, TH1F*> hist_pair = getHistograms(Form("Pb_PerEvent_"), tree , preSelection, cutPIDk);
           TH1F *hist_byY = hist_pair.first;
           TH1F *hist2_byY = hist_pair.second;
	   
           preCutHist->Add(hist_byY);
           postCutHist->Add(hist2_byY);

           delete hist_byY;
           delete hist2_byY;



           std::pair<TH1F*, TH1F*> recohist_pair = getrecoHistograms(Form("Pbreco_PerEvent_"), tree , prerecoSelection, cutPIDk);
           TH1F *recohist_byY = recohist_pair.first;
           TH1F *recohist2_byY = recohist_pair.second;

           prerecoCutHist->Add(recohist_byY);
           postrecoCutHist->Add(recohist2_byY);

           delete recohist_byY;
           delete recohist2_byY;

           // Only Write to file at end 
           if( j != NUM_FILES){
              continue;
           }
              
           // Save the histograms to the output file
           output_file->cd();
           preCutHist->Write(Form("preCutHist_Y"));
           postCutHist->Write(Form("postCutHist_Y"));


           prerecoCutHist->Write(Form("prerecoCutHist_Y"));
           postrecoCutHist->Write(Form("postrecoCutHist_Y"));  
      // }
      
       // Progress Report
       std::cout << "File " << j << " has been looped over." << std::endl;   

   }
   
   output_file->Close();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// HouseKeeping
   // Stop timekeeping
   auto end = std::chrono::high_resolution_clock::now();

   // Calculate elapsed time in seconds
   auto duration_sec = std::chrono::duration_cast<std::chrono::seconds>(end - start);

   // Extract minutes and seconds from duration
   int minutes = duration_sec.count() / 60;
   int seconds = duration_sec.count() % 60;

   // Print elapsed time in minutes and seconds
   std::cout << "Elapsed time: " << minutes << " minutes " << seconds << " seconds" << std::endl;
   
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Scratch Code 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





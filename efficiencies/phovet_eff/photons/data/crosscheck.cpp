//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//to run
//g++ hist_maker.cpp $(root-config --cflags --libs) -o hist_maker
// ./hist_maker
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// This code performs a set of cuts and selections to plot a histogram of the transverse momentum of two tracks. 
// It plots the pre and postcut histograms and writes them to a ROOT file called pidk_histograms.root

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//include statements

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <math.h>
#include <cmath>  
#include <TH1F.h>
#include <TF1.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <cmath>
#include <utility>
#include <chrono>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using namespace std;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Define the cuts that will be used in the analysis.

float  cutPIDk = 0;

string MassSel = "((700 < mass) && (mass < 850))";
string yCut = "(2. < y) && (y < 5.)";
string trck_etaCut = "(2 < trck1_eta) && (trck1_eta < 5) && (2 < trck2_eta) && (trck2_eta < 5)";
string herschel = "(her<6)";
string pt_cut = "(trck1_pt > 100) && (trck2_pt > 100)";
string trckType = "(trck1_type == 3) && (trck2_type == 3)";
string pt2 = "(pt2<.01)";
string nouttrck = "nouttrck == 2";
string osCut = "((trck1_charge * trck2_charge) == -1)";
string kv = "(trck2_richk <= 0) && (trck1_richk <= 0)";
string decay_distance = "1.8 > sqrt(pow((xv - 0.85), 2) + pow((yv - 0.1299), 2))";
string E_over_P = "((trck1_eop < 0.5) && (trck2_eop < 0.5))";
string PhiPhot = "(abs(PhiPhot) > 0.9995)";
string angle = "(abs(angle)< 3.13)";
string pidk = "((trck1_richk <= 0 ) && (trck2_richk <= 0))";
string trackingCut = yCut + " && " + pt_cut + " && " + trck_etaCut;
string preSelection = trackingCut + " && " + osCut + " && " + decay_distance + " && " + pt2 + " && " + herschel + " && " + E_over_P + " && " + PhiPhot + " && " + MassSel + " && " + pidk + " && " + angle  + " && " + nouttrck;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Get the histograms for before and after the cuts (track 1)

pair<TH1F*, TH1F*> getHistograms(string name, TTree *tree, string selection, float cutValue) {

  string preCut_histName = name + "_preCut";
  TH1F *preCutHist = new TH1F(preCut_histName.c_str(), "", 20,-2,2);
  tree->Project(preCut_histName.c_str(), "PhiPhot", (preSelection).c_str());

  string no_photons = selection + " && (ngamma == " + to_string(cutValue) + ")";

  string postCut_histName = name + "_postCut";
  TH1F* postCutHist = new TH1F(postCut_histName.c_str(), "", 20,-2,2);
  tree->Project(postCut_histName.c_str(),"PhiPhot",(no_photons ).c_str());

  pair<TH1F*, TH1F*> histograms = make_pair(preCutHist, postCutHist);
  return histograms;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// get root files, then save the postCut hist and  preCut hist 

int main() {

// Start timekeeping
auto start = std::chrono::high_resolution_clock::now();

   TFile *file;
   TTree *tree;

   TH1F *postCutHist;
   TH1F *preCutHist; 

   // Define
   const int NUM_FILES = 1;
 
   // Open the output ROOT file
   TFile* output_file = new TFile("./az_histograms.root", "RECREATE");

   

       // Create histograms for summing
       preCutHist = new TH1F(Form("preCutHist"), Form("preCutHist"), 20,-2,2);
       postCutHist = new TH1F(Form("postCutHist"), Form("postCutHist"), 20,-2,2);


   // Loop over files
   for (int j = 1; j <= NUM_FILES; j++) {
       file = new TFile(Form("/eos/lhcb/wg/CEP/amanda/trees/dipions/photeff%d.root", j), "READ");
       tree = (TTree*)file->Get("nt");

       // Get histograms 
       std::pair<TH1F*, TH1F*> hist_pair = getHistograms(Form("Pb_PerEvent_%d"), tree , preSelection, cutPIDk);
       TH1F *hist_byY = hist_pair.first;
       TH1F *hist2_byY = hist_pair.second;

       // Add histograms to the existing ones
       preCutHist->Add(hist_byY);
       postCutHist->Add(hist2_byY);
	   
       delete hist_byY;
       delete hist2_byY;

       // Only Write to file at end 
       if( j != NUM_FILES){
          continue;
       }
              
       // Save the histograms to the output file
       output_file->cd();
       preCutHist->Write(Form("preCutHist_Y"));
       postCutHist->Write(Form("postCutHist_Y"));
  
   
      
       // Progress Report
       std::cout << "File " << j << " has been looped over." << std::endl;   

   }
   
   output_file->Close();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// HouseKeeping
   // Stop timekeeping
   auto end = std::chrono::high_resolution_clock::now();

   // Calculate elapsed time in seconds
   auto duration_sec = std::chrono::duration_cast<std::chrono::seconds>(end - start);

   // Extract minutes and seconds from duration
   int minutes = duration_sec.count() / 60;
   int seconds = duration_sec.count() % 60;

   // Print elapsed time in minutes and seconds
   std::cout << "Elapsed time: " << minutes << " minutes " << seconds << " seconds" << std::endl;
   
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Scratch Code 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





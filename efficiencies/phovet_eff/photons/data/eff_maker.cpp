//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//to run
//g++ eff_maker.cpp $(root-config --cflags --libs) -o eff_maker
// ./eff_maker
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// This code divides the pre and post cut histograms from a pidk cut. It then uses the function
// binomial_error to calculate erros. The rest of the histogram styleing is done via lhcbStyle.C 
// The histograms are then writen to a ROOT file called pidk_efficiencies.root

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Include Statements

#include <iostream>
#include <TFile.h>
#include <TH1F.h>
#include "lhcbStyle.C"

const int NUM_ETA_RANGES = 6;
double etamin, etamax;
 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double binomial_error(double Nafter, double Nbefore)
{
    double N = Nbefore;
    double N_d = Nafter;
    double N_m = N - N_d;
    double part_1 = 1.0 / (N * N);
    double part_2 = sqrt((N_m * N_m * N_d) + (N_d * N_d * N_m));
    double bi_part = sqrt(N / (N - 1));
    double err = part_1 * part_2 * bi_part;
    return err;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void eff_maker() {

    // Use LHCb Style
    //gROOT->ProcessLine(".L lhcbStyle.C");
    //lhcbStyle();

    int etaRange = 0;
   
    // Open the root file containing the histograms
    TFile* input_file = new TFile("photon_histograms.root", "READ");

    // Declare the pre- and post-cut histograms
    TH1F* preCutHist[NUM_ETA_RANGES];
    TH1F* postCutHist[NUM_ETA_RANGES];
    TH1F* preCuttrck2Hist[NUM_ETA_RANGES];
    TH1F* postCuttrck2Hist[NUM_ETA_RANGES];
    TH1F* preCuttrck1and2Hist[NUM_ETA_RANGES];
    TH1F* postCuttrck1and2Hist[NUM_ETA_RANGES];

    // Eta Stepping
    for (int etaRange = 0; etaRange < NUM_ETA_RANGES; etaRange++) {
        etamin = 2.0 + etaRange * 0.5;
        etamax = 2.5 + etaRange * 0.5;
        if (etamax > 5.0) {
            continue;
        }
    }
   for (int etaRange = 0; etaRange < NUM_ETA_RANGES; etaRange++) {
    // Retrieve the pre- and post-cut histograms
    // Get the pre-cut histogram
    preCutHist[etaRange] = dynamic_cast<TH1F*>(input_file->Get(Form("preCutHist_trck1_eta%d", etaRange)));

    // Get the post-cut histogram
    postCutHist[etaRange] = dynamic_cast<TH1F*>(input_file->Get(Form("postCutHist_trck1_eta%d", etaRange)));

    // Get the pre-cut histogram for track 2
    preCuttrck2Hist[etaRange] = dynamic_cast<TH1F*>(input_file->Get(Form("preCutHist_trck2_eta%d", etaRange)));

    // Get the post-cut histogram for track 2
    postCuttrck2Hist[etaRange] = dynamic_cast<TH1F*>(input_file->Get(Form("postCutHist_trck2_eta%d", etaRange)));

    // Get the pre-cut histogram for track 1 and 2
    preCuttrck1and2Hist[etaRange] = dynamic_cast<TH1F*>(input_file->Get(Form("preCutHist_track1and2_eta%d", etaRange)));

    // Get the post-cut histogram for track 1 and 2                            
    postCuttrck1and2Hist[etaRange] = dynamic_cast<TH1F*>(input_file->Get(Form("postCutHist_track1and2_eta%d", etaRange)));


}


    // Create efficiency histograms
    TH1F* kv_eff_by_track[NUM_ETA_RANGES];
    TH1F* kv_eff_by_track2[NUM_ETA_RANGES];
    TH1F* kv_eff_by_track1and2[NUM_ETA_RANGES];

    for (int etaRange = 0; etaRange < NUM_ETA_RANGES; etaRange++) {
        kv_eff_by_track[etaRange] = (TH1F*)postCutHist[etaRange]->Clone(Form("kv_eff_by_track_eta%d", etaRange));
        kv_eff_by_track[etaRange]->Divide(preCutHist[etaRange]);
        
       
        for(int i=1; i<=kv_eff_by_track[etaRange]->GetNbinsX(); i++) {
            double Nafter = postCutHist[etaRange]->GetBinContent(i);
            double Nbefore = preCutHist[etaRange]->GetBinContent(i);
            double err = binomial_error(Nafter, Nbefore);
            kv_eff_by_track[etaRange]->SetBinError(i, err);
        }

        kv_eff_by_track2[etaRange] = (TH1F*)postCuttrck2Hist[etaRange]->Clone(Form("kv_eff_by_track2_eta%d", etaRange));
        kv_eff_by_track2[etaRange]->Divide(preCuttrck2Hist[etaRange]);

        for(int i=1; i<=kv_eff_by_track2[etaRange]->GetNbinsX(); i++) {
            double Ntrck2after = postCuttrck2Hist[etaRange]->GetBinContent(i);
            double Ntrck2before = preCuttrck2Hist[etaRange]->GetBinContent(i);
            double err_trck2 = binomial_error(Ntrck2after, Ntrck2before);
            kv_eff_by_track2[etaRange]->SetBinError(i, err_trck2);
        }
                                                
        kv_eff_by_track1and2[etaRange] = (TH1F*)postCuttrck1and2Hist[etaRange]->Clone(Form("kv_eff_by_track1and2_eta%d", etaRange));
        kv_eff_by_track1and2[etaRange]->Divide(preCuttrck1and2Hist[etaRange]);

        for(int i=1; i<=kv_eff_by_track1and2[etaRange]->GetNbinsX(); i++) {
            double Ntrck1and2after = postCuttrck1and2Hist[etaRange]->GetBinContent(i);
            double Ntrck1and2before = preCuttrck1and2Hist[etaRange]->GetBinContent(i);
            double err_trck1and2 = binomial_error(Ntrck1and2after, Ntrck1and2before);
            kv_eff_by_track1and2[etaRange]->SetBinError(i, err_trck1and2);
        }


    }

    // Open the output ROOT file
    TFile* output_file = new TFile("photon_efficiencies.root", "RECREATE");

    // Save the efficiency histograms into the output ROOT file
    for (int etaRange = 0; etaRange < NUM_ETA_RANGES; etaRange++) {
        output_file->cd();
        kv_eff_by_track[etaRange]->Write();
        kv_eff_by_track2[etaRange]->Write();
        kv_eff_by_track1and2[etaRange]->Write();
    }

    // Cleanup
    input_file->Close();
    output_file->Close();

    return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Scratch Code


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

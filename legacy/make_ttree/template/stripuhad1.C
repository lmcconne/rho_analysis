// stripping code for hadrons
// require 2 4 or 6 charged tracks and <=6 photons.
// This books the new ntuple (essentially the same as the old one, but you can add a few more entries e.g. mass of the rho, mass of KK, mass of ee, pt etc), checks some conditions, and fills the new ntuple 
#define striphad_cxx
#include "striphad.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>



void striphad::Loop()
{
   if (fChain == 0) return;

   float pxg[10],pyg[10],pzg[10],eeg[10];
   int spdg[10],prsg[10];
   Long64_t nentries = fChain->GetEntriesFast();
   int ngamma,i,j,k,iflag,fbunch;
   float delx,dely;
   float chinew,chih;
   float newadc[20],rmsadc[20];

  //book ntuple
  TFile *newfile=new TFile("output1.root","recreate");
  TTree *TT=new TTree("TT","TT");
  TT->Branch("run",&run,"run/I");
  TT->Branch("evt",&evt,"evt/I");
  TT->Branch("bunchid",&bunchid,"bunchid/I");
  TT->Branch("bunchtype",&bunchtype,"bunchtype/I");
  TT->Branch("trighlt1",&trighlt1,"trighlt1/I");
  TT->Branch("triglmr",&triglmr,"triglmr/I");
  TT->Branch("trighad",&trighad,"trighad/I");
  TT->Branch("trighadws",&trighadws,"trighadws/I");
  TT->Branch("trigmu",&trigmu,"trigmu/I");
  TT->Branch("trigph",&trigph,"trigph/I");
  TT->Branch("trigel",&trigel,"trigel/I");
  TT->Branch("l0dim",&l0dim,"l0dim/I");
  TT->Branch("l0diem",&l0diem,"l0diem/I");
  TT->Branch("l0dih",&l0dih,"l0dih/I");
  TT->Branch("l0m",&l0m,"l0m/I");
  TT->Branch("l0e",&l0e,"l0e/I");
  TT->Branch("l0ph",&l0ph,"l0ph/I");
  TT->Branch("nlong",&nlong,"nlong/I");
  TT->Branch("nvelo",&nvelo,"nvelo/I");
  TT->Branch("ndown",&ndown,"ndown/I");
  TT->Branch("ntk",&ntk,"ntk/I");
  TT->Branch("nspd",&nspd,"nspd/I");
  TT->Branch("ntotphot",&ntotphot,"ntotphot/I");
  TT->Branch("nout",&nout,"nout/I");
  TT->Branch("ngamma",&ngamma,"ngamma/I");
  TT->Branch("etprev1",&etprev1,"etprev1/F");
  TT->Branch("etprev2",&etprev2,"etprev2/F");
  TT->Branch("tx",&tx,"tx[nout]/F");
  TT->Branch("ty",&ty,"ty[nout]/F");
  TT->Branch("tz",&tz,"tz[nout]/F");
  TT->Branch("px",&px,"px[nout]/F");
  TT->Branch("py",&py,"py[nout]/F");
  TT->Branch("pz",&pz,"pz[nout]/F");
  TT->Branch("charge",&charge,"charge[nout]/I");
  TT->Branch("muon",&muon,"muon[nout]/I");
  TT->Branch("muacc",&muacc,"muacc[nout]/I");
  TT->Branch("pide",&pide,"pide[nout]/F");
  TT->Branch("pidk",&pidk,"pidk[nout]/F");
  TT->Branch("pidp",&pidp,"pidp[nout]/F");
  TT->Branch("pidm",&pidm,"pidm[nout]/F");
  TT->Branch("ecal",&ecal,"ecal[nout]/F");
  TT->Branch("hcal",&hcal,"hcal[nout]/F");
  TT->Branch("eoverp",&eoverp,"eoverp[nout]/F");
  TT->Branch("pxg",&pxg,"pxg[ngamma]/F");
  TT->Branch("pyg",&pyg,"pyg[ngamma]/F");
  TT->Branch("pzg",&pzg,"pzg[ngamma]/F");
  TT->Branch("spdg",&spdg,"spdg[ngamma]/I");
  TT->Branch("prsg",&prsg,"prsg[ngamma]/I");
  TT->Branch("chih",&chih,"chih/F");
  TT->Branch("chinew",&chinew,"chinew/F");
  TT->Branch("fbunch",&fbunch,"fbunch/I");
  
  //this code to output hadronics
  //  nentries=100000;
   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
     Long64_t ientry = LoadTree(jentry);
     if (ientry < 0) break;
     nb = fChain->GetEntry(jentry);   nbytes += nb;
     // if (Cut(ientry) < 0) continue;
     if(jentry/100000==(jentry+99999)/100000)cout<<jentry<<endl;
     
     ngamma=0;
     for(i=0;i<nphotons;i++){
       iflag=1;
       for(j=0;j<nout;j++){
	 dely=phy[i]/phz[i]-py[j]/pz[j];
	 delx=phx[i]/phz[i]-px[j]/pz[j]+780*charge[j]/pz[j];//down-780, up +780
	 if(fabs(delx)<.025&&fabs(dely)<.025)iflag=0;
       }
       if(prs[i]>0&&iflag>0){
	 pxg[ngamma]=phx[i];
	 pyg[ngamma]=phy[i];
	 pzg[ngamma]=phz[i];
	 eeg[ngamma]=sqrt(pow(phx[i],2)+pow(phy[i],2)+pow(phz[i],2));
	 ngamma+=1;
       }
     }

     iflag=0;
     if(nout==2&&nvelo==2)iflag=1;
     if(nout==4&&nvelo==4)iflag=1;
     if(nout==6&&nvelo==6)iflag=1;
     if(nout==0&&nvelo==0)iflag=1;
     if(ntotphot>6)iflag=0;
     //     if(triglmr==0)iflag=0;
     if(iflag==1){
       TT->Fill();
     }
     }
   }
   TT->Write();
   newfile->Close();
}




//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Mar 23 15:35:27 2021 by ROOT version 6.22/06
// from TChain T/
//////////////////////////////////////////////////////////

#ifndef selpA_h
#define selpA_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class selpA {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           run;
   Int_t           evt;
   Int_t           bunchid;
   Int_t           bunchtype;
   Int_t           trigmb;
   Int_t           trighlt1;
   Int_t           triglmr;
   Int_t           trighad;
   Int_t           trighadws;
   Int_t           trigmu;
   Int_t           trigph;
   Int_t           trigel;
   Int_t           trigmbvelo;
   Int_t           l0dim;
   Int_t           l0diem;
   Int_t           l0dih;
   Int_t           l0m;
   Int_t           l0e;
   Int_t           l0ph;
   Int_t           stripphi;
   Int_t           nlong;
   Int_t           nvelo;
   Int_t           ndown;
   Int_t           ntk;
   Int_t           nspd;
   Int_t           ntotphot;
   Int_t           nout;
   Int_t           nphotons;
   Int_t           nrecspd;
   Int_t           nrecprs;
   Float_t         etprev1;
   Float_t         etprev2;
   Int_t           tt[10];   //[nout]
   Float_t         tx[10];   //[nout]
   Float_t         ty[10];   //[nout]
   Float_t         tz[10];   //[nout]
   Float_t         px[10];   //[nout]
   Float_t         py[10];   //[nout]
   Float_t         pz[10];   //[nout]
   Int_t           charge[10];   //[nout]
   Int_t           muon[10];   //[nout]
   Int_t           muacc[10];   //[nout]
   Float_t         pide[10];   //[nout]
   Float_t         pidk[10];   //[nout]
   Float_t         pidp[10];   //[nout]
   Float_t         pidm[10];   //[nout]
   Float_t         ecal[10];   //[nout]
   Float_t         hcal[10];   //[nout]
   Float_t         eoverp[10];   //[nout]
   Float_t         phx[6];   //[nphotons]
   Float_t         phy[6];   //[nphotons]
   Float_t         phz[6];   //[nphotons]
   Int_t           spd[6];   //[nphotons]
   Int_t           prs[6];   //[nphotons]
   Int_t           nl0;
   Float_t         l0et[2];   //[nl0]
   Float_t         l0phi[2];   //[nl0]
   Float_t         l0theta[2];   //[nl0]
   Int_t           nher;
   Int_t           herchan[30];   //[nher]
   Int_t           heradc[30];   //[nher]
   Float_t         spdx[1];   //[nrecspd]
   Float_t         spdy[1];   //[nrecspd]
   Float_t         prsx[1];   //[nrecprs]
   Float_t         prsy[1];   //[nrecprs]
   Int_t           nmuons;
   Float_t         muonchi[4];   //[nmuons]
   Float_t         muonux[4];   //[nmuons]
   Float_t         muonuy[4];   //[nmuons]
   Float_t         muonx[4];   //[nmuons]
   Float_t         muony[4];   //[nmuons]
   Int_t           muonshared[4];   //[nmuons]

   // List of branches
   TBranch        *b_run;   //!
   TBranch        *b_evt;   //!
   TBranch        *b_bunchid;   //!
   TBranch        *b_bunchtype;   //!
   TBranch        *b_trigmb;   //!
   TBranch        *b_trighlt1;   //!
   TBranch        *b_triglmr;   //!
   TBranch        *b_trighad;   //!
   TBranch        *b_trighadws;   //!
   TBranch        *b_trigmu;   //!
   TBranch        *b_trigph;   //!
   TBranch        *b_trigel;   //!
   TBranch        *b_trigmbvelo;   //!
   TBranch        *b_l0dim;   //!
   TBranch        *b_l0diem;   //!
   TBranch        *b_l0dih;   //!
   TBranch        *b_l0m;   //!
   TBranch        *b_l0e;   //!
   TBranch        *b_l0ph;   //!
   TBranch        *b_stripphi;   //!
   TBranch        *b_nlong;   //!
   TBranch        *b_nvelo;   //!
   TBranch        *b_ndown;   //!
   TBranch        *b_ntk;   //!
   TBranch        *b_nspd;   //!
   TBranch        *b_ntotphot;   //!
   TBranch        *b_nout;   //!
   TBranch        *b_nphotons;   //!
   TBranch        *b_nrecspd;   //!
   TBranch        *b_nrecprs;   //!
   TBranch        *b_etprev1;   //!
   TBranch        *b_etprev2;   //!
   TBranch        *b_tt;   //!
   TBranch        *b_tx;   //!
   TBranch        *b_ty;   //!
   TBranch        *b_tz;   //!
   TBranch        *b_px;   //!
   TBranch        *b_py;   //!
   TBranch        *b_pz;   //!
   TBranch        *b_charge;   //!
   TBranch        *b_muon;   //!
   TBranch        *b_muacc;   //!
   TBranch        *b_pide;   //!
   TBranch        *b_pidk;   //!
   TBranch        *b_pidp;   //!
   TBranch        *b_pidm;   //!
   TBranch        *b_ecal;   //!
   TBranch        *b_hcal;   //!
   TBranch        *b_eoverp;   //!
   TBranch        *b_phx;   //!
   TBranch        *b_phy;   //!
   TBranch        *b_phz;   //!
   TBranch        *b_spd;   //!
   TBranch        *b_prs;   //!
   TBranch        *b_nl0;   //!
   TBranch        *b_l0et;   //!
   TBranch        *b_l0phi;   //!
   TBranch        *b_l0theta;   //!
   TBranch        *b_nher;   //!
   TBranch        *b_herchan;   //!
   TBranch        *b_heradc;   //!
   TBranch        *b_spdx;   //!
   TBranch        *b_spdy;   //!
   TBranch        *b_prsx;   //!
   TBranch        *b_prsy;   //!
   TBranch        *b_nmuons;   //!
   TBranch        *b_muonchi;   //!
   TBranch        *b_muonux;   //!
   TBranch        *b_muonuy;   //!
   TBranch        *b_muonx;   //!
   TBranch        *b_muony;   //!
   TBranch        *b_muonshared;   //!

   selpA(TTree *tree=0);
   virtual ~selpA();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef selpA_cxx
selpA::selpA(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      f->GetObject("T",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
      TChain * chain = new TChain("T","");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_0.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_100.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_101.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_102.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_103.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_104.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_105.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_106.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_107.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_108.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_109.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_10.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_110.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_111.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_112.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_113.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_114.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_115.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_116.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_117.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_118.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_119.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_11.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_120.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_121.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_122.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_123.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_124.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_125.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_126.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_127.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_128.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_129.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_12.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_130.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_131.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_132.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_133.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_134.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_135.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_136.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_137.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_138.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_139.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_13.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_140.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_141.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_142.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_143.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_144.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_145.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_146.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_147.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_148.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_149.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_14.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_150.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_151.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_152.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_153.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_154.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_155.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_156.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_157.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_158.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_159.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_15.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_160.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_161.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_162.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_163.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_164.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_165.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_166.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_167.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_168.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_16.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_170.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_171.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_172.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_173.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_174.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_175.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_176.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_177.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_178.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_179.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_17.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_180.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_181.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_182.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_183.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_184.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_185.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_186.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_187.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_188.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_189.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_18.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_190.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_191.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_192.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_193.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_194.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_195.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_196.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_197.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_198.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_199.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_19.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_1.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_200.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_201.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_202.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_203.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_204.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_205.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_206.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_207.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_208.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_209.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_20.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_210.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_211.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_212.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_213.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_214.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_215.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_216.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_217.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_218.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_219.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_21.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_220.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_221.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_222.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_223.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_224.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_225.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_226.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_227.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_228.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_229.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_22.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_230.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_231.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_232.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_233.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_234.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_235.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_236.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_237.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_238.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_239.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_23.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_240.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_241.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_242.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_243.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_244.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_245.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_246.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_247.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_248.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_249.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_24.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_250.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_251.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_252.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_253.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_254.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_255.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_256.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_257.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_258.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_259.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_25.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_260.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_261.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_262.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_263.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_264.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_265.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_266.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_267.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_268.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_269.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_26.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_270.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_271.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_272.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_273.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_274.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_275.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_276.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_277.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_278.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_279.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_27.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_280.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_281.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_282.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_283.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_284.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_285.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_286.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_287.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_288.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_289.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_28.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_290.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_291.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_292.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_293.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_294.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_295.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_296.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_297.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_298.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_299.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_29.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_2.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_300.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_301.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_302.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_303.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_304.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_305.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_306.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_307.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_308.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_309.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_30.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_310.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_311.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_312.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_313.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_314.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_315.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_316.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_317.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_318.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_319.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_31.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_320.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_321.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_322.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_323.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_324.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_325.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_326.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_327.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_328.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_329.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_32.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_330.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_331.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_332.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_333.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_334.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_335.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_336.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_337.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_338.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_339.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_33.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_340.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_341.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_342.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_343.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_344.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_345.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_346.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_347.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_348.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_349.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_34.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_350.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_351.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_352.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_353.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_354.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_355.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_356.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_357.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_358.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_359.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_35.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_360.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_361.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_362.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_363.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_364.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_365.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_366.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_367.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_368.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_369.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_36.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_370.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_371.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_372.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_373.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_374.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_375.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_376.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_377.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_378.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_379.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_37.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_380.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_381.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_382.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_383.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_384.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_385.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_386.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_387.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_388.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_389.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_38.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_390.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_391.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_392.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_393.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_394.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_395.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_396.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_397.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_398.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_399.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_39.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_3.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_400.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_401.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_402.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_403.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_404.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_405.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_406.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_407.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_408.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_409.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_40.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_410.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_411.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_412.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_413.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_414.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_415.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_416.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_417.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_418.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_419.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_41.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_420.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_421.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_422.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_423.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_424.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_425.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_426.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_427.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_428.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_429.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_42.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_430.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_431.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_432.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_433.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_434.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_435.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_436.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_437.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_438.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_439.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_43.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_440.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_441.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_442.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_443.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_444.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_445.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_446.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_447.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_448.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_449.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_44.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_450.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_451.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_452.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_453.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_454.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_455.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_456.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_457.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_458.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_459.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_45.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_460.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_461.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_462.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_463.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_464.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_465.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_466.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_467.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_468.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_469.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_46.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_470.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_471.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_472.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_473.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_474.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_475.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_476.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_477.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_478.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_479.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_47.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_480.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_481.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_482.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_483.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_484.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_485.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_486.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_487.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_488.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_489.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_48.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_490.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_491.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_492.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_493.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_494.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_495.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_496.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_497.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_498.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_499.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_49.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_4.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_500.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_501.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_502.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_503.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_504.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_505.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_506.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_507.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_508.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_509.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_50.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_510.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_511.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_512.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_513.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_514.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_515.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_516.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_517.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_518.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_519.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_51.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_520.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_521.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_522.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_523.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_524.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_525.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_526.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_527.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_528.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_529.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_52.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_530.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_531.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_532.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_533.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_534.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_535.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_536.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_537.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_538.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_539.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_53.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_540.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_541.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_542.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_543.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_544.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_545.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_546.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_547.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_548.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_549.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_54.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_550.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_551.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_552.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_553.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_554.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_555.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_556.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_557.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_558.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_559.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_55.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_560.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_561.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_562.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_563.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_564.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_565.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_566.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_567.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_568.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_569.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_56.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_570.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_571.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_572.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_573.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_574.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_575.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_576.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_577.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_578.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_579.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_57.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_580.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_581.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_582.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_583.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_584.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_585.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_586.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_587.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_588.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_589.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_58.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_590.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_591.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_592.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_593.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_594.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_595.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_596.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_597.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_598.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_599.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_59.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_5.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_600.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_601.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_602.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_603.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_604.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_605.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_606.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_607.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_608.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_609.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_60.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_610.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_611.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_612.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_613.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_614.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_615.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_616.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_617.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_618.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_619.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_61.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_620.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_621.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_622.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_623.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_624.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_625.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_626.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_627.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_628.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_629.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_62.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_630.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_631.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_632.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_633.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_634.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_635.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_636.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_637.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_638.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_639.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_63.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_640.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_641.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_642.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_643.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_644.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_645.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_648.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_649.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_64.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_650.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_651.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_652.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_653.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_654.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_655.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_656.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_657.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_658.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_659.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_65.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_660.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_661.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_66.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_67.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_68.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_69.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_6.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_70.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_71.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_72.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_73.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_74.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_75.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_76.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_77.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_78.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_79.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_7.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_80.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_81.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_82.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_83.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_84.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_85.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_86.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_87.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_88.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_89.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_8.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_90.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_91.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_92.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_93.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_94.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_95.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_96.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_97.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_98.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_99.root/T");
      chain->Add("/eos/lhcb/wg/CEP/ronan/2016_pA_mb/d/lomult_9.root/T");
      tree = chain;
#endif // SINGLE_TREE

   }
   Init(tree);
}

selpA::~selpA()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t selpA::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t selpA::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void selpA::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("run", &run, &b_run);
   fChain->SetBranchAddress("evt", &evt, &b_evt);
   fChain->SetBranchAddress("bunchid", &bunchid, &b_bunchid);
   fChain->SetBranchAddress("bunchtype", &bunchtype, &b_bunchtype);
   fChain->SetBranchAddress("trigmb", &trigmb, &b_trigmb);
   fChain->SetBranchAddress("trighlt1", &trighlt1, &b_trighlt1);
   fChain->SetBranchAddress("triglmr", &triglmr, &b_triglmr);
   fChain->SetBranchAddress("trighad", &trighad, &b_trighad);
   fChain->SetBranchAddress("trighadws", &trighadws, &b_trighadws);
   fChain->SetBranchAddress("trigmu", &trigmu, &b_trigmu);
   fChain->SetBranchAddress("trigph", &trigph, &b_trigph);
   fChain->SetBranchAddress("trigel", &trigel, &b_trigel);
   fChain->SetBranchAddress("trigmbvelo", &trigmbvelo, &b_trigmbvelo);
   fChain->SetBranchAddress("l0dim", &l0dim, &b_l0dim);
   fChain->SetBranchAddress("l0diem", &l0diem, &b_l0diem);
   fChain->SetBranchAddress("l0dih", &l0dih, &b_l0dih);
   fChain->SetBranchAddress("l0m", &l0m, &b_l0m);
   fChain->SetBranchAddress("l0e", &l0e, &b_l0e);
   fChain->SetBranchAddress("l0ph", &l0ph, &b_l0ph);
   fChain->SetBranchAddress("stripphi", &stripphi, &b_stripphi);
   fChain->SetBranchAddress("nlong", &nlong, &b_nlong);
   fChain->SetBranchAddress("nvelo", &nvelo, &b_nvelo);
   fChain->SetBranchAddress("ndown", &ndown, &b_ndown);
   fChain->SetBranchAddress("ntk", &ntk, &b_ntk);
   fChain->SetBranchAddress("nspd", &nspd, &b_nspd);
   fChain->SetBranchAddress("ntotphot", &ntotphot, &b_ntotphot);
   fChain->SetBranchAddress("nout", &nout, &b_nout);
   fChain->SetBranchAddress("nphotons", &nphotons, &b_nphotons);
   fChain->SetBranchAddress("nrecspd", &nrecspd, &b_nrecspd);
   fChain->SetBranchAddress("nrecprs", &nrecprs, &b_nrecprs);
   fChain->SetBranchAddress("etprev1", &etprev1, &b_etprev1);
   fChain->SetBranchAddress("etprev2", &etprev2, &b_etprev2);
   fChain->SetBranchAddress("tt", tt, &b_tt);
   fChain->SetBranchAddress("tx", tx, &b_tx);
   fChain->SetBranchAddress("ty", ty, &b_ty);
   fChain->SetBranchAddress("tz", tz, &b_tz);
   fChain->SetBranchAddress("px", px, &b_px);
   fChain->SetBranchAddress("py", py, &b_py);
   fChain->SetBranchAddress("pz", pz, &b_pz);
   fChain->SetBranchAddress("charge", charge, &b_charge);
   fChain->SetBranchAddress("muon", muon, &b_muon);
   fChain->SetBranchAddress("muacc", muacc, &b_muacc);
   fChain->SetBranchAddress("pide", pide, &b_pide);
   fChain->SetBranchAddress("pidk", pidk, &b_pidk);
   fChain->SetBranchAddress("pidp", pidp, &b_pidp);
   fChain->SetBranchAddress("pidm", pidm, &b_pidm);
   fChain->SetBranchAddress("ecal", ecal, &b_ecal);
   fChain->SetBranchAddress("hcal", hcal, &b_hcal);
   fChain->SetBranchAddress("eoverp", eoverp, &b_eoverp);
   fChain->SetBranchAddress("phx", phx, &b_phx);
   fChain->SetBranchAddress("phy", phy, &b_phy);
   fChain->SetBranchAddress("phz", phz, &b_phz);
   fChain->SetBranchAddress("spd", spd, &b_spd);
   fChain->SetBranchAddress("prs", prs, &b_prs);
   fChain->SetBranchAddress("nl0", &nl0, &b_nl0);
   fChain->SetBranchAddress("l0et", l0et, &b_l0et);
   fChain->SetBranchAddress("l0phi", l0phi, &b_l0phi);
   fChain->SetBranchAddress("l0theta", l0theta, &b_l0theta);
   fChain->SetBranchAddress("nher", &nher, &b_nher);
   fChain->SetBranchAddress("herchan", herchan, &b_herchan);
   fChain->SetBranchAddress("heradc", heradc, &b_heradc);
   fChain->SetBranchAddress("spdx", &spdx, &b_spdx);
   fChain->SetBranchAddress("spdy", &spdy, &b_spdy);
   fChain->SetBranchAddress("prsx", &prsx, &b_prsx);
   fChain->SetBranchAddress("prsy", &prsy, &b_prsy);
   fChain->SetBranchAddress("nmuons", &nmuons, &b_nmuons);
   fChain->SetBranchAddress("muonchi", muonchi, &b_muonchi);
   fChain->SetBranchAddress("muonux", muonux, &b_muonux);
   fChain->SetBranchAddress("muonuy", muonuy, &b_muonuy);
   fChain->SetBranchAddress("muonx", muonx, &b_muonx);
   fChain->SetBranchAddress("muony", muony, &b_muony);
   fChain->SetBranchAddress("muonshared", muonshared, &b_muonshared);
   Notify();
}

Bool_t selpA::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void selpA::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t selpA::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef selpA_cxx

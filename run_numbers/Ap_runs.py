### import modules
import ROOT
print("Modules imported")

#### start timer
import time
start_time = time.time()
print("Timer started")

### create chains
Ap_chain = ROOT.TChain("MyTuple")
### get from charlotte's ntuples
Ap_chain.Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple*.root/TupleTools/MyTuple")
print ("The Ap chains are ready.")

#### Set event limit if desitred
event_limit = 1000000
print("Event limit set to",event_limit,".")

#### create list to save runs list
Ap_runsList = []
print("Ap list has been created.")

#### loop over events in the Ap chain
print ("Looping over the Ap events.")

### set a counting variable
count = 0

for event in Ap_chain:

        ### for quick runs
        if(count == event_limit):
                break

        ### print event count
        if((count % 100000) == 0):
                print (count)

        count += 1

        ### add run number to list
        Ap_runsList.append(event.runNumber)

print ("Finished looping over the Ap events.")

#### find the highest and lowest run number
### sort the runs list
sorted_Ap_runsList = sorted(Ap_runsList)
print("The Ap list has been sorted.")

### print out the list sorted list (not recommender for large numbers of events)
#print(sorted_Ap_runsList)

### print the results
print("The first run entry is:",sorted_Ap_runsList[0],".")
print("The last run entry is:",sorted_Ap_runsList[len(sorted_Ap_runsList)-1],".")

### end of script
print("Script finished,")

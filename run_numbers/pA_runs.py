### import modules
import ROOT
print("Modules imported")

#### start timer
import time
start_time = time.time()
print("Timer started")

### create chains
pA_chain = ROOT.TChain("MyTuple")
### get from charlotte's ntuples
pA_chain.Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple*.root/TupleTools/MyTuple")
print ("The pA chains are ready.")

#### Set event limit if desitred
event_limit = 1000000
print("Event limit set to",event_limit,".")

#### create list to save runs list
pA_runsList = []
print("pA list has been created.")

#### loop over events in the pA chain
print ("Looping over the pA events.")

### set a counting variable
count = 0

for event in pA_chain:
    
    	### for quick runs
	if(count == event_limit):
		break
        
    	### print event count
	if((count % 100000) == 0):
		print (count)

	count += 1
    
    	### add run number to list
	pA_runsList.append(event.runNumber)
 
print ("Finished looping over the pA events.")

#### find the highest and lowest run number
### sort the runs list
sorted_pA_runsList = sorted(pA_runsList)
print("The pA list has been sorted.")

### print out the list sorted list (not recommender for large numbers of events)
#print(sorted_pA_runsList)

### print the results
print("The first run entry is:",sorted_pA_runsList[0],".")
print("The last run entry is:",sorted_pA_runsList[len(sorted_pA_runsList)-1],".")

### end of script
print("Script finished,")

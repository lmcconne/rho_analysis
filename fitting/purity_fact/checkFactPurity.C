#include "../../useful_variables.C"
#include "../../useful_variables.h"
#include "../mass_fit_functions.C"
#include "../mass_fit_functions.h"

double a[10], a_err[10], a_chi2, a_chi2_prob, b[10], b_err[10], b_chi2, b_chi2_prob;

void printTab()
{
	printf("\nParameter Difference Significance\n");
	printf("\\begin{tabular}{|c|c|c|} \\hline\n");
	printf("\t$b_{\\text{dis}}$ & $b_{\\text{el}}$ & \\\\ \\hline\n");
	printf("\t$\\chi^{2}$ &\\num{%f} & \\num{%f} \\\\ \\hline\n",a_chi2,b_chi2);
	printf("\t$P\\left(\\chi^{2}\\right)$ &\\num{%f} & \\num{%f} \\\\ \\hline\n",a_chi2_prob,b_chi2_prob);
	printf("\\end{tabular}\n");
}

double fit2D(double *x, double *par)
{

	// 0 - Nx
	// 1 - rho_width
	// 2 - rho_mass
	// 3 - Ny
	// 4 - a
	// 5 - b

	double q = (pow(x[0],2) - 4*pow(pion_mass,2))/(pow(par[2],2) - 4*pow(pion_mass,2));
	if((TMath::IsNaN(q)) || (q < 0)) q = 0;
	double  mdw = par[1]*(par[2]/x[0])*pow(q,1.5);
	double upper_term = sqrt(x[0] * par[2] * mdw);
	TComplex lower_term = (x[0]*x[0] - par[2]*par[2] + TComplex(0,par[2]*mdw));
	double bw_term = (par[0]*upper_term/lower_term);
	double mass_term = pow(TComplex::Abs(bw_term),2);

	double pt2_term = par[3]*(par[4]*exp(-par[4]*x[1]) + par[5]*exp(-par[5]*x[1]));

	return mass_term + pt2_term;
}

void checkFactPurity()
{
	///////////////////
	// Preliminaries //
	///////////////////

	// start timer
	auto stopwatch = new TStopwatch();
	stopwatch->Start();

	// set LHCb style
	gROOT->ProcessLine(".L ../../lhcbStyle.C");

	// MC beam-coordinates
	TVector3 reco_beam(0.837,-0.187,0);

	// open signal mc file
	cout << "Opening mc input file..." << endl;
	auto input_file  = new TFile("../../rho_mc/rhomc.root");
	auto tree = (TTree*)(input_file->Get("T"));
	auto max_trcks = 50;;
	float tx[max_trcks], ty[max_trcks], tz[max_trcks], trupx[max_trcks], trupy[max_trcks], trupz[max_trcks], px[max_trcks], py[max_trcks], pz[max_trcks], phx[max_trcks], phy[max_trcks], phz[max_trcks], rhoy, rhom, rhopx, rhopy;
	int nout, tt[max_trcks], charge[max_trcks], nphotons, ntotphot, truq[max_trcks], prs[max_trcks];
	tree->SetBranchAddress("trupx",trupx);
	tree->SetBranchAddress("trupy",trupy);
	tree->SetBranchAddress("trupz",trupz);
	tree->SetBranchAddress("truq",truq);
	tree->SetBranchAddress("rhoy",&rhoy);
	tree->SetBranchAddress("rhom",&rhom);
	tree->SetBranchAddress("rhopx",&rhopx);
	tree->SetBranchAddress("rhopy",&rhopy);
	tree->SetBranchAddress("px",px);
	tree->SetBranchAddress("py",py);
	tree->SetBranchAddress("pz",pz);
	tree->SetBranchAddress("charge",charge);
	tree->SetBranchAddress("tt",tt);
	tree->SetBranchAddress("tx",tx);
	tree->SetBranchAddress("ty",ty);
	tree->SetBranchAddress("tz",tz);
	tree->SetBranchAddress("phx",phx);
	tree->SetBranchAddress("phy",phy);
	tree->SetBranchAddress("phz",phz);
	tree->SetBranchAddress("nout",&nout);
	tree->SetBranchAddress("nphotons",&nphotons);
	tree->SetBranchAddress("ntotphot",&ntotphot);
	tree->SetBranchAddress("prs",prs); // I'm using this variable to calc ngamma as it is indexed by the nphotons variable

	// declare and initialise histograms
	
	double xbins_test = 10;
	double xlow_test = 600;
	double xhigh_test = 1000;

	auto truth_hy = new TH1D("truth_hy","Event-Level Simulation",6,2,5);
	auto truth_hm = new TH1D("truth_hm","Event-Level Simulation",xbins_test,xlow_test,xhigh_test);
	auto truth_hp = new TH1D("truth_hp","Event-Level Simulation",ybins,ylow,yhigh);
	auto truth_hmp = new TH2D("truth_hmp","Event-Level Simulation",xbins_test,xlow_test,xhigh_test,ybins,ylow,yhigh);

	auto reco_hy = new TH1D("reco_hy","Reco-Level Sim.",6,2,5);
	auto reco_hm = new TH1D("reco_hm","Reco-Level Sim.",xbins_test,xlow_test,xhigh_test);
	auto reco_hp = new TH1D("reco_hp","Reco-Level Sim.",ybins,ylow,yhigh);
	auto reco_hmp = new TH2D("reco_hmp","Reco-Level Sim.",xbins_test,xlow_test,xhigh_test,ybins,ylow,yhigh);

	auto truth_hp_list = new TList();
	auto truth_hp_m0 = new TH1D("truth_hp_m0","Event-Level Sim.",ybins,ylow,yhigh); truth_hp_list->Add(truth_hp_m0);
	auto truth_hp_m1 = new TH1D("truth_hp_m1","Event-Level Sim.",ybins,ylow,yhigh); truth_hp_list->Add(truth_hp_m1);
	auto truth_hp_m2 = new TH1D("truth_hp_m2","Event-Level Sim.",ybins,ylow,yhigh); truth_hp_list->Add(truth_hp_m2);
	auto truth_hp_m3 = new TH1D("truth_hp_m3","Event-Level Sim.",ybins,ylow,yhigh); truth_hp_list->Add(truth_hp_m3);
	auto truth_hp_m4 = new TH1D("truth_hp_m4","Event-Level Sim.",ybins,ylow,yhigh); truth_hp_list->Add(truth_hp_m4);
	auto truth_hp_m5 = new TH1D("truth_hp_m5","Event-Level Sim.",ybins,ylow,yhigh); truth_hp_list->Add(truth_hp_m5);
	auto truth_hp_m6 = new TH1D("truth_hp_m6","Event-Level Sim.",ybins,ylow,yhigh); truth_hp_list->Add(truth_hp_m6);
	auto truth_hp_m7 = new TH1D("truth_hp_m7","Event-Level Sim.",ybins,ylow,yhigh); truth_hp_list->Add(truth_hp_m7);
	auto truth_hp_m8 = new TH1D("truth_hp_m8","Event-Level Sim.",ybins,ylow,yhigh); truth_hp_list->Add(truth_hp_m8);
	auto truth_hp_m9 = new TH1D("truth_hp_m9","Event-Level Sim.",ybins,ylow,yhigh); truth_hp_list->Add(truth_hp_m9);
	for(int i = 0;i < 10;i++)
	{
		((TH1D*)truth_hp_list->At(i))->GetXaxis()->SetTitle("p_{T}^{2} [GeV^{2}]");
		setLabelY(((TH1D*)truth_hp_list->At(i)),"GeV^{2}");
		((TH1D*)truth_hp_list->At(i))->SetMarkerColor(kBlue);;
		((TH1D*)truth_hp_list->At(i))->SetLineColor(kBlue);;
	}

	////////////////	
	// Event Loop //
	////////////////

	auto nevts = tree->GetEntries();
    	//auto nevts = 10000;
	cout << "Running over " << nevts << " events." << endl;
    	for(int evt_counter = 1;evt_counter < nevts;evt_counter++)
	{
		// print event count
        	if((evt_counter % 100000) == 0) cout << evt_counter << endl;
     
        	// retrieve event
        	tree->GetEntry(evt_counter);

    		// create four vectors for the pions (truth)
    		TLorentzVector truth_pion_vec[2];
		truth_pion_vec[0].SetXYZM(trupx[0],trupy[0],trupz[0],pion_mass);
		truth_pion_vec[1].SetXYZM(trupx[1],trupy[1],trupz[1],pion_mass);

    		// create a four vector for the rho (truth)
		TLorentzVector truth_rho_vec;
    		truth_rho_vec = truth_pion_vec[0] + truth_pion_vec[1];

    		// find the pions' pseudorapidities (truth)
    		double truth_pion1_eta = truth_pion_vec[0].PseudoRapidity();
    		double truth_pion2_eta = truth_pion_vec[1].PseudoRapidity();

    		// find the pions' pts (truth)
    		double truth_pion1_pt = truth_pion_vec[0].Pt();
    		double truth_pion2_pt = truth_pion_vec[1].Pt();

		// find the rho rapidity (truth)
		double truth_rho_y = rhoy; 

		// find the rho mass (truth)
		double truth_rho_mass = rhom; 

		// find the rho rapidity (truth)
		double truth_rho_pt2 = pow(rhopx/1000,2) + pow(rhopy/1000,2); 

    		// truth cut on the nominal detector acceptance
    		if((2 < truth_rho_y) && (truth_rho_y < 5) && \
		(2 < truth_pion1_eta) && (truth_pion1_eta < 5) && \
		(2 < truth_pion2_eta) && (truth_pion2_eta < 5) && \
		(min_trck_pt < truth_pion1_pt) && (min_trck_pt < truth_pion2_pt))
		{
			// fill histogram with truth information
			truth_hy->Fill(truth_rho_y);
			// fill event-level histograms
			truth_hm->Fill(truth_rho_mass);
			truth_hp->Fill(truth_rho_pt2);
			truth_hmp->Fill(truth_rho_mass,truth_rho_pt2);

			// fill mass binned event-level pt2 distributions
			if((600 < truth_rho_mass) && (truth_rho_mass < 640)) ((TH1D*)truth_hp_list->At(0))->Fill(truth_rho_pt2);
			else if((640 < truth_rho_mass) && (truth_rho_mass < 680)) ((TH1D*)truth_hp_list->At(1))->Fill(truth_rho_pt2);
			else if((680 < truth_rho_mass) && (truth_rho_mass < 720)) ((TH1D*)truth_hp_list->At(2))->Fill(truth_rho_pt2);
			else if((720 < truth_rho_mass) && (truth_rho_mass < 760)) ((TH1D*)truth_hp_list->At(3))->Fill(truth_rho_pt2);
			else if((760 < truth_rho_mass) && (truth_rho_mass < 800)) ((TH1D*)truth_hp_list->At(4))->Fill(truth_rho_pt2);
			else if((800 < truth_rho_mass) && (truth_rho_mass < 840)) ((TH1D*)truth_hp_list->At(5))->Fill(truth_rho_pt2);
			else if((840 < truth_rho_mass) && (truth_rho_mass < 880)) ((TH1D*)truth_hp_list->At(6))->Fill(truth_rho_pt2);
			else if((880 < truth_rho_mass) && (truth_rho_mass < 920)) ((TH1D*)truth_hp_list->At(7))->Fill(truth_rho_pt2);
			else if((920 < truth_rho_mass) && (truth_rho_mass < 960)) ((TH1D*)truth_hp_list->At(8))->Fill(truth_rho_pt2);
			else if((960 < truth_rho_mass) && (truth_rho_mass < 1000)) ((TH1D*)truth_hp_list->At(9))->Fill(truth_rho_pt2);

        		// Require exactly two long or upstream tracks
        		if((nout == 2)\
           		&& ((tt[0] == 3) || (tt[0] == 4))\
           		&& ((tt[1] == 3) || (tt[1] == 4)))
			{
	        		// work with the reco information

        			// create four vectors for the pions (reco)
	    			TLorentzVector reco_pion_vec[2];
				reco_pion_vec[0].SetXYZM(px[0],py[0],pz[0],pion_mass);
				reco_pion_vec[1].SetXYZM(px[1],py[1],pz[1],pion_mass);
		
    				// find the pions' pseudorapidities (reco)
    				double reco_pion1_eta = reco_pion_vec[0].PseudoRapidity();
    				double reco_pion2_eta = reco_pion_vec[1].PseudoRapidity();

    				// find the pions' pts (reco)
    				double reco_pion1_pt = reco_pion_vec[0].Pt();
    				double reco_pion2_pt = reco_pion_vec[1].Pt();

    				// create a four vector for the rho (reco)
				TLorentzVector reco_rho_vec;
    				reco_rho_vec = reco_pion_vec[0] + reco_pion_vec[1];

				// get the rho rapidity (reco)
            			double reco_rho_y = reco_rho_vec.Rapidity();

				// get the rho mass (reco)
            			double reco_rho_mass = reco_rho_vec.M();

				// get the rho pt2 (reco)
            			double reco_rho_pt2 = pow(reco_rho_vec.Pt()/1000,2);

            			// tracking cut
				if((2 < reco_rho_y) && (reco_rho_y < 5) && \
				(2 < reco_pion1_eta) && (reco_pion1_eta < 5) && \
				(2 < reco_pion2_eta) && (reco_pion2_eta < 5) && \
				(min_trck_pt < reco_pion1_pt) && (min_trck_pt < reco_pion2_pt))
				{
					// opposite-sign cut
					if(charge[0]*charge[1] == -1)
					{
						int ngamma = 0;
						// loop over photons
						for(int i = 0;i < nphotons;i++)
						{
							int iflag = 1;
							// loop over tracks
							for(int j = 0;j < nout;j++)
							{
								float dely = phy[i]/phz[i] - py[j]/pz[j];
								float delx = phx[i]/phz[i] - px[j]/pz[j] - 780*charge[j]/pz[j];
								if(fabs(delx) < .025 && fabs(dely) < .025) iflag = 0;
							}

							// increment ngamma
							if(prs[i] > 0 && iflag > 0) ngamma += 1;
						}

						// photon-veto
						if(ngamma == 0)
						{
							// calc PV and decay-distance
							double reco_vchi, reco_xv, reco_rho_yv, reco_zv;
							vert(tx[0],ty[0],tz[0],reco_pion_vec[0].Px(),reco_pion_vec[0].Py(),reco_pion_vec[0].Pz(),tx[1],ty[1],tz[1],reco_pion_vec[1].Px(),reco_pion_vec[1].Py(),reco_pion_vec[1].Pz(),reco_xv,reco_rho_yv,reco_zv,reco_vchi);

							// calculate decay-distance
							double reco_decdist = sqrt(pow(reco_xv - reco_beam[0],2) + pow(reco_rho_yv - reco_beam[1],2));

							// cut on decay distance
							if(reco_decdist < decdist_cut)
							{
								reco_hy->Fill(reco_rho_y);
								reco_hm->Fill(reco_rho_mass);
								reco_hp->Fill(reco_rho_pt2);
								reco_hmp->Fill(reco_rho_mass,reco_rho_pt2);
							}
						}
					}
				}
			}
		}
	}

	///////////////////
	// Fit Functions //
	///////////////////

	// mass fit function
	auto fm = new TF1("fm",rhoPlot,600,1000,3); // breit-wigner
	fm->SetParNames("N","rho_width","rho_mass");
	fm->SetParameters(100,rho_width,rho_mass);
	
	// pt2 fit function
	auto fp = new TF1("fp","[0]*([1]*exp(-[1]*x) + [2]*exp(-[2]*x))",0,1);
	fp->SetLineColor(kBlue);
	fp->SetLineWidth(2);
	fp->SetParNames("N","bdis","bel");
	fp->SetParameters(100,2,8);
	fp->SetParLimits(1,0,5);
	fp->SetParLimits(2,5,15);

	// 2D fit function
	auto fmp = new TF2("fmp",fit2D,600,1000,0,1,6); // breit-wigner
	fmp->SetParNames("Nx","rho_width","rho_mass","Ny","a","b");
	
	/////////////////////////
	// Plot and Fit Totals //
	/////////////////////////

	auto truth_fit_tot_cvs = new TCanvas("truth_fit_tot_cvs","Event-Level Sim.");
	truth_fit_tot_cvs->Divide(1,3);
	truth_fit_tot_cvs->cd(1);
	truth_hm->SetMarkerColor(kBlue);
	truth_hm->SetLineColor(kBlue);
	truth_hm->Draw("E1");
	truth_hm->Fit("fm","R");
	truth_fit_tot_cvs->cd(2);
	truth_hp->SetMarkerColor(kBlue);
	truth_hp->SetLineColor(kBlue);
	truth_hp->Draw("E1");
	truth_hp->Fit("fp","R");
	truth_fit_tot_cvs->cd(3);
	truth_hmp->SetMarkerColor(kBlue);
	truth_hmp->SetLineColor(kBlue);
	truth_hmp->Draw("LEGO");
	fmp->SetParameters(fm->GetParameter(0),fm->GetParameter(1),fm->GetParameter(2),fp->GetParameter(0),fp->GetParameter(1),fp->GetParameter(2));
	truth_hmp->Fit("fmp","R");

	auto reco_fit_tot_cvs = new TCanvas("reco_fit_tot_cvs","Reco-Level Sim.");
	reco_fit_tot_cvs->Divide(1,3);
	reco_fit_tot_cvs->cd(1);
	reco_hm->SetMarkerColor(kRed);
	reco_hm->SetLineColor(kRed);
	reco_hm->Draw("E1");
	reco_hm->Fit("fm","R");
	reco_fit_tot_cvs->cd(2);
	reco_hp->SetMarkerColor(kRed);
	reco_hp->SetLineColor(kRed);
	reco_hp->Draw("E1");
	reco_hp->Fit("fp","R");
	reco_fit_tot_cvs->cd(3);
	reco_hmp->SetMarkerColor(kRed);
	reco_hmp->SetLineColor(kRed);
	reco_hmp->Draw("LEGO");
	fmp->SetParameters(fm->GetParameter(0),fm->GetParameter(1),fm->GetParameter(2),fp->GetParameter(0),fp->GetParameter(1),fp->GetParameter(2));
	reco_hmp->Fit("fmp","R");

	////////////////////////////////
	// Plot and Fit Each Mass Bin //
	////////////////////////////////

	auto truth_hp_cvs = new TCanvas("truth_hp_cvs","Event-Level Sim.");
	truth_hp_cvs->Divide(4,3);

	for(int i = 0;i < 10;i++)
	{
		truth_hp_cvs->cd(i+1);
		((TH1D*)truth_hp_list->At(i))->Draw("E1");
		((TH1D*)truth_hp_list->At(i))->Fit("fp","R");
		a[i] = fp->GetParameter(1); a_err[i] = fp->GetParError(1);
		b[i] = fp->GetParameter(2); b_err[i] = fp->GetParError(2);
	}

	// do chi2 test on parameter differences
	parDiffSignf(10,a,a_err,a_chi2,a_chi2_prob);
	parDiffSignf(10,b,b_err,b_chi2,b_chi2_prob);

	///////// 
	// End //
	/////////

	// write histograms to file
	auto output_name = "purity_fact.hists";
	cout << "Creating output file " << output_name << endl;
	auto output_file = new TFile(output_name,"RECREATE");
	cout << "Writing ouput to file..." << endl;
	truth_hp_list->Write();
	output_file->Close();

	// stop timer
	cout << "We're done!" << endl;
	stopwatch->Stop();
	stopwatch->Print();
}

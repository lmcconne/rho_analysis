#ifndef mass_fit_functions_h
#define mass_fit_functions_h

////////////////////////
// mass fit functions //
////////////////////////

// function used to get the rho BW term
TComplex rhoTerm(double *x, double *par);

// function used to plot the rho contribution
double rhoPlot(double *x, double *par);

// function used to get the omega term
TComplex omegaTerm(double *x, double *par);

// function used to plot the omega contribution
double omegaPlot(double *x, double *par);

// MSP fit function (default)
extern TH1D *htemplate;
double fitMSP(double *x, double *par);
// MSP fit function (alternative)
extern TH1D *htemplate_alt_1g_low;
double fitMSP_alt_1g_low(double *x, double *par);
extern TH1D *htemplate_alt_1g_upp;
double fitMSP_alt_1g_upp(double *x, double *par);
extern TH1D *htemplate_alt_2g;
double fitMSP_alt_2g(double *x, double *par);
// fit the MSP fit function with resolution effects (default)
double fitMSP_wRes(double *x,double *par);
// fit the MSP fit function with resolution effects (alternative)
double fitMSP_alt_2g_wRes(double *x,double *par);

// Soeding Parameterisation
double fitSP(double *x, double *par);

// Ross-Stodolsky Model
double fitRS(double *x, double *par);
// fit the Ross-Stodolsky Model with resolution effects
double fitRS_wRes(double *x, double *par);

// linear polynomial function
double fitLinPol(double *x, double *par);

#endif

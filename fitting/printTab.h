#ifndef printTab_h
#define printTab_h

//////////////////////
// Global Variables //
//////////////////////

// purity
double hadterm[12], hadterm_err_stat[12], hadterm_err_sys[12], elterm[12], elterm_err_stat[12], elterm_err_sys[12], purity[12], purity_alt[12], purity_err_stat[12], purity_err_sys[12];

// mass fitting

double sp_rhow[12], sp_rhow_err_stat[12], sp_rhom[12], sp_rhom_err_stat[12], sp_B[12], sp_B_err_stat[12]; // soeding

double rs_rhow[12], rs_rhow_err_stat[12], rs_rhow_chi2, rs_rhow_chi2_prob, rs_rhom[12], rs_rhom_err_stat[12], rs_rhom_chi2, rs_rhom_chi2_prob, rs_k[12], rs_k_err_stat[12], rs_k_chi2, rs_k_chi2_prob, rs_chi2[12], rs_ndf[12], rs_prob[12]; // ross-stodolsky

double rhow[12], rhow_err_stat[12], rhow_err_sys[12], rhow_err_tot[12], rhow_chi2, rhow_chi2_wsys, rhow_chi2_prob, rhow_chi2_prob_wsys, rhom[12], rhom_err_stat[12], rhom_err_sys[12], rhom_err_tot[12], rhom_chi2, rhom_chi2_wsys, rhom_chi2_prob, rhom_chi2_prob_wsys, B[12], B_err_stat[12], B_chi2, B_chi2_prob, C[12], C_err_stat[12], C_chi2, C_chi2_prob, phi[12], phi_err_stat[12], phi_chi2, phi_chi2_prob, D[12], D_err_stat[12], D_chi2, D_chi2_prob, fm_chi2[12], fm_ndf[12], fm_prob[12]; // default

double rhow_alt_1g_low[12], rhow_err_alt_1g_low[12], rhow_chi2_alt_1g_low, rhow_chi2_prob_alt_1g_low, rhom_alt_1g_low[12], rhom_err_alt_1g_low[12], rhom_chi2_alt_1g_low, rhom_chi2_prob_alt_1g_low, B_alt_1g_low[12], B_err_stat_alt_1g_low[12], B_chi2_alt_1g_low, B_chi2_prob_alt_1g_low, C_alt_1g_low[12], C_err_stat_alt_1g_low[12], C_chi2_alt_1g_low, C_chi2_prob_alt_1g_low, phi_alt_1g_low[12], phi_err_stat_alt_1g_low[12], phi_chi2_alt_1g_low, phi_chi2_prob_alt_1g_low, D_alt_1g_low[12], D_err_stat_alt_1g_low[12], D_chi2_alt_1g_low, D_chi2_prob_alt_1g_low, fm_alt_1g_low_chi2[12], fm_alt_1g_low_ndf[12], fm_alt_1g_low_prob[12]; // lower limit alt template

double rhow_alt_1g_upp[12], rhow_err_alt_1g_upp[12], rhow_chi2_alt_1g_upp, rhow_chi2_prob_alt_1g_upp, rhom_alt_1g_upp[12], rhom_err_alt_1g_upp[12], rhom_chi2_alt_1g_upp, rhom_chi2_prob_alt_1g_upp, B_alt_1g_upp[12], B_err_stat_alt_1g_upp[12], B_chi2_alt_1g_upp, B_chi2_prob_alt_1g_upp, C_alt_1g_upp[12], C_err_stat_alt_1g_upp[12], C_chi2_alt_1g_upp, C_chi2_prob_alt_1g_upp, phi_alt_1g_upp[12], phi_err_stat_alt_1g_upp[12], phi_chi2_alt_1g_upp, phi_chi2_prob_alt_1g_upp, D_alt_1g_upp[12], D_err_stat_alt_1g_upp[12], D_chi2_alt_1g_upp, D_chi2_prob_alt_1g_upp, fm_alt_1g_upp_chi2[12], fm_alt_1g_upp_ndf[12], fm_alt_1g_upp_prob[12]; // upper limit alt template

double rhow_alt_2g[12], rhow_err_alt_2g[12], rhow_chi2_alt_2g, rhow_chi2_prob_alt_2g, rhom_alt_2g[12], rhom_err_alt_2g[12], rhom_chi2_alt_2g, rhom_chi2_prob_alt_2g, B_alt_2g[12], B_err_stat_alt_2g[12], B_chi2_alt_2g, B_chi2_prob_alt_2g, C_alt_2g[12], C_err_stat_alt_2g[12], C_chi2_alt_2g, C_chi2_prob_alt_2g, phi_alt_2g[12], phi_err_stat_alt_2g[12], phi_chi2_alt_2g, phi_chi2_prob_alt_2g, D_alt_2g[12], D_err_stat_alt_2g[12], D_chi2_alt_2g, D_chi2_prob_alt_2g, fm_alt_2g_chi2[12], fm_alt_2g_ndf[12], fm_alt_2g_prob[12]; // pipigammagamma alt template

double rhow_err_sys_rs[12],rhow_err_sys_alt_1g_low[12], rhow_err_sys_alt_1g_upp[12], rhow_err_sys_alt_2g[12], rhom_err_sys_rs[12], rhom_err_sys_alt_1g_low[12], rhom_err_sys_alt_1g_upp[12], rhom_err_sys_alt_2g[12]; // systematics

double fm_sig_cor_elems[12][6][6], fm_rs_cor_elems[12][3][3], fm_alt_1g_low_cor_elems[12][6][6], fm_alt_1g_upp_cor_elems[12][6][6], fm_alt_2g_cor_elems[12][6][6]; // correlation matrix elements

// pt2 fitting

double fel[12], fel_err_stat[12], fel_err_sys[12], bel[12], bel_low[12], bel_upp[12], bel_err_stat[12], bel_err_sys[12], bel_err_tot[12], bdis[12], bdis_err_stat[12], bdis_err_sys[12], zeta[12], zeta_err_stat[12], zeta_err_sys[12], fp_sig_chi2[12], fp_sig_ndf[12], fp_sig_prob[12], fp_bkg_chi2[12], fp_bkg_ndf[12], fp_bkg_prob[12]; // parameters, errors, and chi2

double fel_alt[12],  fel_alt_err_stat[12], bel_alt[12], bel_alt_err_stat[12], bdis_alt[12], bdis_alt_err_stat[12], zeta_alt[12], zeta_alt_err_stat[12], fp_sig_chi2_alt[12], fp_sig_ndf_alt[12], fp_sig_prob_alt[12], fp_bkg_chi2_alt[12], fp_bkg_ndf_alt[12], fp_bkg_prob_alt[12];

double fp_sig_cor_elems[12][2][2], fp_sig_cor_elems_alt[12][2][2], fp_bkg_cor_elems[12][2][2], fp_bkg_cor_elems_alt[12][2][2]; // correlation matrix elements

// angular fitting
double lsig[12], lsig_err[12], ldis[12], ldis_err[12], ft_chi2[12], ft_sig_chi2[12], ft_bkg_chi2[12], ft_sig_fix_chi2[12];

// x-section

double Wgp[12], Egamma[12], flux[12], flux_err[12]; // needed to convert to photo-xs

double xs[12], xs_err_stat[12], xs_err_sys[12], xs_err_tot[12], phxs[12], phxs_low[12], phxs_upp[12], phxs_err_stat[12], phxs_err_sys[12], phxs_err_tot[12]; // cross-section values and errors

double nevts_raw[12], nevts_raw_err_stat[12], nevts_raw_err_sys[12], nevts_ycor[12], nevts_ycor_err_stat[12], nevts_ycor_err_sys[12], nevts_pur[12], nevts_pur_err_stat[12], nevts_pur_err_sys[12], nevts_lumi[12], nevts_lumi_err_stat[12], nevts_lumi_err_sys[12], nevts_phof[12], nevts_phof_err_stat[12], nevts_phof_err_sys[12]; // number of events and errors at each calc stage

double err_stat_array[12], err_sys_array[12], tot_err_array[12]; // arrays for error table

double bel0, bel0_low, bel0_upp, bel0_err_stat, bel0_err_sys, pslope, pslope_low, pslope_upp, pslope_err_stat, pslope_err_sys, chi2_bslope, ndf_bslope, prob_bslope; // for b slope table

double sigma0, sigma0_low, sigma0_upp, sigma0_err_stat, sigma0_err_sys, fR, fR_low, fR_upp, fR_err_stat, fR_err_sys, dP, dP_low, dP_upp, dP_err_stat, dP_err_sys, chi2_phxs, ndf_phxs, prob_phxs; // for phxs fit table

#endif

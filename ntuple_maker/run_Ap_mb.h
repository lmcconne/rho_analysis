//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Apr 25 21:03:28 2022 by ROOT version 6.24/06
// from TChain TupleTools/MyTuple/
//////////////////////////////////////////////////////////

#ifndef run_Ap_mb_h
#define run_Ap_mb_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class run_Ap_mb {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          BCID;
   Int_t           BCType;
   UInt_t          OdinTCK;
   UInt_t          L0DUTCK;
   UInt_t          HLT1TCK;
   UInt_t          HLT2TCK;
   ULong64_t       GpsTime;
   Short_t         Polarity;
   Int_t           nPVs;
   Int_t           nTracks;
   Int_t           nLongTracks;
   Int_t           nDownstreamTracks;
   Int_t           nUpstreamTracks;
   Int_t           nVeloTracks;
   Int_t           nTTracks;
   Int_t           nBackTracks;
   Int_t           nRich1Hits;
   Int_t           nRich2Hits;
   Int_t           nVeloClusters;
   Int_t           nITClusters;
   Int_t           nTTClusters;
   Int_t           nOTClusters;
   Int_t           nSPDHits;
   Int_t           nMuonCoordsS0;
   Int_t           nMuonCoordsS1;
   Int_t           nMuonCoordsS2;
   Int_t           nMuonCoordsS3;
   Int_t           nMuonCoordsS4;
   Int_t           nMuonTracks;
   Int_t           B00;
   Int_t           B01;
   Int_t           B02;
   Int_t           B03;
   Int_t           B10;
   Int_t           B11;
   Int_t           B12;
   Int_t           B13;
   Int_t           B20;
   Int_t           B21;
   Int_t           B22;
   Int_t           B23;
   Int_t           F10;
   Int_t           F11;
   Int_t           F12;
   Int_t           F13;
   Int_t           F20;
   Int_t           F21;
   Int_t           F22;
   Int_t           F23;
   Double_t        log_hrc_fom_v3;
   Double_t        log_hrc_fom_B_v3;
   Double_t        log_hrc_fom_F_v3;
   Int_t           nchB;
   Float_t         adc_B[1000];   //[nchB]
   Int_t           nchF;
   Float_t         adc_F[1000];   //[nchF]
   Int_t           L0Global;
   UInt_t          Hlt1Global;
   UInt_t          Hlt2Global;
   Int_t           L0Muon_lowMultDecision;
   Int_t           L0DiMuon_lowMultDecision;
   Int_t           L0DiHadron_lowMultDecision;
   Int_t           L0Electron_lowMultDecision;
   Int_t           L0Photon_lowMultDecision;
   Int_t           L0DiEM_lowMultDecision;
   UInt_t          L0nSelections;
   Int_t           Hlt1NoPVPassThroughDecision;
   Int_t           Hlt1NoBiasNonBeamBeamDecision;
   Int_t           Hlt1MBNoBiasRateLimitedDecision;
   Int_t           Hlt1MBNoBiasDecision;
   Int_t           Hlt1CEPVeloCutDecision;
   Int_t           Hlt1LowMultMuonDecision;
   UInt_t          Hlt1nSelections;
   Int_t           Hlt2NoBiasNonBeamBeamDecision;
   Int_t           Hlt2LowMultLMR2HHDecision;
   Int_t           Hlt2LowMultLMR2HHWSDecision;
   Int_t           Hlt2LowMultLMR2HHHHDecision;
   Int_t           Hlt2LowMultLMR2HHHHWSDecision;
   Int_t           Hlt2LowMultL2pPiDecision;
   Int_t           Hlt2LowMultD2KPiDecision;
   Int_t           Hlt2LowMultD2KPiPiDecision;
   Int_t           Hlt2LowMultD2KKPiDecision;
   Int_t           Hlt2LowMultD2K3PiDecision;
   Int_t           Hlt2LowMultChiC2HHDecision;
   Int_t           Hlt2LowMultChiC2HHHHDecision;
   Int_t           Hlt2LowMultChiC2PPDecision;
   Int_t           Hlt2LowMultHadron_noTrFiltDecision;
   Int_t           Hlt2LowMultL2pPiWSDecision;
   Int_t           Hlt2LowMultD2KPiWSDecision;
   Int_t           Hlt2LowMultD2KPiPiWSDecision;
   Int_t           Hlt2LowMultD2KKPiWSDecision;
   Int_t           Hlt2LowMultD2K3PiWSDecision;
   Int_t           Hlt2LowMultChiC2HHWSDecision;
   Int_t           Hlt2LowMultChiC2HHHHWSDecision;
   Int_t           Hlt2LowMultChiC2PPWSDecision;
   Int_t           Hlt2LowMultMuonDecision;
   Int_t           Hlt2LowMultDiMuonDecision;
   Int_t           Hlt2LowMultDiMuon_PSDecision;
   Int_t           Hlt2LowMultDiPhotonDecision;
   Int_t           Hlt2LowMultDiPhoton_HighMassDecision;
   Int_t           Hlt2LowMultpi0Decision;
   Int_t           Hlt2LowMultDiElectronDecision;
   Int_t           Hlt2LowMultDiElectron_noTrFiltDecision;
   Int_t           Hlt2MBMicroBiasVeloDecision;
   Int_t           Hlt2LowMultTechnical_MinBiasDecision;
   UInt_t          Hlt2nSelections;
   Int_t           MaxRoutingBits;
   Float_t         RoutingBits[64];   //[MaxRoutingBits]
   UInt_t          StrippingMBNoBiasDecision;
   UInt_t          StrippingHlt1NoBiasNonBeamBeamLineDecision;
   UInt_t          StrippingLowMultNonBeamBeamNoBiasLineDecision;
   UInt_t          StrippingLowMultMuonLineDecision;
   UInt_t          StrippingLowMultDiMuonLineDecision;
   Float_t         ET_prev1;
   Float_t         ET_prev2;
   Int_t           nouttrck;
   Float_t         trck_key[10];   //[nouttrck]
   Float_t         trck_type[10];   //[nouttrck]
   Float_t         trck_posx[10];   //[nouttrck]
   Float_t         trck_posy[10];   //[nouttrck]
   Float_t         trck_posz[10];   //[nouttrck]
   Float_t         trck_px[10];   //[nouttrck]
   Float_t         trck_py[10];   //[nouttrck]
   Float_t         trck_pz[10];   //[nouttrck]
   Float_t         trck_charge[10];   //[nouttrck]
   Float_t         trck_muonid[10];   //[nouttrck]
   Float_t         trck_riche[10];   //[nouttrck]
   Float_t         trck_richk[10];   //[nouttrck]
   Float_t         trck_richp[10];   //[nouttrck]
   Float_t         trck_muonll[10];   //[nouttrck]
   Float_t         trck_muacc[10];   //[nouttrck]
   Float_t         trck_ecal[10];   //[nouttrck]
   Float_t         trck_hcal[10];   //[nouttrck]
   Float_t         trck_eop[10];   //[nouttrck]
   Float_t         trck_probe[10];   //[nouttrck]
   Float_t         trck_probmu[10];   //[nouttrck]
   Float_t         trck_probpi[10];   //[nouttrck]
   Float_t         trck_probk[10];   //[nouttrck]
   Float_t         trck_probp[10];   //[nouttrck]
   Float_t         trck_probghost[10];   //[nouttrck]
   Int_t           noutmuon;
   Float_t         muon_chi2ndf[10];   //[noutmuon]
   Float_t         muon_posx[10];   //[noutmuon]
   Float_t         muon_posy[10];   //[noutmuon]
   Float_t         muon_ux[10];   //[noutmuon]
   Float_t         muon_uy[10];   //[noutmuon]
   Float_t         muon_nshared[10];   //[noutmuon]
   Int_t           nphotons;
   Int_t           noutphot;
   Float_t         photon_px[10];   //[noutphot]
   Float_t         photon_py[10];   //[noutphot]
   Float_t         photon_pz[10];   //[noutphot]
   Float_t         photon_nspd[10];   //[noutphot]
   Float_t         photon_nprs[10];   //[noutphot]
   Int_t           noutspd;
   Float_t         spd_posx[30];   //[noutspd]
   Float_t         spd_posy[30];   //[noutspd]
   Int_t           noutprs;
   Float_t         prs_posx[30];   //[noutprs]
   Float_t         prs_posy[30];   //[noutprs]
   Int_t           noutl0mu;
   Float_t         l0mu_pt[10];   //[noutl0mu]
   Float_t         l0mu_phi[10];   //[noutl0mu]
   Float_t         l0mu_theta[10];   //[noutl0mu]
   Int_t           noutl0h;
   Float_t         l0h_et[10];   //[noutl0h]

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_BCType;   //!
   TBranch        *b_OdinTCK;   //!
   TBranch        *b_L0DUTCK;   //!
   TBranch        *b_HLT1TCK;   //!
   TBranch        *b_HLT2TCK;   //!
   TBranch        *b_GpsTime;   //!
   TBranch        *b_Polarity;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nLongTracks;   //!
   TBranch        *b_nDownstreamTracks;   //!
   TBranch        *b_nUpstreamTracks;   //!
   TBranch        *b_nVeloTracks;   //!
   TBranch        *b_nTTracks;   //!
   TBranch        *b_nBackTracks;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nVeloClusters;   //!
   TBranch        *b_nITClusters;   //!
   TBranch        *b_nTTClusters;   //!
   TBranch        *b_nOTClusters;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_nMuonCoordsS0;   //!
   TBranch        *b_nMuonCoordsS1;   //!
   TBranch        *b_nMuonCoordsS2;   //!
   TBranch        *b_nMuonCoordsS3;   //!
   TBranch        *b_nMuonCoordsS4;   //!
   TBranch        *b_nMuonTracks;   //!
   TBranch        *b_B00;   //!
   TBranch        *b_B01;   //!
   TBranch        *b_B02;   //!
   TBranch        *b_B03;   //!
   TBranch        *b_B10;   //!
   TBranch        *b_B11;   //!
   TBranch        *b_B12;   //!
   TBranch        *b_B13;   //!
   TBranch        *b_B20;   //!
   TBranch        *b_B21;   //!
   TBranch        *b_B22;   //!
   TBranch        *b_B23;   //!
   TBranch        *b_F10;   //!
   TBranch        *b_F11;   //!
   TBranch        *b_F12;   //!
   TBranch        *b_F13;   //!
   TBranch        *b_F20;   //!
   TBranch        *b_F21;   //!
   TBranch        *b_F22;   //!
   TBranch        *b_F23;   //!
   TBranch        *b_log_hrc_fom_v3;   //!
   TBranch        *b_log_hrc_fom_B_v3;   //!
   TBranch        *b_log_hrc_fom_F_v3;   //!
   TBranch        *b_nchB;   //!
   TBranch        *b_adc_B;   //!
   TBranch        *b_nchF;   //!
   TBranch        *b_adc_F;   //!
   TBranch        *b_L0Global;   //!
   TBranch        *b_Hlt1Global;   //!
   TBranch        *b_Hlt2Global;   //!
   TBranch        *b_L0Muon_lowMultDecision;   //!
   TBranch        *b_L0DiMuon_lowMultDecision;   //!
   TBranch        *b_L0DiHadron_lowMultDecision;   //!
   TBranch        *b_L0Electron_lowMultDecision;   //!
   TBranch        *b_L0Photon_lowMultDecision;   //!
   TBranch        *b_L0DiEM_lowMultDecision;   //!
   TBranch        *b_L0nSelections;   //!
   TBranch        *b_Hlt1NoPVPassThroughDecision;   //!
   TBranch        *b_Hlt1NoBiasNonBeamBeamDecision;   //!
   TBranch        *b_Hlt1MBNoBiasRateLimitedDecision;   //!
   TBranch        *b_Hlt1MBNoBiasDecision;   //!
   TBranch        *b_Hlt1CEPVeloCutDecision;   //!
   TBranch        *b_Hlt1LowMultMuonDecision;   //!
   TBranch        *b_Hlt1nSelections;   //!
   TBranch        *b_Hlt2NoBiasNonBeamBeamDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHWSDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHHHDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHHHWSDecision;   //!
   TBranch        *b_Hlt2LowMultL2pPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2KKPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2K3PiDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHHHDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2PPDecision;   //!
   TBranch        *b_Hlt2LowMultHadron_noTrFiltDecision;   //!
   TBranch        *b_Hlt2LowMultL2pPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2KKPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2K3PiWSDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHWSDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHHHWSDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2PPWSDecision;   //!
   TBranch        *b_Hlt2LowMultMuonDecision;   //!
   TBranch        *b_Hlt2LowMultDiMuonDecision;   //!
   TBranch        *b_Hlt2LowMultDiMuon_PSDecision;   //!
   TBranch        *b_Hlt2LowMultDiPhotonDecision;   //!
   TBranch        *b_Hlt2LowMultDiPhoton_HighMassDecision;   //!
   TBranch        *b_Hlt2LowMultpi0Decision;   //!
   TBranch        *b_Hlt2LowMultDiElectronDecision;   //!
   TBranch        *b_Hlt2LowMultDiElectron_noTrFiltDecision;   //!
   TBranch        *b_Hlt2MBMicroBiasVeloDecision;   //!
   TBranch        *b_Hlt2LowMultTechnical_MinBiasDecision;   //!
   TBranch        *b_Hlt2nSelections;   //!
   TBranch        *b_MaxRoutingBits;   //!
   TBranch        *b_RoutingBits;   //!
   TBranch        *b_StrippingMBNoBiasDecision;   //!
   TBranch        *b_StrippingHlt1NoBiasNonBeamBeamLineDecision;   //!
   TBranch        *b_StrippingLowMultNonBeamBeamNoBiasLineDecision;   //!
   TBranch        *b_StrippingLowMultMuonLineDecision;   //!
   TBranch        *b_StrippingLowMultDiMuonLineDecision;   //!
   TBranch        *b_ET_prev1;   //!
   TBranch        *b_ET_prev2;   //!
   TBranch        *b_nouttrck;   //!
   TBranch        *b_trck_key;   //!
   TBranch        *b_trck_type;   //!
   TBranch        *b_trck_posx;   //!
   TBranch        *b_trck_posy;   //!
   TBranch        *b_trck_posz;   //!
   TBranch        *b_trck_px;   //!
   TBranch        *b_trck_py;   //!
   TBranch        *b_trck_pz;   //!
   TBranch        *b_trck_charge;   //!
   TBranch        *b_trck_muonid;   //!
   TBranch        *b_trck_riche;   //!
   TBranch        *b_trck_richk;   //!
   TBranch        *b_trck_richp;   //!
   TBranch        *b_trck_muonll;   //!
   TBranch        *b_trck_muacc;   //!
   TBranch        *b_trck_ecal;   //!
   TBranch        *b_trck_hcal;   //!
   TBranch        *b_trck_eop;   //!
   TBranch        *b_trck_probe;   //!
   TBranch        *b_trck_probmu;   //!
   TBranch        *b_trck_probpi;   //!
   TBranch        *b_trck_probk;   //!
   TBranch        *b_trck_probp;   //!
   TBranch        *b_trck_probghost;   //!
   TBranch        *b_noutmuon;   //!
   TBranch        *b_muon_chi2ndf;   //!
   TBranch        *b_muon_posx;   //!
   TBranch        *b_muon_posy;   //!
   TBranch        *b_muon_ux;   //!
   TBranch        *b_muon_uy;   //!
   TBranch        *b_muon_nshared;   //!
   TBranch        *b_nphotons;   //!
   TBranch        *b_noutphot;   //!
   TBranch        *b_photon_px;   //!
   TBranch        *b_photon_py;   //!
   TBranch        *b_photon_pz;   //!
   TBranch        *b_photon_nspd;   //!
   TBranch        *b_photon_nprs;   //!
   TBranch        *b_noutspd;   //!
   TBranch        *b_spd_posx;   //!
   TBranch        *b_spd_posy;   //!
   TBranch        *b_noutprs;   //!
   TBranch        *b_prs_posx;   //!
   TBranch        *b_prs_posy;   //!
   TBranch        *b_noutl0mu;   //!
   TBranch        *b_l0mu_pt;   //!
   TBranch        *b_l0mu_phi;   //!
   TBranch        *b_l0mu_theta;   //!
   TBranch        *b_noutl0h;   //!
   TBranch        *b_l0h_et;   //!

   run_Ap_mb(TTree *tree=0);
   virtual ~run_Ap_mb();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef run_Ap_mb_cxx
run_Ap_mb::run_Ap_mb(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      f->GetObject("TupleTools/MyTuple",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
      TChain * chain = new TChain("TupleTools/MyTuple","");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666097.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666098.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666099.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666100.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666101.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666102.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666103.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666104.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666105.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666106.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666107.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666108.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666109.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666110.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666111.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666112.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666113.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666114.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666115.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666116.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666117.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666118.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666119.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666120.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666121.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666122.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666123.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666124.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666125.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666126.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666127.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666128.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666129.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666130.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666131.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666132.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666133.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666134.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666135.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666137.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666138.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666139.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666141.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666142.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666143.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666146.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666147.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666150.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666156.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666157.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666158.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666159.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666160.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666161.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666162.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666163.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666164.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666169.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666176.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666183.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666194.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666203.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666216.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666220.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666221.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666222.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666223.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666224.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666225.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666226.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666227.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666228.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666229.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666230.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666231.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666232.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666233.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666234.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666235.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666236.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666237.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666238.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666239.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666240.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666242.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666243.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666244.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666245.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666246.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666247.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666248.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666249.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666250.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666251.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666252.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666253.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666254.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666255.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666256.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666257.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666258.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666259.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666260.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666261.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666262.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666263.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666264.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666265.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666266.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666267.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666268.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666269.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666270.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666271.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666272.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666273.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666274.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666275.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666276.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666277.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666278.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666279.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666280.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666281.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666282.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666283.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666284.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666285.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666286.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666287.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666288.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666289.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666290.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666291.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666292.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666293.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666294.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666295.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666296.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666297.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666298.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666300.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666302.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666304.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666308.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666309.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666310.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666311.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666312.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666313.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666314.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666315.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666316.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666317.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666318.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666319.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666320.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666321.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666322.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666323.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666324.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666325.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666326.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666327.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666328.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666329.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666330.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666331.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666332.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666333.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666334.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666335.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666336.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666337.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666338.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666339.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666340.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666341.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666342.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666343.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666344.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666345.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666346.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666347.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666348.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666349.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666350.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666351.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666352.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666353.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666354.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666355.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666356.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666357.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666358.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666359.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666360.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666361.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666362.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666363.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666364.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666365.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666366.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666367.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666368.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple239666369.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple240472224.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285191.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285204.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285222.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285231.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285238.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285243.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285251.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285258.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285273.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285281.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285291.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285304.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285316.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285330.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285340.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285351.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285357.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285367.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285373.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285379.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285388.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285402.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285411.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285420.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285429.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285443.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285453.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285459.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285474.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285484.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285489.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285499.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285505.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285508.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285514.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285523.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285544.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285561.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285570.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285572.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285578.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285584.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285590.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285597.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285603.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285609.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285613.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285619.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285624.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285627.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285631.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285635.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285639.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285645.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285656.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285669.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285684.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285689.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285698.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285720.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285731.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285741.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285746.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285760.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285769.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285781.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285797.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285809.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285817.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285828.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285842.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285859.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285871.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285880.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285887.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285910.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285926.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285944.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285967.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285977.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260285991.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286000.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286009.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286020.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286027.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286038.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286046.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286056.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286063.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286072.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286083.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286096.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286106.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286122.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286135.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286143.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286152.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286158.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286162.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286167.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286231.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286238.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286246.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286258.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286264.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286268.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286279.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286288.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286298.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286309.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286319.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286326.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286333.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286339.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286343.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286345.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286351.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286353.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286358.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286367.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286371.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286379.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286386.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286395.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286400.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286403.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286410.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286420.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286422.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286426.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286429.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286433.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286438.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286445.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286450.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286460.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286465.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286473.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286479.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286482.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286488.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286495.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286500.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286512.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286521.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286533.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286538.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286543.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286557.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286567.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286572.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286581.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286590.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286609.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286627.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286636.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286652.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286674.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286682.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286687.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286704.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286716.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286725.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286739.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286749.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286757.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286761.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286763.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286767.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286769.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286774.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286777.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286783.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286785.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286787.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286789.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286790.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286791.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286794.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286795.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286801.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286802.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286804.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286805.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286807.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286809.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286810.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286812.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286814.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286815.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286817.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286819.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286820.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286821.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286822.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286823.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286824.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286825.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286826.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286827.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286828.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286829.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286830.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286831.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286832.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286833.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286834.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286835.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286836.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286837.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286838.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286839.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286840.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286841.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286842.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286843.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286844.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286845.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286846.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286847.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286848.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286849.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286850.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286851.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286852.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286853.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286854.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286855.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286856.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286857.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286858.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286859.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286860.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286861.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286863.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286865.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286866.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286868.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286870.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286877.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286882.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286890.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286895.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286901.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286906.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286919.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286923.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286932.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286935.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286942.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286946.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286951.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286960.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286973.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286977.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286985.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286990.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260286999.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260287005.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260287007.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260287013.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260287017.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260287021.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260287026.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260287029.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939752.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939754.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939758.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939761.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939763.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939767.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939772.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939777.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939781.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939787.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939792.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939801.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939811.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939817.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939822.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939827.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939833.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939839.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939844.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939849.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939854.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939860.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939865.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939870.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939877.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939883.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939893.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939901.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939909.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939914.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939920.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939928.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939940.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939947.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939954.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939960.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939970.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939979.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939990.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260939998.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940005.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940012.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940018.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940025.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940032.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940040.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940048.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940050.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940056.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940066.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940073.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940084.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940091.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940104.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940117.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940132.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940143.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940156.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940168.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940184.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940194.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940209.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940221.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940232.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940239.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940247.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940252.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940257.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940262.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940266.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940277.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940286.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940296.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940305.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940315.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940321.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940338.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940345.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940350.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940356.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940367.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940378.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940391.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940401.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940406.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940409.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940414.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940422.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940427.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940432.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940434.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940441.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940449.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940453.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940464.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940469.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940475.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940482.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940485.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940532.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940538.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940553.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940559.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940566.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940573.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940581.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940589.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940594.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940599.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940603.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940607.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940617.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940626.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940631.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940636.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940644.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940651.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940657.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940667.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940671.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940678.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940681.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940686.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940692.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940698.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940704.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940709.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940718.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940724.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940731.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940736.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940744.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940749.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940755.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940760.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940768.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940773.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940781.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940793.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940801.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940815.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940825.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940832.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940847.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940856.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940872.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940879.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940897.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940913.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940920.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940940.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940957.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940969.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940979.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260940986.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941001.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941013.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941027.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941044.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941058.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941073.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941083.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941089.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941096.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941103.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941110.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941116.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941119.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941123.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941131.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941138.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941142.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941146.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941150.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941154.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941157.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941162.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941165.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941168.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941172.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941176.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941178.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941182.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941189.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941196.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941200.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941203.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941209.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941212.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941216.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941220.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941223.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941227.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941230.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941234.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941241.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941246.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941251.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941309.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941314.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941319.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941321.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941326.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941330.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941333.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941340.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941342.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941345.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941348.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941353.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941358.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941360.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941365.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941379.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941385.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941387.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941390.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941394.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941397.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941399.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941402.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941403.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941407.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941413.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941420.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941424.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941426.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941429.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941431.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941434.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941436.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941439.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941442.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941444.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941447.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941450.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941451.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941454.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941456.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941459.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941461.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941466.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941470.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941478.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941484.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941492.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941499.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941506.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941522.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941533.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941548.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941566.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941583.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941604.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941630.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941656.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941682.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941702.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941725.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941735.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941755.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941779.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941800.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941826.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941846.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941886.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941897.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941911.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941926.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941954.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941969.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941979.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260941991.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942005.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942016.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942032.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942038.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942045.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942054.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942061.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942070.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942079.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942085.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942091.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942099.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942103.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942113.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942119.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942129.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942139.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942149.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942158.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942169.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942177.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942187.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942197.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942209.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942975.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942987.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942993.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260942997.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943007.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943016.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943020.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943031.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943038.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943046.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943051.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943057.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943063.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943072.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943078.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943082.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943086.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943093.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943100.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943108.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943119.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943129.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943135.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943142.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943146.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943151.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943160.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943168.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943173.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943179.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943185.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943188.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943193.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943195.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943200.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943201.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943210.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943216.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943219.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943224.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943227.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943234.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943238.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943243.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943247.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943251.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943256.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943259.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943262.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple260943264.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293803.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293813.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293827.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293835.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293846.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293860.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293864.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293873.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293881.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293891.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293901.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293908.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293918.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293928.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293937.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293948.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293957.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293964.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293971.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293978.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293986.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264293995.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294000.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294008.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294012.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294019.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294024.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294029.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294033.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294043.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294052.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294060.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294066.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294069.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294074.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294077.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294083.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294091.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294095.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294100.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294104.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294110.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294117.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294123.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294127.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294134.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294141.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294146.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294152.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294159.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294164.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294167.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294170.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294174.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294177.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294178.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294180.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294183.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294186.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294189.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294193.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294195.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294197.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294202.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294204.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294207.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294208.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294210.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294211.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294213.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294214.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294216.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294218.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294220.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294222.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294224.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294225.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294226.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294228.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294231.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294234.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294236.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294239.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294241.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294242.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294244.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294246.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294250.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294254.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294258.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294260.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294262.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294264.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294266.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294268.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294269.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294270.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294272.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294275.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294276.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294293.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294294.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294296.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294298.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294299.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294301.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294302.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294305.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294306.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294308.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294310.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294312.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294313.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294319.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294322.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294325.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294329.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294336.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294344.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294351.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294359.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294368.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294376.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294384.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294396.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294409.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294418.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294427.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294439.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294446.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294452.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294458.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294467.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294478.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294481.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294487.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294493.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294500.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294504.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294512.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294516.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294521.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294528.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294533.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294538.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294544.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294549.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294551.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294557.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294561.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294565.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294568.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294573.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294576.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294579.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294582.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294585.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294587.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294590.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294600.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294605.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294609.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294613.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294617.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294623.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294628.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294633.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294638.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294645.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294651.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294655.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294660.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294663.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294668.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294683.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294685.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294688.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294692.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294695.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294698.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294700.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294705.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294709.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294711.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294715.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294718.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294720.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294722.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294726.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294727.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294730.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294736.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294738.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294741.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294743.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294745.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294747.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294749.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294750.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294752.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294767.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294768.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294771.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294773.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294774.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294776.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294778.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294780.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294782.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294783.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294785.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294787.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294788.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294790.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294791.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294793.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294794.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294796.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294797.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294799.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294801.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294802.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294804.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294807.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294809.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294811.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294812.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294814.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294815.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294817.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294818.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294820.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294822.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294823.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294825.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294826.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294828.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294830.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294831.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294833.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294834.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294836.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294837.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294839.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294841.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294842.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294844.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294846.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294847.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294849.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294850.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294852.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294853.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294855.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294856.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294858.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294860.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294861.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294864.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294865.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294867.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294869.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294871.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294873.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294875.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294877.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294879.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294881.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294883.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294885.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294887.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294889.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294891.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/Pbp2016_nobias/mytuple264294892.root/TupleTools/MyTuple");
      tree = chain;
#endif // SINGLE_TREE

   }
   Init(tree);
}

run_Ap_mb::~run_Ap_mb()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t run_Ap_mb::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t run_Ap_mb::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void run_Ap_mb::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
   fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
   fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
   fChain->SetBranchAddress("HLT1TCK", &HLT1TCK, &b_HLT1TCK);
   fChain->SetBranchAddress("HLT2TCK", &HLT2TCK, &b_HLT2TCK);
   fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
   fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nLongTracks", &nLongTracks, &b_nLongTracks);
   fChain->SetBranchAddress("nDownstreamTracks", &nDownstreamTracks, &b_nDownstreamTracks);
   fChain->SetBranchAddress("nUpstreamTracks", &nUpstreamTracks, &b_nUpstreamTracks);
   fChain->SetBranchAddress("nVeloTracks", &nVeloTracks, &b_nVeloTracks);
   fChain->SetBranchAddress("nTTracks", &nTTracks, &b_nTTracks);
   fChain->SetBranchAddress("nBackTracks", &nBackTracks, &b_nBackTracks);
   fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
   fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
   fChain->SetBranchAddress("nVeloClusters", &nVeloClusters, &b_nVeloClusters);
   fChain->SetBranchAddress("nITClusters", &nITClusters, &b_nITClusters);
   fChain->SetBranchAddress("nTTClusters", &nTTClusters, &b_nTTClusters);
   fChain->SetBranchAddress("nOTClusters", &nOTClusters, &b_nOTClusters);
   fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
   fChain->SetBranchAddress("nMuonCoordsS0", &nMuonCoordsS0, &b_nMuonCoordsS0);
   fChain->SetBranchAddress("nMuonCoordsS1", &nMuonCoordsS1, &b_nMuonCoordsS1);
   fChain->SetBranchAddress("nMuonCoordsS2", &nMuonCoordsS2, &b_nMuonCoordsS2);
   fChain->SetBranchAddress("nMuonCoordsS3", &nMuonCoordsS3, &b_nMuonCoordsS3);
   fChain->SetBranchAddress("nMuonCoordsS4", &nMuonCoordsS4, &b_nMuonCoordsS4);
   fChain->SetBranchAddress("nMuonTracks", &nMuonTracks, &b_nMuonTracks);
   fChain->SetBranchAddress("B00", &B00, &b_B00);
   fChain->SetBranchAddress("B01", &B01, &b_B01);
   fChain->SetBranchAddress("B02", &B02, &b_B02);
   fChain->SetBranchAddress("B03", &B03, &b_B03);
   fChain->SetBranchAddress("B10", &B10, &b_B10);
   fChain->SetBranchAddress("B11", &B11, &b_B11);
   fChain->SetBranchAddress("B12", &B12, &b_B12);
   fChain->SetBranchAddress("B13", &B13, &b_B13);
   fChain->SetBranchAddress("B20", &B20, &b_B20);
   fChain->SetBranchAddress("B21", &B21, &b_B21);
   fChain->SetBranchAddress("B22", &B22, &b_B22);
   fChain->SetBranchAddress("B23", &B23, &b_B23);
   fChain->SetBranchAddress("F10", &F10, &b_F10);
   fChain->SetBranchAddress("F11", &F11, &b_F11);
   fChain->SetBranchAddress("F12", &F12, &b_F12);
   fChain->SetBranchAddress("F13", &F13, &b_F13);
   fChain->SetBranchAddress("F20", &F20, &b_F20);
   fChain->SetBranchAddress("F21", &F21, &b_F21);
   fChain->SetBranchAddress("F22", &F22, &b_F22);
   fChain->SetBranchAddress("F23", &F23, &b_F23);
   fChain->SetBranchAddress("log_hrc_fom_v3", &log_hrc_fom_v3, &b_log_hrc_fom_v3);
   fChain->SetBranchAddress("log_hrc_fom_B_v3", &log_hrc_fom_B_v3, &b_log_hrc_fom_B_v3);
   fChain->SetBranchAddress("log_hrc_fom_F_v3", &log_hrc_fom_F_v3, &b_log_hrc_fom_F_v3);
   fChain->SetBranchAddress("nchB", &nchB, &b_nchB);
   fChain->SetBranchAddress("adc_B", adc_B, &b_adc_B);
   fChain->SetBranchAddress("nchF", &nchF, &b_nchF);
   fChain->SetBranchAddress("adc_F", adc_F, &b_adc_F);
   fChain->SetBranchAddress("L0Global", &L0Global, &b_L0Global);
   fChain->SetBranchAddress("Hlt1Global", &Hlt1Global, &b_Hlt1Global);
   fChain->SetBranchAddress("Hlt2Global", &Hlt2Global, &b_Hlt2Global);
   fChain->SetBranchAddress("L0Muon,lowMultDecision", &L0Muon_lowMultDecision, &b_L0Muon_lowMultDecision);
   fChain->SetBranchAddress("L0DiMuon,lowMultDecision", &L0DiMuon_lowMultDecision, &b_L0DiMuon_lowMultDecision);
   fChain->SetBranchAddress("L0DiHadron,lowMultDecision", &L0DiHadron_lowMultDecision, &b_L0DiHadron_lowMultDecision);
   fChain->SetBranchAddress("L0Electron,lowMultDecision", &L0Electron_lowMultDecision, &b_L0Electron_lowMultDecision);
   fChain->SetBranchAddress("L0Photon,lowMultDecision", &L0Photon_lowMultDecision, &b_L0Photon_lowMultDecision);
   fChain->SetBranchAddress("L0DiEM,lowMultDecision", &L0DiEM_lowMultDecision, &b_L0DiEM_lowMultDecision);
   fChain->SetBranchAddress("L0nSelections", &L0nSelections, &b_L0nSelections);
   fChain->SetBranchAddress("Hlt1NoPVPassThroughDecision", &Hlt1NoPVPassThroughDecision, &b_Hlt1NoPVPassThroughDecision);
   fChain->SetBranchAddress("Hlt1NoBiasNonBeamBeamDecision", &Hlt1NoBiasNonBeamBeamDecision, &b_Hlt1NoBiasNonBeamBeamDecision);
   fChain->SetBranchAddress("Hlt1MBNoBiasRateLimitedDecision", &Hlt1MBNoBiasRateLimitedDecision, &b_Hlt1MBNoBiasRateLimitedDecision);
   fChain->SetBranchAddress("Hlt1MBNoBiasDecision", &Hlt1MBNoBiasDecision, &b_Hlt1MBNoBiasDecision);
   fChain->SetBranchAddress("Hlt1CEPVeloCutDecision", &Hlt1CEPVeloCutDecision, &b_Hlt1CEPVeloCutDecision);
   fChain->SetBranchAddress("Hlt1LowMultMuonDecision", &Hlt1LowMultMuonDecision, &b_Hlt1LowMultMuonDecision);
   fChain->SetBranchAddress("Hlt1nSelections", &Hlt1nSelections, &b_Hlt1nSelections);
   fChain->SetBranchAddress("Hlt2NoBiasNonBeamBeamDecision", &Hlt2NoBiasNonBeamBeamDecision, &b_Hlt2NoBiasNonBeamBeamDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHDecision", &Hlt2LowMultLMR2HHDecision, &b_Hlt2LowMultLMR2HHDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHWSDecision", &Hlt2LowMultLMR2HHWSDecision, &b_Hlt2LowMultLMR2HHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHHHDecision", &Hlt2LowMultLMR2HHHHDecision, &b_Hlt2LowMultLMR2HHHHDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHHHWSDecision", &Hlt2LowMultLMR2HHHHWSDecision, &b_Hlt2LowMultLMR2HHHHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultL2pPiDecision", &Hlt2LowMultL2pPiDecision, &b_Hlt2LowMultL2pPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiDecision", &Hlt2LowMultD2KPiDecision, &b_Hlt2LowMultD2KPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiPiDecision", &Hlt2LowMultD2KPiPiDecision, &b_Hlt2LowMultD2KPiPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KKPiDecision", &Hlt2LowMultD2KKPiDecision, &b_Hlt2LowMultD2KKPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2K3PiDecision", &Hlt2LowMultD2K3PiDecision, &b_Hlt2LowMultD2K3PiDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHDecision", &Hlt2LowMultChiC2HHDecision, &b_Hlt2LowMultChiC2HHDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHHHDecision", &Hlt2LowMultChiC2HHHHDecision, &b_Hlt2LowMultChiC2HHHHDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2PPDecision", &Hlt2LowMultChiC2PPDecision, &b_Hlt2LowMultChiC2PPDecision);
   fChain->SetBranchAddress("Hlt2LowMultHadron_noTrFiltDecision", &Hlt2LowMultHadron_noTrFiltDecision, &b_Hlt2LowMultHadron_noTrFiltDecision);
   fChain->SetBranchAddress("Hlt2LowMultL2pPiWSDecision", &Hlt2LowMultL2pPiWSDecision, &b_Hlt2LowMultL2pPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiWSDecision", &Hlt2LowMultD2KPiWSDecision, &b_Hlt2LowMultD2KPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiPiWSDecision", &Hlt2LowMultD2KPiPiWSDecision, &b_Hlt2LowMultD2KPiPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KKPiWSDecision", &Hlt2LowMultD2KKPiWSDecision, &b_Hlt2LowMultD2KKPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2K3PiWSDecision", &Hlt2LowMultD2K3PiWSDecision, &b_Hlt2LowMultD2K3PiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHWSDecision", &Hlt2LowMultChiC2HHWSDecision, &b_Hlt2LowMultChiC2HHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHHHWSDecision", &Hlt2LowMultChiC2HHHHWSDecision, &b_Hlt2LowMultChiC2HHHHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2PPWSDecision", &Hlt2LowMultChiC2PPWSDecision, &b_Hlt2LowMultChiC2PPWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultMuonDecision", &Hlt2LowMultMuonDecision, &b_Hlt2LowMultMuonDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiMuonDecision", &Hlt2LowMultDiMuonDecision, &b_Hlt2LowMultDiMuonDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiMuon_PSDecision", &Hlt2LowMultDiMuon_PSDecision, &b_Hlt2LowMultDiMuon_PSDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiPhotonDecision", &Hlt2LowMultDiPhotonDecision, &b_Hlt2LowMultDiPhotonDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiPhoton_HighMassDecision", &Hlt2LowMultDiPhoton_HighMassDecision, &b_Hlt2LowMultDiPhoton_HighMassDecision);
   fChain->SetBranchAddress("Hlt2LowMultpi0Decision", &Hlt2LowMultpi0Decision, &b_Hlt2LowMultpi0Decision);
   fChain->SetBranchAddress("Hlt2LowMultDiElectronDecision", &Hlt2LowMultDiElectronDecision, &b_Hlt2LowMultDiElectronDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiElectron_noTrFiltDecision", &Hlt2LowMultDiElectron_noTrFiltDecision, &b_Hlt2LowMultDiElectron_noTrFiltDecision);
   fChain->SetBranchAddress("Hlt2MBMicroBiasVeloDecision", &Hlt2MBMicroBiasVeloDecision, &b_Hlt2MBMicroBiasVeloDecision);
   fChain->SetBranchAddress("Hlt2LowMultTechnical_MinBiasDecision", &Hlt2LowMultTechnical_MinBiasDecision, &b_Hlt2LowMultTechnical_MinBiasDecision);
   fChain->SetBranchAddress("Hlt2nSelections", &Hlt2nSelections, &b_Hlt2nSelections);
   fChain->SetBranchAddress("MaxRoutingBits", &MaxRoutingBits, &b_MaxRoutingBits);
   fChain->SetBranchAddress("RoutingBits", RoutingBits, &b_RoutingBits);
   fChain->SetBranchAddress("StrippingMBNoBiasDecision", &StrippingMBNoBiasDecision, &b_StrippingMBNoBiasDecision);
   fChain->SetBranchAddress("StrippingHlt1NoBiasNonBeamBeamLineDecision", &StrippingHlt1NoBiasNonBeamBeamLineDecision, &b_StrippingHlt1NoBiasNonBeamBeamLineDecision);
   fChain->SetBranchAddress("StrippingLowMultNonBeamBeamNoBiasLineDecision", &StrippingLowMultNonBeamBeamNoBiasLineDecision, &b_StrippingLowMultNonBeamBeamNoBiasLineDecision);
   fChain->SetBranchAddress("StrippingLowMultMuonLineDecision", &StrippingLowMultMuonLineDecision, &b_StrippingLowMultMuonLineDecision);
   fChain->SetBranchAddress("StrippingLowMultDiMuonLineDecision", &StrippingLowMultDiMuonLineDecision, &b_StrippingLowMultDiMuonLineDecision);
   fChain->SetBranchAddress("ET_prev1", &ET_prev1, &b_ET_prev1);
   fChain->SetBranchAddress("ET_prev2", &ET_prev2, &b_ET_prev2);
   fChain->SetBranchAddress("nouttrck", &nouttrck, &b_nouttrck);
   fChain->SetBranchAddress("trck_key", trck_key, &b_trck_key);
   fChain->SetBranchAddress("trck_type", trck_type, &b_trck_type);
   fChain->SetBranchAddress("trck_posx", trck_posx, &b_trck_posx);
   fChain->SetBranchAddress("trck_posy", trck_posy, &b_trck_posy);
   fChain->SetBranchAddress("trck_posz", trck_posz, &b_trck_posz);
   fChain->SetBranchAddress("trck_px", trck_px, &b_trck_px);
   fChain->SetBranchAddress("trck_py", trck_py, &b_trck_py);
   fChain->SetBranchAddress("trck_pz", trck_pz, &b_trck_pz);
   fChain->SetBranchAddress("trck_charge", trck_charge, &b_trck_charge);
   fChain->SetBranchAddress("trck_muonid", trck_muonid, &b_trck_muonid);
   fChain->SetBranchAddress("trck_riche", trck_riche, &b_trck_riche);
   fChain->SetBranchAddress("trck_richk", trck_richk, &b_trck_richk);
   fChain->SetBranchAddress("trck_richp", trck_richp, &b_trck_richp);
   fChain->SetBranchAddress("trck_muonll", trck_muonll, &b_trck_muonll);
   fChain->SetBranchAddress("trck_muacc", trck_muacc, &b_trck_muacc);
   fChain->SetBranchAddress("trck_ecal", trck_ecal, &b_trck_ecal);
   fChain->SetBranchAddress("trck_hcal", trck_hcal, &b_trck_hcal);
   fChain->SetBranchAddress("trck_eop", trck_eop, &b_trck_eop);
   fChain->SetBranchAddress("trck_probe", trck_probe, &b_trck_probe);
   fChain->SetBranchAddress("trck_probmu", trck_probmu, &b_trck_probmu);
   fChain->SetBranchAddress("trck_probpi", trck_probpi, &b_trck_probpi);
   fChain->SetBranchAddress("trck_probk", trck_probk, &b_trck_probk);
   fChain->SetBranchAddress("trck_probp", trck_probp, &b_trck_probp);
   fChain->SetBranchAddress("trck_probghost", trck_probghost, &b_trck_probghost);
   fChain->SetBranchAddress("noutmuon", &noutmuon, &b_noutmuon);
   fChain->SetBranchAddress("muon_chi2ndf", muon_chi2ndf, &b_muon_chi2ndf);
   fChain->SetBranchAddress("muon_posx", muon_posx, &b_muon_posx);
   fChain->SetBranchAddress("muon_posy", muon_posy, &b_muon_posy);
   fChain->SetBranchAddress("muon_ux", muon_ux, &b_muon_ux);
   fChain->SetBranchAddress("muon_uy", muon_uy, &b_muon_uy);
   fChain->SetBranchAddress("muon_nshared", muon_nshared, &b_muon_nshared);
   fChain->SetBranchAddress("nphotons", &nphotons, &b_nphotons);
   fChain->SetBranchAddress("noutphot", &noutphot, &b_noutphot);
   fChain->SetBranchAddress("photon_px", photon_px, &b_photon_px);
   fChain->SetBranchAddress("photon_py", photon_py, &b_photon_py);
   fChain->SetBranchAddress("photon_pz", photon_pz, &b_photon_pz);
   fChain->SetBranchAddress("photon_nspd", photon_nspd, &b_photon_nspd);
   fChain->SetBranchAddress("photon_nprs", photon_nprs, &b_photon_nprs);
   fChain->SetBranchAddress("noutspd", &noutspd, &b_noutspd);
   fChain->SetBranchAddress("spd_posx", spd_posx, &b_spd_posx);
   fChain->SetBranchAddress("spd_posy", spd_posy, &b_spd_posy);
   fChain->SetBranchAddress("noutprs", &noutprs, &b_noutprs);
   fChain->SetBranchAddress("prs_posx", prs_posx, &b_prs_posx);
   fChain->SetBranchAddress("prs_posy", prs_posy, &b_prs_posy);
   fChain->SetBranchAddress("noutl0mu", &noutl0mu, &b_noutl0mu);
   fChain->SetBranchAddress("l0mu_pt", l0mu_pt, &b_l0mu_pt);
   fChain->SetBranchAddress("l0mu_phi", l0mu_phi, &b_l0mu_phi);
   fChain->SetBranchAddress("l0mu_theta", l0mu_theta, &b_l0mu_theta);
   fChain->SetBranchAddress("noutl0h", &noutl0h, &b_noutl0h);
   fChain->SetBranchAddress("l0h_et", l0h_et, &b_l0h_et);
   Notify();
}

Bool_t run_Ap_mb::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void run_Ap_mb::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t run_Ap_mb::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef run_Ap_mb_cxx

void get_herschel(void)
{
  Int_t chan_num=0;

  Herschel* herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->B00);

  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->B01);
  
  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->B02);

  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->B03);

  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->B10);

  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->B11);
  
  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->B12);

  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->B13);

  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->B20);

  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->B21);
  
  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->B22);

  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->B23);

  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->F10);

  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->F11);
  
  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->F12);

  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->F13);

  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->F20);

  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->F21);
  
  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->F22);

  herschel=event->AddHer();//number of herschel channels is automatically implemented
  herschel->SetChannel(chan_num++);
  herschel->SetADC(tin->F23);

  //process hershel: get newly calibrated herschel FOM
  get_herschel_fom();

  return;
}

void init_herschel(const char * indir, const char * pA)
{
  char file[60];
  FILE* fp;
  char tmp1[40],tmp2[40],tmp3[40],tmp4[40],tmp5[40],tmp6[40];

  //Getting pedestals
  sprintf(file,"%s/herschel_calibration/pedestals/pedestals_%s_commonskew.txt",indir,pA);
  fp=fopen(file,"r");
  if(!fp)
    {
      printf("error opening file %s \n",file);
      exit(1);
    }
  for(Int_t i=0;i<40;i++)
    {
      fscanf(fp,"%s %s %s %s %s %s ",tmp1,tmp2,tmp3,tmp4,tmp5,tmp6);
      ped[i][0]=atof(tmp1);
      ped[i][1]=atof(tmp2);
      ped[i][2]=atof(tmp3);
      ped[i][3]=atof(tmp4);
      ped[i][4]=atof(tmp5);
      ped[i][5]=atof(tmp6);
    }
  fclose(fp);

  //Get correlation coefficients 
  //even bunch id
  sprintf(file,"%s/herschel_calibration/correlations/cal_corrcoef_%s_ev_common.txt",indir,pA);
  fp=fopen(file,"r");
  if(!fp)
    {
      printf("error opening file %s \n",file);
      exit(1);
    }
  for(Int_t i=0;i<20;i++)
    for(Int_t j=i;j<20;j++)
      {
        fscanf(fp,"%s ",tmp1);
        if(i==j)
          Sev(i,j)=1.0;
        else
          Sev(i,j)=atof(tmp1);
      }
  fclose(fp);

  //Get correlation coefficients 
  //odd bunch id
  sprintf(file,"%s/herschel_calibration/correlations/cal_corrcoef_%s_odd_common.txt",indir,pA);
  fp=fopen(file,"r");
  if(!fp)
    {
      printf("error opening file %s \n",file);
      exit(1);
    }
  for(Int_t i=0;i<20;i++)
    for(Int_t j=i;j<20;j++)
      {
        fscanf(fp,"%s ",tmp1);
        if(i==j)
          Sodd(i,j)=1.0;
        else
          Sodd(i,j)=atof(tmp1);
      }
  fclose(fp);

  Sev.Invert();
  Sodd.Invert();
}

void get_herschel_fom(void)
{
  TVectorD X(20);//full fom
  TVectorD X_part(20);//fom for backward or forward panels

  for(Int_t i=0;i<20;i++)
    X(i)=0;
  for(Int_t i=0;i<event->GetNHer();i++)
    {
      Herschel* herschel=event->GetHer(i);
      X(i)=get_chi(i,herschel->GetADC());
    }
  TVectorD v = X;
  if((event->GetBunchId()%2)==0)
    v *= Sev;
  else
    v *= Sodd;
  Double_t d = v*X;

  //Get implemented FOM variables
  event->SetFOM(log(d));

  //Get fom for backward pannels
  for(Int_t i=0;i<12;i++)
    X_part(i)=X(i);
  for(Int_t i=12;i<20;i++)
    X_part(i)=0;
  v = X_part;
  if((event->GetBunchId()%2)==0)
    v *= Sev;
  else
    v *= Sodd;
  d = v*X;
  event->SetFOMB(log(d));

  //Get fom for forward pannels
  for(Int_t i=0;i<12;i++)
    X_part(i)=0;
  for(Int_t i=12;i<20;i++)
    X_part(i)=X(i);
  v = X_part;
  if((event->GetBunchId()%2)==0)
    v *= Sev;
  else
    v *= Sodd;
  d = v*X;
  event->SetFOMF(log(d));
}

Double_t get_chi(Int_t i, Double_t adc)
{
  Double_t chi=-999;

  Int_t a=(int)i/4;
  Int_t mod=i%4;
  Int_t ind=a*8+mod;

  if((event->GetBunchId()%2)==1)
    ind+=4;

  if(adc<ped[ind][0])
    chi=(adc-ped[ind][0])/(ped[ind][1]*(1+ped[ind][2]*adc));
  else
    chi=(adc-ped[ind][0])/(ped[ind][1]);

  return chi;
}

// as things are written I am assuming the Ap configuration
#include "../importMe.C"
#include "../importMe.h"
using namespace std;

void loop_mc()
{
	// set gStyle options
	gStyle->SetOptStat(10);
	gStyle->SetOptFit(1);

	// create output file
	TString outputName = "ntuple_truth.root";
	cout << "creating output file " << outputName << "." <<  endl;
	auto outputFile = new TFile(outputName,"RECREATE");

    	// set corrections
    	auto useAccCor = true;

	// create output ntuple oject
	float ntuple[38];
 	auto nt = new TNtuple("nt","nt","trck1_type:trck2_type:trck1_charge:trck2_charge:trck1_posx:trck2_posx:trck1_posy:trck2_posy:trck1_posz:trck2_posz:trck1_px:trck2_px:trck1_py:trck2_py:trck1_pz:trck2_pz:trck1_eta:trck2_eta:trck1_richk:trck2_richk:trck1_riche:trck2_riche:runNumber:ngamma:her:nouttrck:mass:phiMass:pt2:y:phiY:theta:thetaX:W:WX:vchi:xv:yv");

	// open signal mc file
	TString inputName = "../rhoMC/rhomc.root";
	cout << "opening input file " << inputName << "." << endl;
	auto inputFile  = new TFile(inputName,"READ");
	TChain *tree = new TChain("T");
	tree->Add("../rhoMC/rhomc.root");
	tree->Add("../rhoMC/rhomc.root/T;3");
	Float_t trupx[50], trupy[50], trupz[50], pidk[50], pide[50], tx[50], ty[50], tz[50];
	Int_t nout, tt[50], charge[50], truq[50], nphotons, run, herchan;
	tree->SetBranchAddress("nout",&nout);
	tree->SetBranchAddress("tt",tt);
	tree->SetBranchAddress("tx",tx);
	tree->SetBranchAddress("ty",ty);
	tree->SetBranchAddress("tz",tz);
	tree->SetBranchAddress("trupx",trupx);
	tree->SetBranchAddress("trupy",trupy);
	tree->SetBranchAddress("trupz",trupz);
	tree->SetBranchAddress("truq",truq);
	tree->SetBranchAddress("pidk",pidk);
	tree->SetBranchAddress("pide",pide);
	tree->SetBranchAddress("charge",charge);
	tree->SetBranchAddress("herchan",&herchan);
	tree->SetBranchAddress("nphotons",&nphotons);
	tree->SetBranchAddress("run",&run);

	//// initialise hists
	// no Cuts
	auto noCuts_yHist_truth = new TH1F("noCuts_yHist_truth","",6,2,5);
	auto noCuts_histList = new TList();
	auto *noCuts_3DHist_truth = new TH3F("noCuts_3DHist_truth", "",massHist_numBins,lowerMassRange,upperMassRange,pt2Hist_numBins,lowerPt2Range,upperPt2Range,cosThetaHist_numBins,lowerCosThetaRange,upperCosThetaRange);

	for(Int_t i = 0;i < 6;i++)
	{
		auto *name3D = new Char_t[20];
		sprInt_tf(name3D,"noCuts_hist3D_truth_byY_%d",i);
		auto *h3D = new TH3F("temp","",massHist_numBins,lowerMassRange,upperMassRange,pt2Hist_numBins,lowerPt2Range,upperPt2Range,cosThetaHist_numBins,lowerCosThetaRange,upperCosThetaRange);
		h3D->SetName(name3D);
		noCuts_histList->Add(h3D);
	}
	// post acceptance
	auto postAC_yHist_truth = new TH1F("postAC_yHist_truth","",6,2,5);
	auto postAC_histList = new TList();
	auto postAC_3DHist_truth = new TH3F("postAC_3DHist_truth", "",massHist_numBins,lowerMassRange,upperMassRange,pt2Hist_numBins,lowerPt2Range,upperPt2Range,cosThetaHist_numBins,lowerCosThetaRange,upperCosThetaRange);
	for(Int_t i = 0;i < 6;i++)
	{
		auto *name3D = new Char_t[20];
		sprInt_tf(name3D,"postAC_hist3D_truth_byY_%d",i);
		auto *h3D = new TH3F("temp","",massHist_numBins,lowerMassRange,upperMassRange,pt2Hist_numBins,lowerPt2Range,upperPt2Range,cosThetaHist_numBins,lowerCosThetaRange,upperCosThetaRange);
		h3D->SetName(name3D);
		postAC_histList->Add(h3D);
	}

    	// get the acceptance corrections hists
    	auto *accptFile = new TFile("../acceptance/Ap/accpt.root");
    	auto *accpt =  (TH3F*)(accptFile->Get("accpt"));
    	auto *accpt_byY_0 = (TH3F*)(accptFile->Get("accpt_byY_0"));
    	auto *accpt_byY_1 = (TH3F*)(accptFile->Get("accpt_byY_1"));
    	auto *accpt_byY_2 = (TH3F*)(accptFile->Get("accpt_byY_2"));
    	auto *accpt_byY_3 = (TH3F*)(accptFile->Get("accpt_byY_3"));
    	auto *accpt_byY_4 = (TH3F*)(accptFile->Get("accpt_byY_4"));
    	auto *accpt_byY_5 = (TH3F*)(accptFile->Get("accpt_byY_5"));

 	auto accpt_histList = new TList();
	for(Int_t i = 0;i < 6;i++)
	{
		auto *name3D = new Char_t[20];
		sprInt_tf(name3D,"accpt_byY_%d",i);
		auto *accptTemp = ((TH3F*)(accptFile->Get(name3D)));
		accptTemp->SetName(name3D);
		accpt_histList->Add(accptTemp);
	}

	//// event loop
	auto numEvents = tree->GetEntries();
    	//auto numEvents = 100;
	cout << "Running over " << numEvents << " events." << endl;
    	auto count = 0;

    	for(auto i = 0; i < numEvents; i++)
	{
		// prInt_t event count
        	if((count % 100000) == 0) cout << count << endl;
        
        	// increment event counter
        	count += 1;
		
        	// retrieve event
        	tree->GetEntry(i);

		// set weight to one by default
            	auto weight = 1.;
		
    		// create four vectors for the pions (truth)
    		TLorentzVector pionFourVector_truth[2];
		pionFourVector_truth[0].SetXYZM(trupx[0],trupy[0],trupz[0],pionMass);
		pionFourVector_truth[1].SetXYZM(trupx[1],trupy[1],trupz[1],pionMass);
			
		// check if tracks are reconstructible
		auto trck1_isLong = isLong(pionFourVector_truth[0],truq[0],bmag);
		auto trck2_isLong = isLong(pionFourVector_truth[1],truq[1],bmag);

    		// create a four vector for the rho (truth)
		TLorentzVector rhoFourVector_truth;
		rhoFourVector_truth = pionFourVector_truth[0] + pionFourVector_truth[1];

		// find the pions' pseudorapidities (truth)
    		auto pionEta1_truth = pionFourVector_truth[0].PseudoRapidity();
    		auto pionEta2_truth = pionFourVector_truth[1].PseudoRapidity();

    		// find the pions' pts (truth)
    		auto pionPt1_truth = pionFourVector_truth[0].Pt();
    		auto pionPt2_truth = pionFourVector_truth[1].Pt();

		// calc rho kinematic variables
		auto y_truth = rhoFourVector_truth.Rapidity();
		auto mass_truth = rhoFourVector_truth.M();
		auto pt2_truth = rhoFourVector_truth.Pt() * rhoFourVector_truth.Pt();
		pt2_truth /= (1000*1000);
	 
		// calc PV
		Double_t vchi_truth, xv_truth, yv_truth, zv_truth;
		vert(tx[0],ty[0],tz[0],trupx[0],trupy[0],trupz[0],tx[1],ty[1],tz[1],trupx[1],trupy[1],trupz[1],xv_truth,yv_truth,zv_truth,vchi_truth);

		// get the theta variable
		auto theta_truth = gammaP_theta(rhoFourVector_truth,pionFourVector_truth[0],pionFourVector_truth[1],truq[0],truq[1]);
		auto cos_truth = cos(theta_truth);
		auto thetaX_truth = gammaP_theta(rhoFourVector_truth,pionFourVector_truth[0],pionFourVector_truth[1],truq[0],truq[1],thetaV,thetaH);
		// get the W variable
		auto W_truth = gammaP_W(rhoFourVector_truth);
		auto WX_truth = gammaP_W(rhoFourVector_truth,thetaV,thetaH);

		// fill ntuple
		ntuple[0] = tt[0];
		ntuple[1] = tt[1];
		ntuple[2] = truq[0];
		ntuple[3] = truq[1];
		ntuple[4] = tx[0];
		ntuple[5] = tx[1];
		ntuple[6] = ty[0];
		ntuple[7] = ty[1];
		ntuple[8] = tz[0];
		ntuple[9] = tz[1];
		ntuple[10] = trupx[0];
		ntuple[11] = trupx[1];
		ntuple[12] = trupy[0];
		ntuple[13] = trupy[1];
		ntuple[14] = trupz[0];
		ntuple[15] = trupz[1];
		ntuple[16] = pionEta1_truth;
		ntuple[17] = pionEta2_truth;
		ntuple[18] = pidk[0];
		ntuple[19] = pidk[1];
		ntuple[20] = pide[0];
		ntuple[21] = pide[1];
		ntuple[22] = run;
		ntuple[23] = 0; //nphotons;
		ntuple[24] = herchan;
		ntuple[25] = nout;
		ntuple[26] = mass_truth;
		ntuple[27] = mass_truth; // dummy used for the phi mass
		ntuple[28] = pt2_truth;
		ntuple[29] = y_truth;
		ntuple[30] = y_truth; // dummy used for phi y
		ntuple[31] = theta_truth;
		ntuple[32] = thetaX_truth;
		ntuple[33] = W_truth;
		ntuple[34] = WX_truth;
		ntuple[35] = vchi_truth;
		ntuple[36] = xv_truth;
		ntuple[37] = yv_truth;

		// truth cut on the nominal detector acceptance
    		if((2 < y_truth) && (y_truth < 5) && \
		(2 < pionEta1_truth) && (pionEta1_truth < 5) && \
		(2 < pionEta2_truth) && (pionEta2_truth < 5) && \
		(minTrackPt < pionPt1_truth) && (minTrackPt < pionPt2_truth) && \
		(trck1_isLong == true) && (trck2_isLong == true))
		{
			// fill ntuple
			nt->Fill(ntuple);
		}

		// rho mass cut
		if((mass_truth < lowerMassRange) || (upperMassRange < mass_truth)) continue;

		// rho pt2 cut
		if((pt2_truth < lowerPt2Range) || (upperPt2Range < pt2_truth)) continue;

		// rho cos(theta) cut
		if(((2 < y_truth) && (y_truth < 2.5)) || ((4.5 < y_truth) && (y_truth < 5)))
		{
			if(fabs(cos_truth) >= 0.4) continue;
		}
		else
		{
			if(fabs(cos_truth) >= 0.8) continue;
		}
		
		// fill truth hists (no cuts)
		noCuts_yHist_truth->Fill(y_truth);
		noCuts_3DHist_truth->Fill(mass_truth,pt2_truth,cos_truth);
		if ((2. < y_truth) && (y_truth < 2.5)) ((TH3F*)(noCuts_histList->At(0)))->Fill(mass_truth,pt2_truth,cos_truth);
		else if((2.5 < y_truth) && (y_truth < 3.)) ((TH3F*)(noCuts_histList->At(1)))->Fill(mass_truth,pt2_truth,cos_truth);
		else if((3. < y_truth) && (y_truth < 3.5)) ((TH3F*)(noCuts_histList->At(2)))->Fill(mass_truth,pt2_truth,cos_truth);
		else if((3.5 < y_truth) && (y_truth < 4.)) ((TH3F*)(noCuts_histList->At(3)))->Fill(mass_truth,pt2_truth,cos_truth);
		else if((4. < y_truth) && (y_truth < 4.5)) ((TH3F*)(noCuts_histList->At(4)))->Fill(mass_truth,pt2_truth,cos_truth);
		else if((4.5 < y_truth) && (y_truth < 5.)) ((TH3F*)(noCuts_histList->At(5)))->Fill(mass_truth,pt2_truth,cos_truth);
		    		

		// truth cut on the nominal detector acceptance
    		if((2 < y_truth) && (y_truth < 5) && \
		(2 < pionEta1_truth) && (pionEta1_truth < 5) && \
		(2 < pionEta2_truth) && (pionEta2_truth < 5) && \
		(minTrackPt < pionPt1_truth) && (minTrackPt < pionPt2_truth) && \
		(trck1_isLong == true) && (trck2_isLong == true))
		{
			// get the acceptance correction
	    		Double_t acc;
            		if((2. < y_truth) && (y_truth < 2.5)) acc = accpt_byY_0->GetBinContent(accpt_byY_0->FindBin(mass_truth,pt2_truth,cos_truth));
             		else if((2.5 < y_truth) && (y_truth < 3.)) acc = accpt_byY_1->GetBinContent(accpt_byY_1->FindBin(mass_truth,pt2_truth,cos_truth));
             		else if((3 < y_truth) && (y_truth < 3.5)) acc = accpt_byY_2->GetBinContent(accpt_byY_2->FindBin(mass_truth,pt2_truth,cos_truth));
             		else if((3.5 < y_truth) && (y_truth < 4.)) acc = accpt_byY_3->GetBinContent(accpt_byY_3->FindBin(mass_truth,pt2_truth,cos_truth));
             		else if((4. < y_truth) && (y_truth < 4.5)) acc = accpt_byY_4->GetBinContent(accpt_byY_4->FindBin(mass_truth,pt2_truth,cos_truth));
             		else if((4.5 < y_truth) && (y_truth < 5.))  acc = accpt_byY_5->GetBinContent(accpt_byY_5->FindBin(mass_truth,pt2_truth,cos_truth));
 
			// calculate the weight from the acceptance
            		if(useAccCor == true) weight = 1./acc;
           		checkWeight(i,weight);
			
			if(acc == 0.)
			{
				cout << "Here's a bug: acceptance = 0." << endl;
				cout << "Truth mass is: " << mass_truth << endl;
				cout << "Truth pt2 is: " << pt2_truth << endl;
				cout << "Truth cos(theta) is: " << cos_truth << endl << endl;;
			}
			
			// fill truth hists for events within acceptance
			postAC_yHist_truth->Fill(y_truth,weight);
			postAC_3DHist_truth->Fill(mass_truth,pt2_truth,cos_truth,weight);
			if((2. < y_truth) && (y_truth < 2.5)) ((TH3F*)(postAC_histList->At(0)))->Fill(mass_truth,pt2_truth,cos_truth,weight);
			else if((2.5 < y_truth) && (y_truth < 3.)) ((TH3F*)(postAC_histList->At(1)))->Fill(mass_truth,pt2_truth,cos_truth,weight);
			else if((3. < y_truth) && (y_truth < 3.5)) ((TH3F*)(postAC_histList->At(2)))->Fill(mass_truth,pt2_truth,cos_truth,weight);
			else if((3.5 < y_truth) && (y_truth < 4.)) ((TH3F*)(postAC_histList->At(3)))->Fill(mass_truth,pt2_truth,cos_truth,weight);
			else if((4. < y_truth) && (y_truth < 4.5)) ((TH3F*)(postAC_histList->At(4)))->Fill(mass_truth,pt2_truth,cos_truth,weight);
			else if((4.5 < y_truth) && (y_truth < 5.)) ((TH3F*)(postAC_histList->At(5)))->Fill(mass_truth,pt2_truth,cos_truth,weight);
		}
	
	}

	// plot truth rapidity
	auto yCanvas_truth = new TCanvas("yCanvas_truth","");
	noCuts_yHist_truth->SetMinimum(0);
	noCuts_yHist_truth->SetMaximum(60000);
	noCuts_yHist_truth->GetXaxis()->SetTitle("y[#pi^{+}#pi^{-}]");
	noCuts_yHist_truth->GetYaxis()->SetTitle("truth entries");
	noCuts_yHist_truth->SetMarkerColor(kRed);
	noCuts_yHist_truth->SetLineColor(kRed);
	noCuts_yHist_truth->Draw("E1");
	postAC_yHist_truth->GetXaxis()->SetTitle("y[#pi^{+}#pi^{-}]");
	postAC_yHist_truth->GetYaxis()->SetTitle("truth entries");
	postAC_yHist_truth->SetMarkerColor(kGreen);
	postAC_yHist_truth->SetLineColor(kGreen);
	postAC_yHist_truth->Draw("E1 SAME");
	auto yLegend_truth = new TLegend(0.725,0.15,1.,.35);
	yLegend_truth->AddEntry(noCuts_yHist_truth,"no cuts");
	yLegend_truth->AddEntry(postAC_yHist_truth,"in acc.");
	yLegend_truth->Draw("SAME");
	yCanvas_truth->SaveAs("yHist_truth.png");

	// plot truth mass
	auto massCanvas_truth = new TCanvas("massCanvas_truth","");
	auto noCuts_massHist_truth = (TH1F*)(noCuts_3DHist_truth->ProjectionX("noCuts_massHist_truth"));
	//noCuts_massHist_truth->SetMinimum(0);
	//noCuts_massHist_truth->SetMaximum(60000);
	noCuts_massHist_truth->GetXaxis()->SetTitle("mass[#pi^{+}#pi^{-}]");
	noCuts_massHist_truth->GetYaxis()->SetTitle("truth entries");
	noCuts_massHist_truth->SetMarkerColor(kRed);
	noCuts_massHist_truth->SetLineColor(kRed);
	noCuts_massHist_truth->Draw("E1");
	auto postAC_massHist_truth = (TH1F*)(noCuts_3DHist_truth->ProjectionX("postAC_massHist_truth"));
	postAC_massHist_truth->GetXaxis()->SetTitle("mass[#pi^{+}#pi^{-}]");
	postAC_massHist_truth->GetYaxis()->SetTitle("truth entries");
	postAC_massHist_truth->SetMarkerColor(kGreen);
	postAC_massHist_truth->SetLineColor(kGreen);
	postAC_massHist_truth->Draw("E1 SAME");
	auto massLegend_truth = new TLegend(0.725,0.15,1.,.35);
	massLegend_truth->AddEntry(noCuts_massHist_truth,"no cuts");
	massLegend_truth->AddEntry(postAC_massHist_truth,"in acc.");
	massLegend_truth->Draw("SAME");
	massCanvas_truth->SaveAs("massHist_truth.png");

	// plot truth pt2
	auto pt2Canvas_truth = new TCanvas("pt2Canvas_truth","");
	auto noCuts_pt2Hist_truth = (TH1F*)(noCuts_3DHist_truth->ProjectionY("noCuts_pt2Hist_truth"));
	//noCuts_pt2Hist_truth->SetMinimum(0);
	//noCuts_pt2Hist_truth->SetMaximum(60000);
	noCuts_pt2Hist_truth->GetXaxis()->SetTitle("p_{T}^{2} [GeV]^{2}");
	noCuts_pt2Hist_truth->GetYaxis()->SetTitle("truth entries");
	noCuts_pt2Hist_truth->SetMarkerColor(kRed);
	noCuts_pt2Hist_truth->SetLineColor(kRed);
	noCuts_pt2Hist_truth->Draw("E1");
	auto postAC_pt2Hist_truth = (TH1F*)(postAC_3DHist_truth->ProjectionY("postAC_massHist_truth"));
	postAC_pt2Hist_truth->GetXaxis()->SetTitle("p_{T}^{2} [GeV]^{2}");
	postAC_pt2Hist_truth->GetYaxis()->SetTitle("truth entries");
	postAC_pt2Hist_truth->SetMarkerColor(kGreen);
	postAC_pt2Hist_truth->SetLineColor(kGreen);
	postAC_pt2Hist_truth->Draw("E1 SAME");
	auto pt2Legend_truth = new TLegend(0.725,0.15,1.,.35);
	pt2Legend_truth->AddEntry(noCuts_pt2Hist_truth,"no cuts");
	pt2Legend_truth->AddEntry(postAC_pt2Hist_truth,"in acc.");
	pt2Legend_truth->Draw("SAME");
	pt2Canvas_truth->SaveAs("pt2Hist_truth.png");

	// plot truth cos(theta)
	auto cosCanvas_truth = new TCanvas("cosCanvas_truth","");
	auto noCuts_cosThetaHist_truth = (TH1F*)(noCuts_3DHist_truth->ProjectionZ("noCuts_cosThetaHist_truth"));
	noCuts_cosThetaHist_truth->SetMinimum(0);
	//noCuts_cosThetaHist_truth->SetMaximum(60000);
	noCuts_cosThetaHist_truth->GetXaxis()->SetTitle("cos(#theta_{CoM})");
	noCuts_cosThetaHist_truth->GetYaxis()->SetTitle("truth entries");
	noCuts_cosThetaHist_truth->SetMarkerColor(kRed);
	noCuts_cosThetaHist_truth->SetLineColor(kRed);
	noCuts_cosThetaHist_truth->Draw("E1");
	auto postAC_cosThetaHist_truth = (TH1F*)(postAC_3DHist_truth->ProjectionZ("postAC_cosThetaHist_truth"));
	postAC_cosThetaHist_truth->GetXaxis()->SetTitle("cos(#theta_{CoM}))");
	postAC_cosThetaHist_truth->GetYaxis()->SetTitle("truth entries");
	postAC_cosThetaHist_truth->SetMarkerColor(kGreen);
	postAC_cosThetaHist_truth->SetLineColor(kGreen);
	//postAC_cosThetaHist_truth->Draw("E1 SAME");
	auto cosLegend_truth = new TLegend(0.725,0.15,1.,.35);
	cosLegend_truth->AddEntry(noCuts_cosThetaHist_truth,"no cuts");
	cosLegend_truth->AddEntry(postAC_cosThetaHist_truth,"in acc.");
	cosLegend_truth->Draw("SAME");
	cosCanvas_truth->SaveAs("cosThetaHist_truth.png");

	// fit truth cos(theta) distribution
	auto fcos = new TF1("fcos","[0]*(1-[1]*x*x)",lowerCosThetaRange,upperCosThetaRange);
	noCuts_cosThetaHist_truth->Fit("fcos","","",-1,1);

	// initialise arrays
	Double_t yRange[6] = {2.25,2.75,3.25,3.75,4.25,4.75};
	Double_t yErr[12] = {0,0,0,0,0,0};
	Double_t fitRange[6] = {0.4,0.8,0.8,0.8,0.8,0.4};

	// declare lambda variable arrays
	Double_t lambdaNoCuts[6], lambdaNoCuts_err[6];
	Double_t lambdaPostAC[6], lambdaPostAC_err[6];

	// loop over rapidity bins
	auto cosThetaHist_truth_byY_canvas = new TCanvas("cosThetaHist_truth_byY_canvas","");
	cosThetaHist_truth_byY_canvas->Divide(3,2);
	for(Int_t i = 0;i<6;i++)
	{
		cout << "Fitting no cuts hist for bin " << i << endl;
		cosThetaHist_truth_byY_canvas->cd(i+1);
		TH1F *massHist = (TH1F*)(((TH3F*)(noCuts_histList->At(i)))->ProjectionX("mass"));
		TH1F *pt2Hist = (TH1F*)(((TH3F*)(noCuts_histList->At(i)))->ProjectionY("pt2"));
		TH1F *noCuts_h = (TH1F*)(((TH3F*)(noCuts_histList->At(i)))->ProjectionZ());
		noCuts_h->SetTitle("");
		noCuts_h->SetLineColor(kRed);
		noCuts_h->SetMarkerColor(kRed);
		noCuts_h->GetXaxis()->SetTitleSize(0.05);
		noCuts_h->GetYaxis()->SetTitleSize(0.05);
		noCuts_h->Draw("E1");
		TF1 *fcosTheta_noCuts = new TF1("fcosTheta_noCuts","[0] * (1 - [1]*x*x)",-1.,1.);
		fcosTheta_noCuts->SetParNames("N","lambda");
		noCuts_h->Fit("fcosTheta_noCuts","","",-fitRange[i],fitRange[i]);
		lambdaNoCuts[i] = fcosTheta_noCuts->GetParameter(1);
		lambdaNoCuts_err[i] = fcosTheta_noCuts->GetParError(1);
      
		cout << "Fitting post acceptance hist for bin " << i << endl;
		TH1F *massHist_postAC = (TH1F*)(((TH3F*)(postAC_histList->At(i)))->ProjectionX("mass"));
		TH1F *pt2Hist_postAC = (TH1F*)(((TH3F*)(postAC_histList->At(i)))->ProjectionY("pt2"));
		TH1F *postAC_h = (TH1F*)(((TH3F*)(postAC_histList->At(i)))->ProjectionZ());
		postAC_h->SetLineColor(kGreen);
		postAC_h->SetMarkerColor(kGreen);
		postAC_h->Draw("E1 SAME");
		TF1 *fcosTheta_postAC = new TF1("fcosTheta_postAC","[0] * (1 - [1]*x*x)",-1.,1.);
		fcosTheta_postAC->SetParNames("N","lambda");
		fcosTheta_postAC->SetLineColor(kGreen);
		postAC_h->Fit("fcosTheta_postAC","","",-fitRange[i],fitRange[i]);
		lambdaPostAC[i] = fcosTheta_postAC->GetParameter(1);
		lambdaPostAC_err[i] = fcosTheta_postAC->GetParError(1);
	}

	// plot the lambda values together to compare
	auto yMultiGraph_lambda = new TMultiGraph();

	auto *yGraph_lambdaNoCuts = new TGraphErrors(6,yRange,lambdaNoCuts,yErr,lambdaNoCuts_err);
	yGraph_lambdaNoCuts->SetLineColor(kRed);
	yGraph_lambdaNoCuts->SetMarkerColor(kRed);
	yGraph_lambdaNoCuts->SetMarkerStyle(20);
	yMultiGraph_lambda->Add(yGraph_lambdaNoCuts);

	auto *yGraph_lambdaPostAC = new TGraphErrors(6,yRange,lambdaPostAC,yErr,lambdaPostAC_err);
	yGraph_lambdaPostAC->SetLineColor(kGreen);
	yGraph_lambdaPostAC->SetMarkerColor(kGreen);
	yGraph_lambdaPostAC->SetMarkerStyle(20);
	yMultiGraph_lambda->Add(yGraph_lambdaPostAC);

	auto lambda_canvas = new TCanvas();
	yMultiGraph_lambda->GetXaxis()->SetTitle("y[#pi^{+}#pi^{-}]");
	yMultiGraph_lambda->GetXaxis()->SetTitleSize(0.05);
	yMultiGraph_lambda->GetYaxis()->SetTitle("#lambda");
	yMultiGraph_lambda->GetYaxis()->SetTitleSize(0.05);
	yMultiGraph_lambda->Draw("AP");
	auto lambdaLegend_truth = new TLegend(0.725,0.15,1.,.35);
	lambdaLegend_truth->AddEntry(yGraph_lambdaNoCuts,"no cuts");
	lambdaLegend_truth->AddEntry(yGraph_lambdaPostAC,"in acc.");
	lambdaLegend_truth->Draw("SAME");
	
	// write hists to output file
	cout << "Writing hists to file." << endl;
	outputFile->cd();
    	nt->Write();
	noCuts_histList->Write();
	postAC_histList->Write();

	// close files and finish up
	cout << "Closing files." << endl;
	//inputFile->Close();
	outputFile->Close();
	cout << "Files closed and we're done." << endl;
}

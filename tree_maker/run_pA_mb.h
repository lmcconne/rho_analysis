//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Oct 23 00:22:15 2022 by ROOT version 6.24/06
// from TChain TupleTools/MyTuple/
//////////////////////////////////////////////////////////

#ifndef run_pA_mb_h
#define run_pA_mb_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class run_pA_mb {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          BCID;
   Int_t           BCType;
   UInt_t          OdinTCK;
   UInt_t          L0DUTCK;
   UInt_t          HLT1TCK;
   UInt_t          HLT2TCK;
   ULong64_t       GpsTime;
   Short_t         Polarity;
   Int_t           nPVs;
   Int_t           nTracks;
   Int_t           nLongTracks;
   Int_t           nDownstreamTracks;
   Int_t           nUpstreamTracks;
   Int_t           nVeloTracks;
   Int_t           nTTracks;
   Int_t           nBackTracks;
   Int_t           nRich1Hits;
   Int_t           nRich2Hits;
   Int_t           nVeloClusters;
   Int_t           nITClusters;
   Int_t           nTTClusters;
   Int_t           nOTClusters;
   Int_t           nSPDHits;
   Int_t           nMuonCoordsS0;
   Int_t           nMuonCoordsS1;
   Int_t           nMuonCoordsS2;
   Int_t           nMuonCoordsS3;
   Int_t           nMuonCoordsS4;
   Int_t           nMuonTracks;
   Int_t           B00;
   Int_t           B01;
   Int_t           B02;
   Int_t           B03;
   Int_t           B10;
   Int_t           B11;
   Int_t           B12;
   Int_t           B13;
   Int_t           B20;
   Int_t           B21;
   Int_t           B22;
   Int_t           B23;
   Int_t           F10;
   Int_t           F11;
   Int_t           F12;
   Int_t           F13;
   Int_t           F20;
   Int_t           F21;
   Int_t           F22;
   Int_t           F23;
   Double_t        log_hrc_fom_v3;
   Double_t        log_hrc_fom_B_v3;
   Double_t        log_hrc_fom_F_v3;
   Int_t           nchB;
   Float_t         adc_B[1000];   //[nchB]
   Int_t           nchF;
   Float_t         adc_F[1000];   //[nchF]
   Int_t           L0Global;
   UInt_t          Hlt1Global;
   UInt_t          Hlt2Global;
   Int_t           L0Muon_lowMultDecision;
   Int_t           L0DiMuon_lowMultDecision;
   Int_t           L0DiHadron_lowMultDecision;
   Int_t           L0Electron_lowMultDecision;
   Int_t           L0Photon_lowMultDecision;
   Int_t           L0DiEM_lowMultDecision;
   UInt_t          L0nSelections;
   Int_t           Hlt1NoPVPassThroughDecision;
   Int_t           Hlt1LowMultPassThroughDecision;
   Int_t           Hlt1NoBiasNonBeamBeamDecision;
   Int_t           Hlt1MBNoBiasRateLimitedDecision;
   Int_t           Hlt1MBNoBiasDecision;
   Int_t           Hlt1CEPVeloCutDecision;
   Int_t           Hlt1LowMultDecision;
   Int_t           Hlt1LowMultMuonDecision;
   Int_t           Hlt1LowMultHerschelDecision;
   Int_t           Hlt1LowMultVeloCut_LeptonsDecision;
   Int_t           Hlt1LowMultVeloAndHerschel_LeptonsDecision;
   UInt_t          Hlt1nSelections;
   Int_t           Hlt2NoBiasNonBeamBeamDecision;
   Int_t           Hlt2LowMultLMR2HHDecision;
   Int_t           Hlt2LowMultLMR2HHWSDecision;
   Int_t           Hlt2LowMultLMR2HHHHDecision;
   Int_t           Hlt2LowMultLMR2HHHHWSDecision;
   Int_t           Hlt2LowMultL2pPiDecision;
   Int_t           Hlt2LowMultD2KPiDecision;
   Int_t           Hlt2LowMultD2KPiPiDecision;
   Int_t           Hlt2LowMultD2KKPiDecision;
   Int_t           Hlt2LowMultD2K3PiDecision;
   Int_t           Hlt2LowMultChiC2HHDecision;
   Int_t           Hlt2LowMultChiC2HHHHDecision;
   Int_t           Hlt2LowMultChiC2PPDecision;
   Int_t           Hlt2LowMultHadron_noTrFiltDecision;
   Int_t           Hlt2LowMultL2pPiWSDecision;
   Int_t           Hlt2LowMultD2KPiWSDecision;
   Int_t           Hlt2LowMultD2KPiPiWSDecision;
   Int_t           Hlt2LowMultD2KKPiWSDecision;
   Int_t           Hlt2LowMultD2K3PiWSDecision;
   Int_t           Hlt2LowMultChiC2HHWSDecision;
   Int_t           Hlt2LowMultChiC2HHHHWSDecision;
   Int_t           Hlt2LowMultChiC2PPWSDecision;
   Int_t           Hlt2LowMultMuonDecision;
   Int_t           Hlt2LowMultDiMuonDecision;
   Int_t           Hlt2LowMultDiMuon_PSDecision;
   Int_t           Hlt2LowMultDiPhotonDecision;
   Int_t           Hlt2LowMultDiPhoton_HighMassDecision;
   Int_t           Hlt2LowMultpi0Decision;
   Int_t           Hlt2LowMultDiElectronDecision;
   Int_t           Hlt2LowMultDiElectron_noTrFiltDecision;
   Int_t           Hlt2MBMicroBiasVeloDecision;
   Int_t           Hlt2LowMultTechnical_MinBiasDecision;
   UInt_t          Hlt2nSelections;
   Int_t           MaxRoutingBits;
   Float_t         RoutingBits[64];   //[MaxRoutingBits]
   UInt_t          StrippingMBNoBiasDecision;
   UInt_t          StrippingHlt1NoBiasNonBeamBeamLineDecision;
   UInt_t          StrippingLowMultNonBeamBeamNoBiasLineDecision;
   UInt_t          StrippingLowMultMuonLineDecision;
   UInt_t          StrippingLowMultDiMuonLineDecision;
   Float_t         ET_prev1;
   Float_t         ET_prev2;
   Int_t           nouttrck;
   Float_t         trck_key[10];   //[nouttrck]
   Float_t         trck_type[10];   //[nouttrck]
   Float_t         trck_posx[10];   //[nouttrck]
   Float_t         trck_posy[10];   //[nouttrck]
   Float_t         trck_posz[10];   //[nouttrck]
   Float_t         trck_px[10];   //[nouttrck]
   Float_t         trck_py[10];   //[nouttrck]
   Float_t         trck_pz[10];   //[nouttrck]
   Float_t         trck_charge[10];   //[nouttrck]
   Float_t         trck_muonid[10];   //[nouttrck]
   Float_t         trck_riche[10];   //[nouttrck]
   Float_t         trck_richk[10];   //[nouttrck]
   Float_t         trck_richp[10];   //[nouttrck]
   Float_t         trck_muonll[10];   //[nouttrck]
   Float_t         trck_muacc[10];   //[nouttrck]
   Float_t         trck_ecal[10];   //[nouttrck]
   Float_t         trck_hcal[10];   //[nouttrck]
   Float_t         trck_eop[10];   //[nouttrck]
   Float_t         trck_probe[10];   //[nouttrck]
   Float_t         trck_probmu[10];   //[nouttrck]
   Float_t         trck_probpi[10];   //[nouttrck]
   Float_t         trck_probk[10];   //[nouttrck]
   Float_t         trck_probp[10];   //[nouttrck]
   Float_t         trck_probghost[10];   //[nouttrck]
   Int_t           noutmuon;
   Float_t         muon_chi2ndf[10];   //[noutmuon]
   Float_t         muon_posx[10];   //[noutmuon]
   Float_t         muon_posy[10];   //[noutmuon]
   Float_t         muon_ux[10];   //[noutmuon]
   Float_t         muon_uy[10];   //[noutmuon]
   Float_t         muon_nshared[10];   //[noutmuon]
   Int_t           nphotons;
   Int_t           noutphot;
   Float_t         photon_px[10];   //[noutphot]
   Float_t         photon_py[10];   //[noutphot]
   Float_t         photon_pz[10];   //[noutphot]
   Float_t         photon_nspd[10];   //[noutphot]
   Float_t         photon_nprs[10];   //[noutphot]
   Int_t           noutspd;
   Float_t         spd_posx[30];   //[noutspd]
   Float_t         spd_posy[30];   //[noutspd]
   Int_t           noutprs;
   Float_t         prs_posx[30];   //[noutprs]
   Float_t         prs_posy[30];   //[noutprs]
   Int_t           noutl0mu;
   Float_t         l0mu_pt[10];   //[noutl0mu]
   Float_t         l0mu_phi[10];   //[noutl0mu]
   Float_t         l0mu_theta[10];   //[noutl0mu]
   Int_t           noutl0h;
   Float_t         l0h_et[10];   //[noutl0h]

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_BCType;   //!
   TBranch        *b_OdinTCK;   //!
   TBranch        *b_L0DUTCK;   //!
   TBranch        *b_HLT1TCK;   //!
   TBranch        *b_HLT2TCK;   //!
   TBranch        *b_GpsTime;   //!
   TBranch        *b_Polarity;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nLongTracks;   //!
   TBranch        *b_nDownstreamTracks;   //!
   TBranch        *b_nUpstreamTracks;   //!
   TBranch        *b_nVeloTracks;   //!
   TBranch        *b_nTTracks;   //!
   TBranch        *b_nBackTracks;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nVeloClusters;   //!
   TBranch        *b_nITClusters;   //!
   TBranch        *b_nTTClusters;   //!
   TBranch        *b_nOTClusters;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_nMuonCoordsS0;   //!
   TBranch        *b_nMuonCoordsS1;   //!
   TBranch        *b_nMuonCoordsS2;   //!
   TBranch        *b_nMuonCoordsS3;   //!
   TBranch        *b_nMuonCoordsS4;   //!
   TBranch        *b_nMuonTracks;   //!
   TBranch        *b_B00;   //!
   TBranch        *b_B01;   //!
   TBranch        *b_B02;   //!
   TBranch        *b_B03;   //!
   TBranch        *b_B10;   //!
   TBranch        *b_B11;   //!
   TBranch        *b_B12;   //!
   TBranch        *b_B13;   //!
   TBranch        *b_B20;   //!
   TBranch        *b_B21;   //!
   TBranch        *b_B22;   //!
   TBranch        *b_B23;   //!
   TBranch        *b_F10;   //!
   TBranch        *b_F11;   //!
   TBranch        *b_F12;   //!
   TBranch        *b_F13;   //!
   TBranch        *b_F20;   //!
   TBranch        *b_F21;   //!
   TBranch        *b_F22;   //!
   TBranch        *b_F23;   //!
   TBranch        *b_log_hrc_fom_v3;   //!
   TBranch        *b_log_hrc_fom_B_v3;   //!
   TBranch        *b_log_hrc_fom_F_v3;   //!
   TBranch        *b_nchB;   //!
   TBranch        *b_adc_B;   //!
   TBranch        *b_nchF;   //!
   TBranch        *b_adc_F;   //!
   TBranch        *b_L0Global;   //!
   TBranch        *b_Hlt1Global;   //!
   TBranch        *b_Hlt2Global;   //!
   TBranch        *b_L0Muon_lowMultDecision;   //!
   TBranch        *b_L0DiMuon_lowMultDecision;   //!
   TBranch        *b_L0DiHadron_lowMultDecision;   //!
   TBranch        *b_L0Electron_lowMultDecision;   //!
   TBranch        *b_L0Photon_lowMultDecision;   //!
   TBranch        *b_L0DiEM_lowMultDecision;   //!
   TBranch        *b_L0nSelections;   //!
   TBranch        *b_Hlt1NoPVPassThroughDecision;   //!
   TBranch        *b_Hlt1LowMultPassThroughDecision;   //!
   TBranch        *b_Hlt1NoBiasNonBeamBeamDecision;   //!
   TBranch        *b_Hlt1MBNoBiasRateLimitedDecision;   //!
   TBranch        *b_Hlt1MBNoBiasDecision;   //!
   TBranch        *b_Hlt1CEPVeloCutDecision;   //!
   TBranch        *b_Hlt1LowMultDecision;   //!
   TBranch        *b_Hlt1LowMultMuonDecision;   //!
   TBranch        *b_Hlt1LowMultHerschelDecision;   //!
   TBranch        *b_Hlt1LowMultVeloCut_LeptonsDecision;   //!
   TBranch        *b_Hlt1LowMultVeloAndHerschel_LeptonsDecision;   //!
   TBranch        *b_Hlt1nSelections;   //!
   TBranch        *b_Hlt2NoBiasNonBeamBeamDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHWSDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHHHDecision;   //!
   TBranch        *b_Hlt2LowMultLMR2HHHHWSDecision;   //!
   TBranch        *b_Hlt2LowMultL2pPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2KKPiDecision;   //!
   TBranch        *b_Hlt2LowMultD2K3PiDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHHHDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2PPDecision;   //!
   TBranch        *b_Hlt2LowMultHadron_noTrFiltDecision;   //!
   TBranch        *b_Hlt2LowMultL2pPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2KPiPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2KKPiWSDecision;   //!
   TBranch        *b_Hlt2LowMultD2K3PiWSDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHWSDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2HHHHWSDecision;   //!
   TBranch        *b_Hlt2LowMultChiC2PPWSDecision;   //!
   TBranch        *b_Hlt2LowMultMuonDecision;   //!
   TBranch        *b_Hlt2LowMultDiMuonDecision;   //!
   TBranch        *b_Hlt2LowMultDiMuon_PSDecision;   //!
   TBranch        *b_Hlt2LowMultDiPhotonDecision;   //!
   TBranch        *b_Hlt2LowMultDiPhoton_HighMassDecision;   //!
   TBranch        *b_Hlt2LowMultpi0Decision;   //!
   TBranch        *b_Hlt2LowMultDiElectronDecision;   //!
   TBranch        *b_Hlt2LowMultDiElectron_noTrFiltDecision;   //!
   TBranch        *b_Hlt2MBMicroBiasVeloDecision;   //!
   TBranch        *b_Hlt2LowMultTechnical_MinBiasDecision;   //!
   TBranch        *b_Hlt2nSelections;   //!
   TBranch        *b_MaxRoutingBits;   //!
   TBranch        *b_RoutingBits;   //!
   TBranch        *b_StrippingMBNoBiasDecision;   //!
   TBranch        *b_StrippingHlt1NoBiasNonBeamBeamLineDecision;   //!
   TBranch        *b_StrippingLowMultNonBeamBeamNoBiasLineDecision;   //!
   TBranch        *b_StrippingLowMultMuonLineDecision;   //!
   TBranch        *b_StrippingLowMultDiMuonLineDecision;   //!
   TBranch        *b_ET_prev1;   //!
   TBranch        *b_ET_prev2;   //!
   TBranch        *b_nouttrck;   //!
   TBranch        *b_trck_key;   //!
   TBranch        *b_trck_type;   //!
   TBranch        *b_trck_posx;   //!
   TBranch        *b_trck_posy;   //!
   TBranch        *b_trck_posz;   //!
   TBranch        *b_trck_px;   //!
   TBranch        *b_trck_py;   //!
   TBranch        *b_trck_pz;   //!
   TBranch        *b_trck_charge;   //!
   TBranch        *b_trck_muonid;   //!
   TBranch        *b_trck_riche;   //!
   TBranch        *b_trck_richk;   //!
   TBranch        *b_trck_richp;   //!
   TBranch        *b_trck_muonll;   //!
   TBranch        *b_trck_muacc;   //!
   TBranch        *b_trck_ecal;   //!
   TBranch        *b_trck_hcal;   //!
   TBranch        *b_trck_eop;   //!
   TBranch        *b_trck_probe;   //!
   TBranch        *b_trck_probmu;   //!
   TBranch        *b_trck_probpi;   //!
   TBranch        *b_trck_probk;   //!
   TBranch        *b_trck_probp;   //!
   TBranch        *b_trck_probghost;   //!
   TBranch        *b_noutmuon;   //!
   TBranch        *b_muon_chi2ndf;   //!
   TBranch        *b_muon_posx;   //!
   TBranch        *b_muon_posy;   //!
   TBranch        *b_muon_ux;   //!
   TBranch        *b_muon_uy;   //!
   TBranch        *b_muon_nshared;   //!
   TBranch        *b_nphotons;   //!
   TBranch        *b_noutphot;   //!
   TBranch        *b_photon_px;   //!
   TBranch        *b_photon_py;   //!
   TBranch        *b_photon_pz;   //!
   TBranch        *b_photon_nspd;   //!
   TBranch        *b_photon_nprs;   //!
   TBranch        *b_noutspd;   //!
   TBranch        *b_spd_posx;   //!
   TBranch        *b_spd_posy;   //!
   TBranch        *b_noutprs;   //!
   TBranch        *b_prs_posx;   //!
   TBranch        *b_prs_posy;   //!
   TBranch        *b_noutl0mu;   //!
   TBranch        *b_l0mu_pt;   //!
   TBranch        *b_l0mu_phi;   //!
   TBranch        *b_l0mu_theta;   //!
   TBranch        *b_noutl0h;   //!
   TBranch        *b_l0h_et;   //!

   run_pA_mb(TTree *tree=0);
   virtual ~run_pA_mb();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef run_pA_mb_cxx
run_pA_mb::run_pA_mb(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      f->GetObject("TupleTools/MyTuple",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
      TChain * chain = new TChain("TupleTools/MyTuple","");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962141.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962147.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962151.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962154.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962159.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962161.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962163.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962169.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962174.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962177.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962180.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962184.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962187.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962190.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962193.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962196.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962199.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962202.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962203.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962205.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962208.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962210.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962213.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962216.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962217.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962220.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962223.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962225.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962228.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962230.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962233.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962235.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962239.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962241.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962244.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962246.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962249.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962252.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962255.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962257.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962261.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962262.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962263.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962266.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962269.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962270.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962271.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962273.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962277.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962280.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962283.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962286.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962289.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962293.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962299.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962305.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962309.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962314.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962318.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962323.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962326.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962331.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962336.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962341.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962345.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962349.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962354.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962357.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962364.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962368.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962374.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962377.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962379.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962382.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962384.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962387.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962390.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962393.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962395.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962398.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962401.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962404.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962407.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962410.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962413.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962416.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962419.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962422.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962425.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962428.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962434.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962437.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962439.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962447.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962450.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962454.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962456.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962459.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962477.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962481.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962482.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962484.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962486.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962489.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962492.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962493.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962496.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962498.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962500.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962502.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962503.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962506.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962508.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962511.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962513.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962516.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962520.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962523.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962527.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962530.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962534.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962540.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962543.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962546.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962554.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962556.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962559.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962562.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962564.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962567.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962574.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962577.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962580.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962583.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962586.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962589.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962592.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962596.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962601.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962605.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962608.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962611.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962614.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962617.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962622.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962624.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962628.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962632.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962638.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962644.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962654.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962658.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962664.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962668.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962671.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962677.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962683.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962687.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962691.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962698.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962703.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962709.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962714.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962719.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962728.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962733.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962736.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962741.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962746.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962748.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962753.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962757.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962762.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962766.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962772.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962778.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962783.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962787.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962791.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962799.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962806.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962812.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962816.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962818.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962822.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962829.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962836.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962843.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962852.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962859.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962867.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962877.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962887.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962892.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962896.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962903.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962913.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265962921.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963075.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963083.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963090.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963096.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963104.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963109.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963116.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963120.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963128.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963132.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963134.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963139.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963146.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963151.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963156.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963161.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963168.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963175.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963180.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963186.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963193.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963198.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963204.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963210.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963215.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963220.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963226.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963229.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963233.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963237.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963239.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963241.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963247.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963249.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963252.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963255.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963270.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963276.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963281.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963285.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963289.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963292.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963297.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963300.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963305.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963309.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963313.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963318.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963322.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963324.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963329.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963332.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963337.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963341.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963346.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963352.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963358.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963362.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963367.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963370.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963374.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963377.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963381.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963384.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963387.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963390.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963391.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963392.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963393.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963394.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963395.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963396.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963397.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963398.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963399.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963400.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963401.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963402.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963403.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963404.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963405.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963406.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963407.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963408.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963409.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963410.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963411.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963412.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963413.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963414.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963415.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963416.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963417.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963419.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963420.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963421.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963422.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963423.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963424.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963425.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963426.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963427.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963428.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963431.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963433.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963435.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963438.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963440.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963442.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963444.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963446.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963449.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963452.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963453.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963456.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963458.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963460.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963462.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963463.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963465.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963468.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963471.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963473.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963476.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963478.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963481.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963483.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963486.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963488.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963490.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple265963494.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385427.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385428.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385429.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385430.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385431.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385432.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385433.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385434.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385435.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385436.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385438.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385439.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385440.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385441.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385442.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385443.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385444.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385445.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385447.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385448.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385449.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385450.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385451.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385452.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385453.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385454.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385455.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385456.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385457.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385459.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385460.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385461.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385462.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385463.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385464.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385465.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385466.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385467.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385468.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385469.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385470.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385471.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385472.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385473.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385474.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385475.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385476.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385477.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385478.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385479.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385480.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385481.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385482.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385484.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385485.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385486.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385487.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385488.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385489.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385490.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385491.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385492.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385493.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385494.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385495.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385496.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385497.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385498.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385499.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385500.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385501.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385502.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385503.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385504.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385505.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385506.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385507.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385508.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385509.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385512.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385517.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385521.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385526.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385532.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385535.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385540.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385545.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385550.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385552.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385560.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385563.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385568.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385571.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385574.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385578.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385581.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385585.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385833.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385834.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385835.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385836.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385837.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385838.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385839.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385840.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385841.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385842.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385843.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385844.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385845.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385846.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385847.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385848.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385849.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385850.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385851.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385852.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385853.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385854.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385856.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385857.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385858.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385859.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385860.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385861.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385862.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385863.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385864.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385865.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385866.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385867.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385868.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385869.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385870.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385871.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385872.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385873.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385874.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385875.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385876.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385877.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385878.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385879.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385880.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385881.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385882.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385883.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385884.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385885.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385886.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385889.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385892.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385895.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385897.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385900.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385903.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385906.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385909.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385912.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385917.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385920.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385922.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385925.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385930.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385933.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385936.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385939.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385942.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385945.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385948.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385951.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385953.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385956.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385960.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385963.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385967.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385969.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385972.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385975.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385979.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385982.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385986.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385989.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385992.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385996.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267385999.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386003.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386005.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386008.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386011.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386014.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386015.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386017.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386020.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386023.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386025.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386440.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386441.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386443.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386444.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386445.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386446.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386447.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386448.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386449.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386450.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386451.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386452.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386453.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386454.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386455.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386456.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386457.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386458.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386459.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386460.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386461.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386462.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386464.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386465.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386469.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386470.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386473.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386474.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386477.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386479.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386483.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386486.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386487.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386492.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386494.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386497.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386499.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386505.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386509.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386513.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386515.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386516.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386518.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386522.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386524.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386527.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386529.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386534.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386537.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386540.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386545.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386548.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386552.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386556.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386559.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386562.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386565.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386569.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386571.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386575.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386579.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386582.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386583.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386587.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386590.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386593.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386596.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386600.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386606.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386610.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386613.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386615.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386618.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386621.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386625.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386630.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386633.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386636.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386637.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386641.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386644.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386647.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386650.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386653.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386656.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386659.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386663.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386665.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386668.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386670.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386674.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386678.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386681.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386685.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386686.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386687.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386688.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386689.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386690.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386691.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386701.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386706.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386710.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386715.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386720.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386724.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386729.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386735.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386738.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386747.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386751.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386756.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386762.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386767.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386772.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386777.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386781.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386784.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386789.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386790.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386795.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386799.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386802.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386805.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386809.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386813.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386818.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386823.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386830.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386835.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386840.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386843.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386846.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386849.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386852.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386855.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386860.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386863.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386868.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386876.root/TupleTools/MyTuple");
      chain->Add("/eos/lhcb/wg/CEP/charlotte/pPb2016_nobias/mytuple267386882.root/TupleTools/MyTuple");
      tree = chain;
#endif // SINGLE_TREE

   }
   Init(tree);
}

run_pA_mb::~run_pA_mb()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t run_pA_mb::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t run_pA_mb::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void run_pA_mb::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
   fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
   fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
   fChain->SetBranchAddress("HLT1TCK", &HLT1TCK, &b_HLT1TCK);
   fChain->SetBranchAddress("HLT2TCK", &HLT2TCK, &b_HLT2TCK);
   fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
   fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nLongTracks", &nLongTracks, &b_nLongTracks);
   fChain->SetBranchAddress("nDownstreamTracks", &nDownstreamTracks, &b_nDownstreamTracks);
   fChain->SetBranchAddress("nUpstreamTracks", &nUpstreamTracks, &b_nUpstreamTracks);
   fChain->SetBranchAddress("nVeloTracks", &nVeloTracks, &b_nVeloTracks);
   fChain->SetBranchAddress("nTTracks", &nTTracks, &b_nTTracks);
   fChain->SetBranchAddress("nBackTracks", &nBackTracks, &b_nBackTracks);
   fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
   fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
   fChain->SetBranchAddress("nVeloClusters", &nVeloClusters, &b_nVeloClusters);
   fChain->SetBranchAddress("nITClusters", &nITClusters, &b_nITClusters);
   fChain->SetBranchAddress("nTTClusters", &nTTClusters, &b_nTTClusters);
   fChain->SetBranchAddress("nOTClusters", &nOTClusters, &b_nOTClusters);
   fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
   fChain->SetBranchAddress("nMuonCoordsS0", &nMuonCoordsS0, &b_nMuonCoordsS0);
   fChain->SetBranchAddress("nMuonCoordsS1", &nMuonCoordsS1, &b_nMuonCoordsS1);
   fChain->SetBranchAddress("nMuonCoordsS2", &nMuonCoordsS2, &b_nMuonCoordsS2);
   fChain->SetBranchAddress("nMuonCoordsS3", &nMuonCoordsS3, &b_nMuonCoordsS3);
   fChain->SetBranchAddress("nMuonCoordsS4", &nMuonCoordsS4, &b_nMuonCoordsS4);
   fChain->SetBranchAddress("nMuonTracks", &nMuonTracks, &b_nMuonTracks);
   fChain->SetBranchAddress("B00", &B00, &b_B00);
   fChain->SetBranchAddress("B01", &B01, &b_B01);
   fChain->SetBranchAddress("B02", &B02, &b_B02);
   fChain->SetBranchAddress("B03", &B03, &b_B03);
   fChain->SetBranchAddress("B10", &B10, &b_B10);
   fChain->SetBranchAddress("B11", &B11, &b_B11);
   fChain->SetBranchAddress("B12", &B12, &b_B12);
   fChain->SetBranchAddress("B13", &B13, &b_B13);
   fChain->SetBranchAddress("B20", &B20, &b_B20);
   fChain->SetBranchAddress("B21", &B21, &b_B21);
   fChain->SetBranchAddress("B22", &B22, &b_B22);
   fChain->SetBranchAddress("B23", &B23, &b_B23);
   fChain->SetBranchAddress("F10", &F10, &b_F10);
   fChain->SetBranchAddress("F11", &F11, &b_F11);
   fChain->SetBranchAddress("F12", &F12, &b_F12);
   fChain->SetBranchAddress("F13", &F13, &b_F13);
   fChain->SetBranchAddress("F20", &F20, &b_F20);
   fChain->SetBranchAddress("F21", &F21, &b_F21);
   fChain->SetBranchAddress("F22", &F22, &b_F22);
   fChain->SetBranchAddress("F23", &F23, &b_F23);
   fChain->SetBranchAddress("log_hrc_fom_v3", &log_hrc_fom_v3, &b_log_hrc_fom_v3);
   fChain->SetBranchAddress("log_hrc_fom_B_v3", &log_hrc_fom_B_v3, &b_log_hrc_fom_B_v3);
   fChain->SetBranchAddress("log_hrc_fom_F_v3", &log_hrc_fom_F_v3, &b_log_hrc_fom_F_v3);
   fChain->SetBranchAddress("nchB", &nchB, &b_nchB);
   fChain->SetBranchAddress("adc_B", adc_B, &b_adc_B);
   fChain->SetBranchAddress("nchF", &nchF, &b_nchF);
   fChain->SetBranchAddress("adc_F", adc_F, &b_adc_F);
   fChain->SetBranchAddress("L0Global", &L0Global, &b_L0Global);
   fChain->SetBranchAddress("Hlt1Global", &Hlt1Global, &b_Hlt1Global);
   fChain->SetBranchAddress("Hlt2Global", &Hlt2Global, &b_Hlt2Global);
   fChain->SetBranchAddress("L0Muon,lowMultDecision", &L0Muon_lowMultDecision, &b_L0Muon_lowMultDecision);
   fChain->SetBranchAddress("L0DiMuon,lowMultDecision", &L0DiMuon_lowMultDecision, &b_L0DiMuon_lowMultDecision);
   fChain->SetBranchAddress("L0DiHadron,lowMultDecision", &L0DiHadron_lowMultDecision, &b_L0DiHadron_lowMultDecision);
   fChain->SetBranchAddress("L0Electron,lowMultDecision", &L0Electron_lowMultDecision, &b_L0Electron_lowMultDecision);
   fChain->SetBranchAddress("L0Photon,lowMultDecision", &L0Photon_lowMultDecision, &b_L0Photon_lowMultDecision);
   fChain->SetBranchAddress("L0DiEM,lowMultDecision", &L0DiEM_lowMultDecision, &b_L0DiEM_lowMultDecision);
   fChain->SetBranchAddress("L0nSelections", &L0nSelections, &b_L0nSelections);
   fChain->SetBranchAddress("Hlt1NoPVPassThroughDecision", &Hlt1NoPVPassThroughDecision, &b_Hlt1NoPVPassThroughDecision);
   fChain->SetBranchAddress("Hlt1LowMultPassThroughDecision", &Hlt1LowMultPassThroughDecision, &b_Hlt1LowMultPassThroughDecision);
   fChain->SetBranchAddress("Hlt1NoBiasNonBeamBeamDecision", &Hlt1NoBiasNonBeamBeamDecision, &b_Hlt1NoBiasNonBeamBeamDecision);
   fChain->SetBranchAddress("Hlt1MBNoBiasRateLimitedDecision", &Hlt1MBNoBiasRateLimitedDecision, &b_Hlt1MBNoBiasRateLimitedDecision);
   fChain->SetBranchAddress("Hlt1MBNoBiasDecision", &Hlt1MBNoBiasDecision, &b_Hlt1MBNoBiasDecision);
   fChain->SetBranchAddress("Hlt1CEPVeloCutDecision", &Hlt1CEPVeloCutDecision, &b_Hlt1CEPVeloCutDecision);
   fChain->SetBranchAddress("Hlt1LowMultDecision", &Hlt1LowMultDecision, &b_Hlt1LowMultDecision);
   fChain->SetBranchAddress("Hlt1LowMultMuonDecision", &Hlt1LowMultMuonDecision, &b_Hlt1LowMultMuonDecision);
   fChain->SetBranchAddress("Hlt1LowMultHerschelDecision", &Hlt1LowMultHerschelDecision, &b_Hlt1LowMultHerschelDecision);
   fChain->SetBranchAddress("Hlt1LowMultVeloCut_LeptonsDecision", &Hlt1LowMultVeloCut_LeptonsDecision, &b_Hlt1LowMultVeloCut_LeptonsDecision);
   fChain->SetBranchAddress("Hlt1LowMultVeloAndHerschel_LeptonsDecision", &Hlt1LowMultVeloAndHerschel_LeptonsDecision, &b_Hlt1LowMultVeloAndHerschel_LeptonsDecision);
   fChain->SetBranchAddress("Hlt1nSelections", &Hlt1nSelections, &b_Hlt1nSelections);
   fChain->SetBranchAddress("Hlt2NoBiasNonBeamBeamDecision", &Hlt2NoBiasNonBeamBeamDecision, &b_Hlt2NoBiasNonBeamBeamDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHDecision", &Hlt2LowMultLMR2HHDecision, &b_Hlt2LowMultLMR2HHDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHWSDecision", &Hlt2LowMultLMR2HHWSDecision, &b_Hlt2LowMultLMR2HHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHHHDecision", &Hlt2LowMultLMR2HHHHDecision, &b_Hlt2LowMultLMR2HHHHDecision);
   fChain->SetBranchAddress("Hlt2LowMultLMR2HHHHWSDecision", &Hlt2LowMultLMR2HHHHWSDecision, &b_Hlt2LowMultLMR2HHHHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultL2pPiDecision", &Hlt2LowMultL2pPiDecision, &b_Hlt2LowMultL2pPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiDecision", &Hlt2LowMultD2KPiDecision, &b_Hlt2LowMultD2KPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiPiDecision", &Hlt2LowMultD2KPiPiDecision, &b_Hlt2LowMultD2KPiPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KKPiDecision", &Hlt2LowMultD2KKPiDecision, &b_Hlt2LowMultD2KKPiDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2K3PiDecision", &Hlt2LowMultD2K3PiDecision, &b_Hlt2LowMultD2K3PiDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHDecision", &Hlt2LowMultChiC2HHDecision, &b_Hlt2LowMultChiC2HHDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHHHDecision", &Hlt2LowMultChiC2HHHHDecision, &b_Hlt2LowMultChiC2HHHHDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2PPDecision", &Hlt2LowMultChiC2PPDecision, &b_Hlt2LowMultChiC2PPDecision);
   fChain->SetBranchAddress("Hlt2LowMultHadron_noTrFiltDecision", &Hlt2LowMultHadron_noTrFiltDecision, &b_Hlt2LowMultHadron_noTrFiltDecision);
   fChain->SetBranchAddress("Hlt2LowMultL2pPiWSDecision", &Hlt2LowMultL2pPiWSDecision, &b_Hlt2LowMultL2pPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiWSDecision", &Hlt2LowMultD2KPiWSDecision, &b_Hlt2LowMultD2KPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KPiPiWSDecision", &Hlt2LowMultD2KPiPiWSDecision, &b_Hlt2LowMultD2KPiPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2KKPiWSDecision", &Hlt2LowMultD2KKPiWSDecision, &b_Hlt2LowMultD2KKPiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultD2K3PiWSDecision", &Hlt2LowMultD2K3PiWSDecision, &b_Hlt2LowMultD2K3PiWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHWSDecision", &Hlt2LowMultChiC2HHWSDecision, &b_Hlt2LowMultChiC2HHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2HHHHWSDecision", &Hlt2LowMultChiC2HHHHWSDecision, &b_Hlt2LowMultChiC2HHHHWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultChiC2PPWSDecision", &Hlt2LowMultChiC2PPWSDecision, &b_Hlt2LowMultChiC2PPWSDecision);
   fChain->SetBranchAddress("Hlt2LowMultMuonDecision", &Hlt2LowMultMuonDecision, &b_Hlt2LowMultMuonDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiMuonDecision", &Hlt2LowMultDiMuonDecision, &b_Hlt2LowMultDiMuonDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiMuon_PSDecision", &Hlt2LowMultDiMuon_PSDecision, &b_Hlt2LowMultDiMuon_PSDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiPhotonDecision", &Hlt2LowMultDiPhotonDecision, &b_Hlt2LowMultDiPhotonDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiPhoton_HighMassDecision", &Hlt2LowMultDiPhoton_HighMassDecision, &b_Hlt2LowMultDiPhoton_HighMassDecision);
   fChain->SetBranchAddress("Hlt2LowMultpi0Decision", &Hlt2LowMultpi0Decision, &b_Hlt2LowMultpi0Decision);
   fChain->SetBranchAddress("Hlt2LowMultDiElectronDecision", &Hlt2LowMultDiElectronDecision, &b_Hlt2LowMultDiElectronDecision);
   fChain->SetBranchAddress("Hlt2LowMultDiElectron_noTrFiltDecision", &Hlt2LowMultDiElectron_noTrFiltDecision, &b_Hlt2LowMultDiElectron_noTrFiltDecision);
   fChain->SetBranchAddress("Hlt2MBMicroBiasVeloDecision", &Hlt2MBMicroBiasVeloDecision, &b_Hlt2MBMicroBiasVeloDecision);
   fChain->SetBranchAddress("Hlt2LowMultTechnical_MinBiasDecision", &Hlt2LowMultTechnical_MinBiasDecision, &b_Hlt2LowMultTechnical_MinBiasDecision);
   fChain->SetBranchAddress("Hlt2nSelections", &Hlt2nSelections, &b_Hlt2nSelections);
   fChain->SetBranchAddress("MaxRoutingBits", &MaxRoutingBits, &b_MaxRoutingBits);
   fChain->SetBranchAddress("RoutingBits", RoutingBits, &b_RoutingBits);
   fChain->SetBranchAddress("StrippingMBNoBiasDecision", &StrippingMBNoBiasDecision, &b_StrippingMBNoBiasDecision);
   fChain->SetBranchAddress("StrippingHlt1NoBiasNonBeamBeamLineDecision", &StrippingHlt1NoBiasNonBeamBeamLineDecision, &b_StrippingHlt1NoBiasNonBeamBeamLineDecision);
   fChain->SetBranchAddress("StrippingLowMultNonBeamBeamNoBiasLineDecision", &StrippingLowMultNonBeamBeamNoBiasLineDecision, &b_StrippingLowMultNonBeamBeamNoBiasLineDecision);
   fChain->SetBranchAddress("StrippingLowMultMuonLineDecision", &StrippingLowMultMuonLineDecision, &b_StrippingLowMultMuonLineDecision);
   fChain->SetBranchAddress("StrippingLowMultDiMuonLineDecision", &StrippingLowMultDiMuonLineDecision, &b_StrippingLowMultDiMuonLineDecision);
   fChain->SetBranchAddress("ET_prev1", &ET_prev1, &b_ET_prev1);
   fChain->SetBranchAddress("ET_prev2", &ET_prev2, &b_ET_prev2);
   fChain->SetBranchAddress("nouttrck", &nouttrck, &b_nouttrck);
   fChain->SetBranchAddress("trck_key", trck_key, &b_trck_key);
   fChain->SetBranchAddress("trck_type", trck_type, &b_trck_type);
   fChain->SetBranchAddress("trck_posx", trck_posx, &b_trck_posx);
   fChain->SetBranchAddress("trck_posy", trck_posy, &b_trck_posy);
   fChain->SetBranchAddress("trck_posz", trck_posz, &b_trck_posz);
   fChain->SetBranchAddress("trck_px", trck_px, &b_trck_px);
   fChain->SetBranchAddress("trck_py", trck_py, &b_trck_py);
   fChain->SetBranchAddress("trck_pz", trck_pz, &b_trck_pz);
   fChain->SetBranchAddress("trck_charge", trck_charge, &b_trck_charge);
   fChain->SetBranchAddress("trck_muonid", trck_muonid, &b_trck_muonid);
   fChain->SetBranchAddress("trck_riche", trck_riche, &b_trck_riche);
   fChain->SetBranchAddress("trck_richk", trck_richk, &b_trck_richk);
   fChain->SetBranchAddress("trck_richp", trck_richp, &b_trck_richp);
   fChain->SetBranchAddress("trck_muonll", trck_muonll, &b_trck_muonll);
   fChain->SetBranchAddress("trck_muacc", trck_muacc, &b_trck_muacc);
   fChain->SetBranchAddress("trck_ecal", trck_ecal, &b_trck_ecal);
   fChain->SetBranchAddress("trck_hcal", trck_hcal, &b_trck_hcal);
   fChain->SetBranchAddress("trck_eop", trck_eop, &b_trck_eop);
   fChain->SetBranchAddress("trck_probe", trck_probe, &b_trck_probe);
   fChain->SetBranchAddress("trck_probmu", trck_probmu, &b_trck_probmu);
   fChain->SetBranchAddress("trck_probpi", trck_probpi, &b_trck_probpi);
   fChain->SetBranchAddress("trck_probk", trck_probk, &b_trck_probk);
   fChain->SetBranchAddress("trck_probp", trck_probp, &b_trck_probp);
   fChain->SetBranchAddress("trck_probghost", trck_probghost, &b_trck_probghost);
   fChain->SetBranchAddress("noutmuon", &noutmuon, &b_noutmuon);
   fChain->SetBranchAddress("muon_chi2ndf", muon_chi2ndf, &b_muon_chi2ndf);
   fChain->SetBranchAddress("muon_posx", muon_posx, &b_muon_posx);
   fChain->SetBranchAddress("muon_posy", muon_posy, &b_muon_posy);
   fChain->SetBranchAddress("muon_ux", muon_ux, &b_muon_ux);
   fChain->SetBranchAddress("muon_uy", muon_uy, &b_muon_uy);
   fChain->SetBranchAddress("muon_nshared", muon_nshared, &b_muon_nshared);
   fChain->SetBranchAddress("nphotons", &nphotons, &b_nphotons);
   fChain->SetBranchAddress("noutphot", &noutphot, &b_noutphot);
   fChain->SetBranchAddress("photon_px", photon_px, &b_photon_px);
   fChain->SetBranchAddress("photon_py", photon_py, &b_photon_py);
   fChain->SetBranchAddress("photon_pz", photon_pz, &b_photon_pz);
   fChain->SetBranchAddress("photon_nspd", photon_nspd, &b_photon_nspd);
   fChain->SetBranchAddress("photon_nprs", photon_nprs, &b_photon_nprs);
   fChain->SetBranchAddress("noutspd", &noutspd, &b_noutspd);
   fChain->SetBranchAddress("spd_posx", spd_posx, &b_spd_posx);
   fChain->SetBranchAddress("spd_posy", spd_posy, &b_spd_posy);
   fChain->SetBranchAddress("noutprs", &noutprs, &b_noutprs);
   fChain->SetBranchAddress("prs_posx", prs_posx, &b_prs_posx);
   fChain->SetBranchAddress("prs_posy", prs_posy, &b_prs_posy);
   fChain->SetBranchAddress("noutl0mu", &noutl0mu, &b_noutl0mu);
   fChain->SetBranchAddress("l0mu_pt", l0mu_pt, &b_l0mu_pt);
   fChain->SetBranchAddress("l0mu_phi", l0mu_phi, &b_l0mu_phi);
   fChain->SetBranchAddress("l0mu_theta", l0mu_theta, &b_l0mu_theta);
   fChain->SetBranchAddress("noutl0h", &noutl0h, &b_noutl0h);
   fChain->SetBranchAddress("l0h_et", l0h_et, &b_l0h_et);
   Notify();
}

Bool_t run_pA_mb::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void run_pA_mb::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t run_pA_mb::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef run_pA_mb_cxx
